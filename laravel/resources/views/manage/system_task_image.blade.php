@extends('manage/header')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('container')
    @parent
    <div style="margin-top:10px;" id="images">
        @foreach($images as $value)
            <div class="add-image" style="float:left;margin-left:30px;margin-top:10px;">
                <a href="{{ $value['view_image'] }}" target="_blank"><img id="img_show" width="200px" height="200px" src="{{ $value['view_image'] }}"></a>
                <input id="img_value" type="hidden" name="images[]" value="{{ $value['image'] }}" />
            </div>
        @endforeach
    </div>
@endsection

