@extends('manage/header')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
    table {
        border:1.1px solid #999ea2;
    }
    tr {
        border:1.1px solid #999ea2
    }
    td {
        border:1.1px solid #999ea2;
    }
    th {
        border:1.1px solid #999ea2;
    }
</style>
@section('container')
    @parent
    <div id="top-content">
        <div class="panel" style="margin-top:20px;width:96%;margin-left:2%;padding-top:50px;background-color: #daebfb">
            <div class="panel-body">
                <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%;">
                    <thead>
                    <tr style="background-color: #daebfb;">
                        <th colspan="8" style="text-align: center;font-size:24px;line-height: 65px;">Inspector Record</th>
                        <th colspan="2" style="text-align: center;font-size:24px;">Supervisor Remarks</th>
                    <tr style="background-color: #daebfb;">
                    <tr>
                        <th style="text-align: center;">Clause Ref.</th>
                        <th style="text-align: center">Id</th>
                        <th style="text-align: center;width:60%">Task/任務描述</th>
                        <th style="text-align: center">Yes</th>
                        <th style="text-align: center">No</th>
                        <th style="text-align: center">NA</th>
                        <th style="text-align: center;">Remarks</th>
                        <th style="text-align: center;">Photo upload</th>
                        <th style="text-align: center">Follow Up</th>
                        <th style="text-align: center">Photo Upload</th>
                    </tr>
                    </thead>
                    <tbody id="from-id-content">
                    @foreach ($taskInfo as $value)
                        <tr>
                            @if(in_array($value['task_id'], array_keys($systemLogInfo['task'])))
                                <td style="text-align: center;">{{ $value['clause_no'] }}</td>
                                <td style="text-align: center;">{{ $value['task_id'] }}</td>
                                <td align="left" style="width:60%;">{{ $value['name'] }}</td>
                                @if(!empty($systemLogInfo['task'][$value['task_id']]['status']))
                                    <td style="text-align: center;"><input type="checkbox" disabled @if($systemLogInfo['task'][$value['task_id']]['status'] == 1) checked @endif value="1" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                    <td style="text-align: center;"><input type="checkbox" disabled @if($systemLogInfo['task'][$value['task_id']]['status'] == 2) checked @endif value="2" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                    <td style="text-align: center;"><input type="checkbox" disabled @if($systemLogInfo['task'][$value['task_id']]['status'] == 3) checked @endif value="3" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                @else
                                    <td style="text-align: center;"><input type="checkbox" disabled value="1" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                    <td style="text-align: center;"><input type="checkbox" disabled value="2" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                    <td style="text-align: center;"><input type="checkbox" disabled value="3" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                @endif
                                <td style="text-align: center;width:90px;text-overflow:ellipsis;">
                                    @if(!empty($systemLogInfo['task'][$value['task_id']]['remarks']))
                                        <button onclick="javascript:return returnFalse()" class="btn btn-default add-tooltip" data-toggle="tooltip" data-container="body"
                                                data-placement="left" data-original-title="{{ $systemLogInfo['task'][$value['task_id']]['remarks'] }}" style="width:90px;text-overflow:ellipsis;white-space:nowrap;overflow:hidden;">
                                            {{ $systemLogInfo['task'][$value['task_id']]['remarks'] }}</button>
                                    @endif
                                </td>
                                <td style="text-align: center;">
                                    {{--                                        @if(!empty($systemLogInfo['task'][$value['task_id']]['image']))--}}
                                    {{--                                            <div class="src-image"><a target="_blank" href="{{ $systemLogInfo['task'][$value['task_id']]['view_image'] }}"><img src="{{ $systemLogInfo['task'][$value['task_id']]['view_image'] }}" style="margin-top:5px;" width="50px;" height="50px;"></a><input type="hidden" name="data[{{$value['task_id']}}][image]" value="{{ $systemLogInfo['task'][$value['task_id']]['image'] }}"></div>--}}
                                    {{--                                        @else--}}
                                    {{--                                            <input type="hidden" name="data[{{$value['task_id']}}][image]" value="">--}}
                                    {{--                                        @endif--}}
                                    <a href="{{ route('manage.systemLogTask.image', ['system_log_id' => $systemLogInfo['system_log_id'], 'task_id' => $value['task_id'], 'type' => 1]) }}">
                                        <label class="input-group-btn" style="margin-bottom:-1px;text-align: center;">
                                        <span id="search" class="btn btn-primary" style="height:25px;line-height:13px;">
                                            View
                                        </span>
                                        </label>
                                    </a>
                                </td>
                                <td>
                                    @if(!empty($systemLogInfo['task'][$value['task_id']]['supervisor_remarks']))
                                        <button onclick="javascript:return returnFalse()" class="btn btn-default add-tooltip" data-toggle="tooltip" data-container="body"
                                                data-placement="left" data-original-title="{{ $systemLogInfo['task'][$value['task_id']]['supervisor_remarks'] }}" style="width:90px;text-overflow:ellipsis;white-space:nowrap;overflow:hidden;">
                                            {{ $systemLogInfo['task'][$value['task_id']]['supervisor_remarks'] }}</button>
                                    @endif
                                </td>
                                <td>
                                    {{--                                        @if(!empty($systemLogInfo['task'][$value['task_id']]['supervisor_image']))--}}
                                    {{--                                            <div class="src-image"><a target="_blank" href="{{ $systemLogInfo['task'][$value['task_id']]['view_supervisor_image'] }}"><img src="{{ $systemLogInfo['task'][$value['task_id']]['view_supervisor_image'] }}" style="margin-top:5px;" width="50px;" height="50px;"></a><input type="hidden" name="data[{{$value['task_id']}}][supervisor_image]" value="{{ $systemLogInfo['task'][$value['task_id']]['supervisor_image'] }}"></div>--}}
                                    {{--                                        @endif--}}
                                    <a href="{{ route('manage.systemLogTask.image', ['system_log_id' => $systemLogInfo['system_log_id'], 'task_id' => $value['task_id'], 'type' => 2]) }}">
                                        <label class="input-group-btn" style="margin-bottom:-1px;text-align: center;">
                                        <span id="search" class="btn btn-primary" style="height:25px;line-height:13px;">
                                            View
                                        </span>
                                        </label>
                                    </a>
                                </td>
                            @else
                                <input type="hidden" name="data[{{ $value['task_id'] }}][task_id]" value="{{ $value['task_id'] }}">
                                <td style="text-align: center;">{{ $value['clause_no'] }}</td>
                                <td style="text-align: center;">{{ $value['task_id'] }}</td>
                                <td style="text-align: center;">{{ $value['name'] }}</td>
                                <td style="text-align: center;"><input type="checkbox" disabled="disabled" value="1" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                <td style="text-align: center;"><input type="checkbox" disabled="disabled" value="2" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                <td style="text-align: center;"><input type="checkbox" disabled="disabled" value="3" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                <td style="text-align: center;"><textarea cols="10" disabled="disabled" rows="1" name="data[{{ $value['task_id'] }}][remarks]"></textarea></td>
                                <td style="text-align: center;">
                                    {{--                                <input type="file" name="file" id="uploadImage" onchange="postData(this)">--}}
                                    {{--                                            <label class="input-group-btn">--}}
                                    {{--                                                <span class="btn btn-primary">--}}
                                    {{--                                                    <i class="glyphicon glyphicon-folder-open"></i>--}}
                                    {{--                                                    Browse… <input type="file" name="file" onchange="postData(this, {{ $value['task_id'] }})" style="display: none;" multiple>--}}
                                    {{--                                                </span>--}}
                                    {{--                                            </label>--}}
                                    <a href="{{ route('manage.systemLogTask.image', ['system_log_id' => $systemLogInfo['system_log_id'], 'task_id' => $value['task_id'], 'type' => 1]) }}">
                                        <label class="input-group-btn" style="margin-bottom:-1px;text-align: center;">
                                        <span id="search" class="btn btn-primary" style="height:25px;line-height:13px;">
                                            View
                                        </span>
                                        </label>
                                    </a>
                                </td>
                                <td style="text-align: center;"><textarea cols="10" disabled="disabled" rows="1" name="data[{{ $value['task_id'] }}][supervisor_remarks]"></textarea></td>
                                <td style="text-align: center;">
                                    {{--                                <input type="file" name="file" id="uploadImage" onchange="postData(this)">--}}
                                    {{--                                            <label class="input-group-btn">--}}
                                    {{--                                                <span class="btn btn-primary">--}}
                                    {{--                                                    <i class="glyphicon glyphicon-folder-open"></i>--}}
                                    {{--                                                    Browse… <input type="file" name="file" onchange="postData(this, {{ $value['task_id'] }})" style="display: none;" multiple>--}}
                                    {{--                                                </span>--}}
                                    {{--                                            </label>--}}
                                    <a href="{{ route('manage.systemLogTask.image', ['system_log_id' => $systemLogInfo['system_log_id'], 'task_id' => $value['task_id'], 'type' => 2]) }}">
                                        <label class="input-group-btn" style="margin-bottom:-1px;text-align: center;">
                                        <span id="search" class="btn btn-primary" style="height:25px;line-height:13px;">
                                            View
                                        </span>
                                        </label>
                                    </a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @if(!empty($systemLogInfo['view_sign']))
                <div>
                    <div style="float:left;background-color:#daebfb">
                        <div style="font-size:19px;color:black">Inspector User Sign:</div>
                    </div>
                    <div class="panel" style="margin-top:50px;width:80%;margin-left:20%;">
                        <div class="panel-body">
                            <img src="{{ $systemLogInfo['view_sign'] }}">
                        </div>
                    </div>
                </div>
            @endif

            @if(!empty($systemLogInfo['view_supervisor_sign']))
                <div>
                    <div style="float:left;background-color:#daebfb">
                        <div style="font-size:19px;color:black">Supervisor User Sign:</div>
                    </div>
                    <div class="panel" style="margin-top:50px;width:80%;margin-left:20%;">
                        <div class="panel-body">
                            <img src="{{ $systemLogInfo['view_supervisor_sign'] }}">
                        </div>
                    </div>
                </div>
            @endif

            @if(!empty($systemLogInfo['hkaa_sign']))
                <div>
                    <div style="float:left;background-color:#daebfb">
                        <div style="font-size:19px;color:black">HKAA User Sign:</div>
                    </div>
                    <div class="panel" style="margin-top:50px;width:80%;margin-left:20%;">
                        <div class="panel-body">
                            <img src="{{ $systemLogInfo['view_hkaa_sign'] }}">
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div id="middle-content" style="display: none">
        <highchart id="testchart" config="testconfig"></highchart>
    </div>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/highcharts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<script src="https://github.com/devongovett/pdfkit/releases/download/v0.6.2/pdfkit.js"></script>
<script src="{{ url('/admin/js/jspdf.debug.js') }}"></script>
<script src="{{ url('/admin/js/rgbcolor.js') }}"></script>
<script src="{{ url('/admin/js/canvg.js') }}"></script>
<script src="{{ url('/admin/js/script.js') }}"></script>
@section('menu')
    @parent
    <li class="active-link">
        <a href="{{ route('admin.supervisor.summary') }}">
            <i class="demo-psi-home"></i>
            <span class="menu-title">
                <strong>Supervisor Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.supervisor.individual') }}">
            <i class="demo-psi-home"></i>
            <span class="menu-title">
                <strong>Individual ID</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.supervisor.send', ['system_log_id' => $systemLogInfo['system_log_id']]) }}">
            <i class="demo-psi-home"></i>
            <span class="menu-title">
                <strong>Email Screen</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.supervisor.download", ['system_log_id' => $systemLogInfo['system_log_id']]) }}">
            <i class="demo-psi-home"></i>
            <span class="menu-title">
                <strong>Export Excel</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.supervisor.sign', ['system_log_id' => $systemLogInfo['system_log_id']]) }}">
            <i class="demo-psi-home"></i>
            <span class="menu-title">
                <strong>Endorse and Sign</strong>
            </span>
        </a>
    </li>
@endsection

<script>
    function postData(self, task_id) {
        var formData = new FormData();
        formData.append("file", $(self)[0].files[0]);
        var url = '{{ route("admin.upload") }}';
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "post",
            data: formData,
            processData: false, // 告诉jQuery不要去处理发送的数据
            contentType: false, // 告诉jQuery不要去设置Content-Type请求头
            dataType: 'text',
            success: function(data) {
                var params = JSON.parse(data);
                var image_url = '<a target="_blank" href="'+params.viewPath+'"><img src="'+params.viewPath+'" style="margin-top:5px;" width="50px;" height="50px;"></a><input type="hidden" name="data['+task_id+'][supervisor_image]" value="'+params.filePath+'">';
                $(self).parents("td").find('.src-image').remove();
                $(self).parents("td").append(image_url);
            },
            error: function(data) {

            }
        });
    }

    function returnFalse(){
        return false;
    }

    $(function(){
        $(".status").change(function(){
            if ($(this).is(':checked')) {
                $(this).parents('tr').find('.status').prop("checked", false);
                $(this).prop("checked", true);
            }
        });

        $('#save').click(function(){
            $('.status').removeAttr("disabled");
            $('#save-task').submit();
            return false;
        });
    })
</script>
