@extends('manage/header')
@section('container')
    @parent
    <!--Page Title-->

    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="morris-chart-network" class="morris-full-content" style="display:none"></div>
    <div id="page-title">
        <h1 class="page-header text-overflow">System Log List</h1>

        <!--Searchbox-->
        <div class="searchbox">
            <div class="input-group custom-search-form">
            </div>
        </div>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">System Log List</h3>
            </div>
            <form action="{{ route('manage.systemLog') }}" method="get" id="search_data_form">
                <div class="fixed-table-toolbar" style="height: 50px;">
                    <div class="columns columns-right btn-group pull-right" style="margin-right: 20px;">
                        <button class="btn btn-primary search_form">Search</button>
                    </div>
                    <div class="pull-right search">
                        @if(!empty($where['status']))
                            <select name="status">
                                <option @if($where['status'] == 0) selected @endif value="0">All</option>
                                <option @if($where['status'] == 1) selected @endif value="1">Inspector Sign</option>
                                <option @if($where['status'] == 2) selected @endif value="2">Supervisor Sign</option>
                                <option @if($where['status'] == 3) selected @endif value="3">Hkaa Staff Sign</option>
                            </select>
                        @else
                            <select name="status">
                                <option selected value="0">All</option>
                                <option value="1">Inspector Sign</option>
                                <option value="2">Supervisor Sign</option>
                                <option value="3">Hkaa Staff Sign</option>
                            </select>
                        @endif
                        &nbsp&nbsp
                    </div>
                </div>
            </form>
            <div class="panel-body">
                <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="text-align: center">ID</th>
                        <th style="text-align: center">SystemId</th>
                        <th style="text-align: center">FromId</th>
                        <th style="text-align: center">CreateTime</th>
                        <th style="text-align: center">Operate</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (!empty($data))
                        @foreach($data->items() as $value)
                            <tr>
                                <td style="text-align: center">{{ $value['system_log_id'] }}</td>
                                <td style="text-align: center">{{ $value['system_id'] }}</td>
                                <td style="text-align: center">{{ $value['from_id'] }}</td>
                                <td style="text-align: center">{{ date('Y-m-d H:i:s', $value['create_time']) }}</td>
                                <td style="text-align: center">
                                    <a href="{{ route("manage.systemLog.showAll", ['system_log_id' => $value['system_log_id']]) }}">
                                        <div class="columns columns-right btn-group">
                                            <button class="btn btn-primary search_form">Show All</button>
                                        </div>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                @if (!empty($data))
                    {{ $data->links('manage.pages', ['data' => $data, $where]) }}
                @endif
            </div>
        </div>
    </div>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script>
    function loadFile(file){
        $("#filenames").val(file.name)
    }

    $(function(){
    })
</script>
