@extends('manage/header')
@section('container')
    @parent
    <!--Page Title-->

    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="morris-chart-network" class="morris-full-content" style="display:none"></div>
    <div id="page-title">
        <h1 class="page-header text-overflow">System List</h1>

        <!--Searchbox-->
        <div class="searchbox">
            <div class="input-group custom-search-form">
            </div>
        </div>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">System List</h3>
            </div>
            <form action="{{ route('manage.system') }}" method="get" id="search_data_form">
                <div class="fixed-table-toolbar" style="height: 50px;">
                    <div class="columns columns-right btn-group pull-right" style="margin-right: 20px;">
                        <button class="btn btn-primary search_form">Search</button>
                    </div>
                    <div class="pull-right search">
                        <input class="form-control" type="text" name="system_id" placeholder="System Id" id="system_id" style="height: initial" value="@if(!empty($where['system_id'])) {{ $where['system_id'] }} @endif">
                        &nbsp&nbsp
                    </div>
                </div>
            </form>
            <div class="panel-body">
                <form action="{{ route("manage.system") }}" method="post" enctype="multipart/form-data" id="upload-system">
                    {{ csrf_field() }}
                    <div class="row" style="width:80%;margin-left:15%;margin-bottom:20px;">
                    <div class="col-lg-8 col-sm-6 col-12">
                        <div class="input-group">
                            <input type="text" id="filenames" class="form-control" style="height:50px;" οnkeydοwn="return false;" οnpaste="return false;" placeholder="System Task Associate Csv File...">
                            <label class="input-group-btn">
                            <span class="btn btn-primary" style="height:50px;line-height:35px;">
                                <i class="glyphicon glyphicon-folder-open"></i>
                                Browse… <input type="file" name="system" onchange="loadFile(this.files[0])" style="display: none;" multiple>
                            </span>
                            </label>
                            <label class="input-group-btn">
                            <span id="save" class="btn btn-primary" style="height:50px;line-height:35px;margin-left: 10px;z-index:1">
                                Upload And Save
                            </span>
                            </label>
                        </div>
                    </div>
                </div>

                <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="text-align: center">SystemId</th>
                        <th style="text-align: center">FromId</th>
                        <th style="text-align: center">Equipment Type</th>
                        <th style="text-align: center">Brand Name</th>
                        <th style="text-align: center">CreateTime</th>
                        <th style="text-align: center">Operate</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (!empty($data))
                        @foreach($data->items() as $value)
                            <tr>
                                <td style="text-align: center">{{ $value['system_id'] }}</td>
                                <td style="text-align: center">{{ $value['from_id'] }}</td>
                                <td style="text-align: center">@if(!empty($value['equipment'])) {{ $value['equipment'] }} @endif</td>
                                <td style="text-align: center">@if(!empty($value['brand'])) {{ $value['brand'] }} @endif</td>
                                <td style="text-align: center">{{ date('Y-m-d H:i:s', $value['create_time']) }}</td>
                                <td style="text-align: center">
                                    <a href="{{ route("manage.system.check_task", ['system_id' => $value['id']]) }}">
                                        <label class="input-group-btn">
                                            <span id="save" class="btn btn-primary" style="height:50px;line-height:35px;margin-left: 10px;z-index:1">
                                                Check Task
                                            </span>
                                        </label>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                @if (!empty($data))
                    {{ $data->links('manage.pages', ['data' => $data, $where]) }}
                @endif
            </div>
        </div>
    </div>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script>
    function loadFile(file){
        $("#filenames").val(file.name)
    }

    $(function(){
        $("#save").click(function(){
            $("#upload-system").submit();
            return false;
        });
    })
</script>
