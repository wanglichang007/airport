@extends('manage/header')
@section('container')
    @parent
    <!--Page Title-->

    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="morris-chart-network" class="morris-full-content" style="display:none"></div>
    <div id="page-title">
        <h1 class="page-header text-overflow">Task List</h1>

        <!--Searchbox-->
        <div class="searchbox">
            <div class="input-group custom-search-form">
            </div>
        </div>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Task List</h3>
            </div>
            <div class="panel-body">
                <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="text-align: center">Task Id</th>
                        <th style="text-align: center">Clause No</th>
                        <th style="text-align: center">Name</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (!empty($task))
                        @foreach($task as $value)
                            <tr>
                                <td style="text-align: center">{{ $value['taskInfo']['task_id'] }}</td>
                                <td style="text-align: center">{{ $value['taskInfo']['clause_no'] }}</td>
                                <td style="text-align: center">{{ $value['taskInfo']['name'] }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                @if (!empty($data))
                    {{ $data->links('manage.pages', ['data' => $data, []]) }}
                @endif
            </div>
        </div>
    </div>
@endsection
