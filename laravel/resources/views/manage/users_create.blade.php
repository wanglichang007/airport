@extends('manage/header')
@section('container')
    @parent
    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Create User</h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">

        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Create User</h3>
                    </div>

                    <!--Horizontal Form-->
                    <!--===================================================-->
                    <form class="form-horizontal" action="{{ route('manage.users.create') }}" method="post">
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="demo-hor-inputemail">Account</label>
                                <div class="col-sm-9">
                                    <input type="text" name="account"  placeholder="Account" id="demo-hor-inputemail" class="form-control" style="height: initial" />

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="demo-hor-inputemail">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" name="password"  placeholder="Password" id="demo-hor-inputemail" class="form-control" style="height: initial" />

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="demo-hor-inputemail">Repeat password</label>
                                <div class="col-sm-9">
                                    <input type="password" name="repeat_password"  placeholder="Repeat password" id="demo-hor-inputemail" class="form-control" style="height: initial" />

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="demo-hor-inputemail">Nickname</label>
                                <div class="col-sm-9">
                                    <input type="text" name="nickname"  placeholder="Nickname" id="demo-hor-inputemail" class="form-control" style="height: initial" />

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="demo-hor-inputemail">Type</label>
                                <div class="col-sm-9">
                                    <select class="col-sm-16"  name="type">
                                        <option value="1">Inspector</option>
                                        <option value="2">Supervisor</option>
                                        <option value="3">Hkaa Staff</option>
                                    </select>
                                </div>
                            </div>
                            <div class="panel-footer text-right">
                                <button class="btn btn-success" type="submit">Save</button>
                            </div>
                        </div>
                    </form>
                    <!--===================================================-->
                    <!--End Horizontal Form-->

                </div>
            </div>
        </div>
    </div>
    <script>
    </script>
@endsection
<script>
    $('.search_form').click(function(){
        $('#search_data_form').submit();
        return false;
    })
</script>
