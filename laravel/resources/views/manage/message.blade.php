<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>message</title>


    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>


    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ url('/admin/css/bootstrap.min.css') }}" rel="stylesheet">


    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{ url('/admin/css/nifty.min.css') }}" rel="stylesheet">


    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{ url('/admin/css/demo/nifty-demo-icons.min.css') }}" rel="stylesheet">







    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{ url('/admin/plugins/pace/pace.min.css') }}" rel="stylesheet">
    <script src="{{ url('/admin/plugins/pace/pace.min.js') }}"></script>


    <!--jQuery [ REQUIRED ]-->
    <script src="{{ url('/admin/js/jquery-2.2.4.min.js') }}"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{ url('/admin/js/bootstrap.min.js') }}"></script>


    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="{{ url('/admin/js/nifty.min.js') }}"></script>






    <!--=================================================-->




    <!--=================================================

    REQUIRED
    You must include this in your project.


    RECOMMENDED
    This category must be included but you may modify which plugins or components which should be included in your project.


    OPTIONAL
    Optional plugins. You may choose whether to include it in your project or not.


    DEMONSTRATION
    This is to be removed, used for demonstration purposes only. This category must not be included in your project.


    SAMPLE
    Some script samples which explain how to initialize plugins or components. This category should not be included in your project.


    Detailed information and more samples can be found in the document.

    =================================================-->


</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
<div id="container" class="cls-container">

    <!-- HEADER -->
    <!--===================================================-->
    <div class="cls-header">
        <div class="cls-brand">
            <a class="box-inline" href="index.html">
                <!-- <img alt="Nifty Admin" src="images/logo.png" class="brand-icon"> -->
                <span class="brand-title"></span>
            </a>
        </div>
    </div>

    <!-- CONTENT -->
    <!--===================================================-->
    <div class="cls-content">
        {{--<h1 class="error-code text-info">出错了!</h1>--}}
        <p class="text-main text-semibold text-lg text-uppercase">{{ $message }}</p>
        <hr class="new-section-sm bord-no">
        @if(!empty($route))
            <div class="pad-top"><a class="btn-link" href="{{ route($route) }}">返回</a></div>
        @else
            <div class="pad-top"><a class="btn-link" href="javascript://" onClick="javascript :history.back(-1);">返回</a></div>
        @endif
    </div>


</div>
<!--===================================================-->
<!-- END OF CONTAINER -->


</body>
</html>

