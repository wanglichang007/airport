@if($data->lastPage() > 1)
    <div class="dataTables_paginate paging_simple_numbers" id="demo-dt-delete_paginate">
        <ul class="pagination">
            <li class="paginate_button next" aria-controls="demo-dt-delete" tabindex="0" id="demo-dt-delete_next">
                @if(!empty($where))
                    <a href="{{ $data->url(1)."&".http_build_query($where) }}"><i class="">first</i></a>
                @else
                    <a href="{{ $data->url(1) }}"><i class="">first</i></a>
                @endif
            </li>
            @if ($data->currentPage() > 1)

                <li class="paginate_button previous" aria-controls="demo-dt-delete" tabindex="0" id="demo-dt-delete_previous">
                    @if(!empty($where))
                        <a href="{{ $data->previousPageUrl()."&".http_build_query($where) }}"><i class="demo-psi-arrow-left"></i></a>
                    @else
                        <a href="{{ $data->previousPageUrl() }}"><i class="demo-psi-arrow-left"></i></a>
                    @endif
                </li>
            @endif
            <?php
            $number = 4;
            $be = $data->currentPage() - $number > 1 ? $data->currentPage() - $number : 1;
            $en = $be + 2*$number >  $data->lastPage() ? $data->lastPage() : $be + 2*$number;
            // if ( < 5) {
            //     $be = 1;
            //     if ( >  $data->currentPage() + (10 - $data->currentPage()))
            //         $en = $data->currentPage() + (10 - $data->currentPage());
            //     else
            //         $en = $data->lastPage();
            // } else {
            //     $be = $data->currentPage() - 4;
            //     if ($data->currentPage() + 5 < $data->lastPage()) {
            //         $en = $data->currentPage() + 5;
            //     }
            //     else {
            //         $en = $data->lastPage();
            //         $be = $data->lastPage() - 9;
            //     }
            // }
            ?>
            @for($i = $be; $i <= $en; $i++)
                <li class="paginate_button @if($data->currentPage() == $i) active @endif" aria-controls="demo-dt-delete" tabindex="0">
                    @if(!empty($where))
                        <a href="{{ $data->url($i)."&".http_build_query($where) }}">{{ $i }}</a>
                    @else
                        <a href="{{ $data->url($i) }}">{{ $i }}</a>
                    @endif
                </li>
            @endfor
            @if ($data->currentPage() < $data->lastPage())
                <li class="paginate_button next" aria-controls="demo-dt-delete" tabindex="0" id="demo-dt-delete_next">
                    @if(!empty($where))
                        <a href="{{ $data->nextPageUrl()."&".http_build_query($where) }}"><i class="demo-psi-arrow-right"></i></a>
                    @else
                        <a href="{{ $data->nextPageUrl() }}"><i class="demo-psi-arrow-right"></i></a>
                    @endif
                </li>
            @endif
            <li class="paginate_button next" aria-controls="demo-dt-delete" tabindex="0" id="demo-dt-delete_next">
                @if(!empty($where))
                    <a href="{{ $data->url($data->lastPage())."&".http_build_query($where) }}"><i class="">last</i></a>
                @else
                    <a href="{{ $data->url($data->lastPage()) }}"><i class="">last</i></a>
                @endif
            </li>
            <li class="paginate_button next" aria-controls="demo-dt-delete" tabindex="0" id="demo-dt-delete_next">
                <a href="javascript:;"><i class="">{{$data->total()}} &nbsp;&nbsp;records </i></a>
            </li>
        </ul>
    </div>
@endif
