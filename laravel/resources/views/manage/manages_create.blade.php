@extends('manage/header')
@section('container')
    @parent
    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">Create Manage User</h1>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">

        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Create Manage User</h3>
                    </div>

                    <!--Horizontal Form-->
                    <!--===================================================-->
                    <form class="form-horizontal" action="{{ route('manage.manages.create') }}" method="post">
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="demo-hor-inputemail">Account</label>
                                <div class="col-sm-9">
                                    <input type="text" name="account"  placeholder="Account" id="demo-hor-inputemail" class="form-control" />

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="demo-hor-inputemail">Password</label>
                                <div class="col-sm-9">
                                    <input type="password" name="password"  placeholder="Password" id="demo-hor-inputemail" class="form-control" />

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="demo-hor-inputemail">Repeat password</label>
                                <div class="col-sm-9">
                                    <input type="password" name="repeat_password"  placeholder="Repeat password" id="demo-hor-inputemail" class="form-control" />

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="demo-hor-inputemail">Nickname</label>
                                <div class="col-sm-9">
                                    <input type="text" name="nickname"  placeholder="Nickname" id="demo-hor-inputemail" class="form-control" />

                                </div>
                            </div>
                            <div class="panel-footer text-right">
                                <button class="btn btn-success" type="submit">Save</button>
                            </div>
                        </div>
                    </form>
                    <!--===================================================-->
                    <!--End Horizontal Form-->

                </div>
            </div>
        </div>
    </div>
    <script>
    </script>
@endsection
