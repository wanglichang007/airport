@extends('manage/header')
@section('container')
    @parent
    <!--Page Title-->

    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="morris-chart-network" class="morris-full-content" style="display:none"></div>
    <div id="page-title">
        <h1 class="page-header text-overflow">Users List</h1>

        <!--Searchbox-->
        <div class="searchbox">
            <div class="input-group custom-search-form">
            </div>
        </div>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Users List</h3>
            </div>
            <form action="{{ route('manage.users') }}" method="get" id="search_data_form">
                <div class="fixed-table-toolbar">
                    <div class="columns columns-right btn-group pull-right" style="margin-right: 20px;">
                        <button class="btn btn-primary search_form">Search</button>
                    </div>
                    <div class="pull-right search">
                        <input class="form-control" style="height:34px;" type="text" name="account" placeholder="Account" value="@if(!empty($where['account'])) {{ $where['account'] }} @endif">
                        &nbsp&nbsp
                    </div>
                    <div class="pull-right search" style="margin-right: 5px;padding-top:5px;">
                        <select class="col-sm-16"  name="type">
                            @if(!empty($where['type']))
                                <option @if($where['type'] == 1) selected @endif value="1">Inspector</option>
                                <option @if($where['type'] == 2) selected @endif value="2">Supervisor</option>
                                <option @if($where['type'] == 3) selected @endif value="3">Hkaa Staff</option>
                            @else
                                <option value="1">Inspector</option>
                                <option value="2">Supervisor</option>
                                <option value="3">Hkaa Staff</option>
                            @endif
                        </select>
                    </div>
                </div>
            </form>
            <div id="demo-custom-toolbar2" class="table-toolbar-left" style="margin-top:10px;margin-left:20px;">
                <a href="{{ route('manage.users.create') }}"><button id="demo-dt-addrow-btn" class="btn btn-primary"><i class="demo-pli-plus"></i>Create User</button></a>
            </div>
            <div class="panel-body">
                <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="text-align: center">Account</th>
                        <th style="text-align: center">Nickname</th>
                        <th style="text-align: center">Type</th>
                        <th style="text-align: center">CreateTime</th>
                        <th style="text-align: center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (!empty($data))
                        @foreach($data->items() as $value)
                            <tr>
                                <td style="text-align: center">{{ $value['account'] }}</td>
                                <td style="text-align: center">{{ $value['name'] }}</td>
                                <td>
                                    @if($value['type'] == 1)
                                        Inspector
                                    @elseif ($value['type'] == 2)
                                        Supervisor
                                    @elseif ($value['type'] == 3)
                                        Hkaa Staff
                                    @else
                                        Unknown Type
                                    @endif
                                </td>
                                <td style="text-align: center">{{ date('Y-m-d H:i:s', $value['create_time']) }}</td>
                                <td style="text-align: center"><a href="{{ route("manage.delete", ['user_id' => $value['users_id']]) }}" onclick="return del();"><button class="btn btn-danger">Delete</button></a></td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                @if (!empty($data))
                    {{ $data->links('manage.pages', ['data' => $data, $where]) }}
                @endif
            </div>
        </div>
    </div>
@endsection
<script>
    function del() {
        var msg = "Are you sure you want to delete?";
        if (confirm(msg)==true){
            return true;
        }else{
            return false;
        }
    }
</script>
