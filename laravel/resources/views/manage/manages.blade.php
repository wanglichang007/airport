@extends('manage/header')
@section('container')
    @parent
    <!--Page Title-->

    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="morris-chart-network" class="morris-full-content" style="display:none"></div>
    <div id="page-title">
        <h1 class="page-header text-overflow">Manage Users List</h1>

        <!--Searchbox-->
        <div class="searchbox">
            <div class="input-group custom-search-form">
            </div>
        </div>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Manage Users List</h3>
            </div>
            <div id="demo-custom-toolbar2" class="table-toolbar-left" style="margin-top:10px;margin-left:20px;">
                <a href="{{ route('manage.manages.create') }}"><button id="demo-dt-addrow-btn" class="btn btn-primary"><i class="demo-pli-plus"></i>Create Manage User</button></a>
            </div>
            <div class="panel-body">
                <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="text-align: center">Account</th>
                        <th style="text-align: center">Nickname</th>
                        <th style="text-align: center">CreateTime</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (!empty($data))
                        @foreach($data->items() as $value)
                            <tr>
                                <td style="text-align: center">{{ $value['account'] }}</td>
                                <td style="text-align: center">{{ $value['name'] }}</td>
                                <td style="text-align: center">{{ date('Y-m-d H:i:s', $value['create_time']) }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                @if (!empty($data))
                    {{ $data->links('manage.pages', ['data' => $data, []]) }}
                @endif
            </div>
        </div>
    </div>
@endsection
