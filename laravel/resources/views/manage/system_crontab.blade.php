@extends('manage/header')
@section('container')
    @parent
    <!--Page Title-->

    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="morris-chart-network" class="morris-full-content" style="display:none"></div>
    <div id="page-title">
        <h1 class="page-header text-overflow">System Crontab List</h1>

        <!--Searchbox-->
        <div class="searchbox">
            <div class="input-group custom-search-form">
            </div>
        </div>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">System Crontab List</h3>
            </div>
            <div class="panel-body">
                <form action="{{ route("manage.system.crontab") }}" method="post" enctype="multipart/form-data" id="upload-system">
                    {{ csrf_field() }}
                    <div class="row" style="width:80%;margin-left:15%;margin-bottom:20px;">
                        <div class="col-lg-10 col-sm-6 col-12">
                            <div class="input-group">
                                <input type="text" id="filenames" class="form-control" style="height:50px;" οnkeydοwn="return false;" οnpaste="return false;" placeholder="System Task Associate Csv File...">
                                <label class="input-group-btn">
                            <span class="btn btn-primary" style="height:50px;line-height:35px;">
                                <i class="glyphicon glyphicon-folder-open"></i>
                                Browse… <input type="file" name="system" onchange="loadFile(this.files[0])" style="display: none;" multiple>
                            </span>
                                </label>
                                <label class="input-group-btn">
                            <span id="save" class="btn btn-primary" style="height:50px;line-height:35px;margin-left: 10px;z-index:1">
                                Upload And Save
                            </span>
                                </label>
                                <label class="input-group-btn">
                                    <a href="{{ route('manage.system.clearCrontab') }}">
                                        <span id="clear" class="btn btn-primary" style="height:50px;line-height:35px;margin-left: 10px;z-index:1">
                                            Clear Data
                                        </span>
                                    </a>
                                </label>
{{--                                <label class="input-group-btn">--}}
{{--                                    <a href="{{ route('manage.system.downloadCrontab') }}">--}}
{{--                                        <span id="clear" class="btn btn-primary" style="height:50px;line-height:35px;margin-left: 10px;z-index:1">--}}
{{--                                            Download Excel(Last Excel)--}}
{{--                                        </span>--}}
{{--                                    </a>--}}
{{--                                </label>--}}
                            </div>
                        </div>
                    </div>

                    <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th style="text-align: center">SystemId</th>
                            <th style="text-align: center">FromId</th>
                            <th style="text-align: center">WorkTime</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if (!empty($data))
                            @foreach($data->items() as $value)
                                <tr>
                                    <td style="text-align: center">{{ $value['system_id'] }}</td>
                                    <td style="text-align: center">{{ $value['from_id'] }}</td>
                                    <td style="text-align: center">{{ date("Y-m-d", $value['work_time']) }}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                @if (!empty($data))
                    {{ $data->links('manage.pages', ['data' => $data]) }}
                @endif
            </div>
        </div>
    </div>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script>
    function loadFile(file){
        $("#filenames").val(file.name)
    }

    $(function(){
        $("#save").click(function(){
            $("#upload-system").submit();
            return false;
        });
    })
</script>
