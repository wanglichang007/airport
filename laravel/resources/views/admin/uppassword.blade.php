@extends('admin/header')
@section('container')
    @parent
    <div id="page-content">

        <div class="row">
            <div class="col-sm-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Update Password</h3>
                    </div>

                    <!--Horizontal Form-->
                    <!--===================================================-->
                    <form class="form-horizontal" action="{{ route('admin.personal.uppassword') }}" method="post">
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="demo-hor-inputemail">New Password</label>
                                <div class="col-sm-9">
                                    <input type="password" name="password"  placeholder="Password" id="demo-hor-inputemail" class="form-control" style="height: initial" />

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="demo-hor-inputemail">Repeat New Password</label>
                                <div class="col-sm-9">
                                    <input type="password" name="repeat_password"  placeholder="Repeat password" id="demo-hor-inputemail" class="form-control" style="height: initial" />

                                </div>
                            </div>
                            <div class="panel-footer text-right">
                                <button class="btn btn-success" onclick="javascript:return delImage(this)" type="submit">Save</button>
                            </div>
                        </div>
                    </form>
                    <!--===================================================-->
                    <!--End Horizontal Form-->

                </div>
            </div>
        </div>
    </div>
    <script>
    </script>
@endsection

@section('menu')
    @parent
    <li class="active-link">
        <a href="javascript:history.go(-1);">
            <i class="fa fa-chevron-left"></i>
            <span class="menu-title">
                <strong style="color: white;">Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
@endsection
<script>
    function delImage() {
        var msg = "Sure?";
        if (confirm(msg)==true){
            return true;
        }else{
            return false;
        }
    }

    $('.search_form').click(function(){
        $('#search_data_form').submit();
        return false;
    })
</script>
