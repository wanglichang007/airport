@extends('admin/header')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('container')
    @parent
    <div class="row pad-ver" style="margin-top:100px;">
        <input type="hidden" id="system_log_id" name="system_log_id" value="{{ $systemLogId }}">
        <input type="hidden" id="task_id" name="task_id" value="{{ $taskId }}">
        <input type="hidden" id="type" name="type" value="{{ $type }}">
        <div class="container">
            <label class="input-group-btn">
                <a href="javascript:history.go(-1);">
                    <span id="search" class="btn btn-primary" style="height:50px;line-height:35px;margin-left: 10px;z-index:1">
                        Back
                    </span>
                </a>
            </label>
        </div>
    </div>
    <div style="margin-top:10px;" id="images">
        @foreach($images as $value)
            <div class="add-image" style="float:left;margin-left:30px;margin-top:10px;">
                <a href="{{ $value['view_image'] }}" target="_blank"><img id="img_show" width="200px" height="200px" src="{{ $value['view_image'] }}"></a>
                <input id="img_value" type="hidden" name="images[]" value="{{ $value['image'] }}" />
            </div>
        @endforeach
    </div>
@endsection
@section('menu')
    @parent
    <li class="active-link">
        <a href="javascript:history.back(-1)">
            <i class="demo-psi-home"></i>
            <span class="menu-title">
                <strong>Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.hkaastaff.downloadCrontab") }}">
            <i class="fa fa-cloud-download"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Monthly Schedule</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script>
    function postData(self) {
        var formData = new FormData();
        formData.append("file", $(self)[0].files[0]);
        var url = '{{ route("admin.upload") }}';
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "post",
            data: formData,
            processData: false, // 告诉jQuery不要去处理发送的数据
            contentType: false, // 告诉jQuery不要去设置Content-Type请求头
            dataType: 'text',
            success: function(data) {
                var params = JSON.parse(data);
                console.log(params);
                    {{--var href = '{{ route("admin.upload.image") }}'+'?image='+params.filePath+'&size=200_200';--}}
                var htm = '<div class="add-image" style="float:left;margin-left:30px;margin-top:10px;">' +
                    '<a href="'+params.viewPath+'" target="_blank"><img id="img_show" width="200px;" height="200px;" src="'+params.viewPath+'"></a>' +
                    '<input id="img_value" type="hidden" name="images[]" value="'+params.filePath+'" />' +
                    '&nbsp<i style="float:right;margin-top:180px;" onclick="javascript:return delImage(this)" class="fa fa-trash-o"></i>' +
                    '</div>';
                $('#images').append(htm);
            },
            error: function(data) {

            }
        });
    }

    $(function(){
        $('#mainnav-container').hide();
        $('#save').click(function(){
            $('#save-image').submit();
            return false;
        })
    })
</script>
