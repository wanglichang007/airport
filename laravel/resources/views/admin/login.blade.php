<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin</title>


    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>


    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ url('/admin/css/bootstrap.min.css') }}" rel="stylesheet">


    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{ url('/admin/css/nifty.min.css') }}" rel="stylesheet">


    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{ url('/admin/css/demo/nifty-demo-icons.min.css') }}" rel="stylesheet">



    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ url('/admin/css/demo/nifty-demo.min.css') }}" rel="stylesheet">


    <!--Magic Checkbox [ OPTIONAL ]-->
    <link href="{{ url('/admin/plugins/magic-check/css/magic-check.min.css') }}" rel="stylesheet">






    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{ url('/admin/plugins/pace/pace.min.css') }}" rel="stylesheet">
    <script src="{{ url('/admin/plugins/pace/pace.min.js') }}"></script>


    <!--jQuery [ REQUIRED ]-->
    <script src="{{ url('/admin/js/jquery-2.2.4.min.js') }}"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{ url('/admin/js/bootstrap.min.js') }}"></script>


    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="{{ url('/admin/js/nifty.min.js') }}"></script>






    <!--=================================================-->

    <!--Background Image [ DEMONSTRATION ]-->
    <script src="{{ url('/admin/js/demo/bg-images.js') }}"></script>





    <!--=================================================

    REQUIRED
    You must include this in your project.


    RECOMMENDED
    This category must be included but you may modify which plugins or components which should be included in your project.


    OPTIONAL
    Optional plugins. You may choose whether to include it in your project or not.


    DEMONSTRATION
    This is to be removed, used for demonstration purposes only. This category must not be included in your project.


    SAMPLE
    Some script samples which explain how to initialize plugins or components. This category should not be included in your project.


    Detailed information and more samples can be found in the document.

    =================================================-->


</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->

<body>
<div id="container" class="cls-container" style="background-color: #DBEBF3">

    <!-- BACKGROUND IMAGE -->
    <!--===================================================-->
    <div id="bg-overlay"></div>


    <!-- LOGIN FORM -->
    <!--===================================================-->
    <div class="cls-content">
        <div class="panel">
            <div class="panel-body" style="padding:30px 20px 50px;">
                <div class="mar-ver pad-btm" style="padding-bottom: 30px;">
{{--                    <h5 class="h5 mar-no">電扶以及行人走道例行保養工作紀錄-紀錄輸入程序(Trial Version)</h5>--}}
                    <span style="font-size:2rem;">電動扶梯以及自動人行道例行保養工作紀錄-紀錄輸入程序(Trial Version)</span>
                    <br>
{{--                    <h3 class="h3 mar-no">PLANNEDMAINTEANCE-TASKCHECKLIST(ESCALATOR&WALKWAY)</h3>--}}
                    <span style="font-size:3rem;">PLANNED MAINTENANCE-TASK CHECKLIST(ESCALATOR & WALKWAY)</span>
                </div>
                <form action="{{ route('admin.login') }}" method="post" style="display:inline-block;text-align:center;">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Account" name="account" style="width:350px;height:36px;" autofocus>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" placeholder="Password" style="width:350px;height:36px;" name="password">
                    </div>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
                </form>
            </div>

        </div>
    </div>
    <!--===================================================-->


    <!-- DEMO PURPOSE ONLY -->
    <!--===================================================-->
    {{--<div class="demo-bg">--}}
        {{--<div id="demo-bg-list">--}}
            {{--<div class="demo-loading"><i class="psi-repeat-2"></i></div>--}}
            {{--<img class="demo-chg-bg bg-trans active" src="img/bg-img/thumbs/bg-trns.jpg" alt="Background Image">--}}
            {{--<img class="demo-chg-bg" src="img/bg-img/thumbs/bg-img-1.jpg" alt="Background Image">--}}
            {{--<img class="demo-chg-bg" src="img/bg-img/thumbs/bg-img-2.jpg" alt="Background Image">--}}
            {{--<img class="demo-chg-bg" src="img/bg-img/thumbs/bg-img-3.jpg" alt="Background Image">--}}
            {{--<img class="demo-chg-bg" src="img/bg-img/thumbs/bg-img-4.jpg" alt="Background Image">--}}
            {{--<img class="demo-chg-bg" src="img/bg-img/thumbs/bg-img-5.jpg" alt="Background Image">--}}
            {{--<img class="demo-chg-bg" src="img/bg-img/thumbs/bg-img-6.jpg" alt="Background Image">--}}
            {{--<img class="demo-chg-bg" src="img/bg-img/thumbs/bg-img-7.jpg" alt="Background Image">--}}
            {{--</div>--}}
        {{--</div>--}}
    <!--===================================================-->



</div>
<!--===================================================-->
<!-- END OF CONTAINER -->


</body>
</html>
