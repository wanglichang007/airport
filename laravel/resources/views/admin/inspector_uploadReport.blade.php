@extends('admin/header')
{{--<link href="{{ url('/admin/plugins/dropzone/dropzone.min.css') }}" rel="stylesheet">--}}
{{--<script src="{{ url('/admin/plugins/dropzone/dropzone.min.js') }}"></script>--}}
{{--<script src="{{ url('/admin/js/demo/form-file-upload.js') }}"></script>--}}
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
@section('container')
    @parent
    <form action="{{ route('admin.inspector.updateReport') }}" method="post" enctype="multipart/form-data" id="upload-license">
        {{ csrf_field() }}
        <div class="panel" style="margin-top:10px;padding-top:17px;width:96%;margin-left:2%;">
            <div class="form-group" style="margin-left:10%">
                <span style="color:black">System ID:</span> <input type="text" value="{{ $dataModel['system_id'] }}" class="form-control" style="height:50px;margin-right: 10px;width:10%;display:inline;color:blue" disabled="disabled">
                <span style="color:black">Form ID:</span> <input type="text" value="{{ $dataModel['from_id'] }}" class="form-control" style="height:50px;margin-right: 10px;width:10%;display:inline;color:blue" disabled="disabled">
                @if (!empty($dataModel['incident_report_time']))
                    <span style="color:black">Incident Report Date:</span> <input type="text" name="incident_report_time" value="{{ date("Y-m-d", $dataModel['incident_report_time']) }}" class="form-control" style="height:50px;width:20%;display:inline;color:blue" >
                @else
                    <span style="color:black">Incident Report Date:</span> <input type="text" name="incident_report_time" value="" id="datepicker" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;color:blue">
                @endif

            </div>
            <div class="form-group" style="margin-left:10%">
                <span style="color:black">Date Time:</span> <input type="text" class="form-control" id="date-time" style="height:50px;width:20%;display:inline;color:blue" disabled="disabled">
            </div>
        </div>
        <div class="row pad-ver">
            <input type="hidden" name="system_log_id" value="{{ $dataModel['system_log_id'] }}">
            <div class="container">
                <div class="row" style="width:80%;margin-left:15%">
                    <div class="col-lg-8 col-sm-6 col-12">
                        <div class="input-group">
                            <input type="text" id="filenames" class="form-control" style="height:50px;" οnkeydοwn="return false;" οnpaste="return false;" placeholder="Incident Report Image...">
                            <label class="input-group-btn">
                            <span class="btn btn-primary" style="height:50px;line-height:35px;">
                                <i class="glyphicon glyphicon-folder-open"></i>
                                Browse… <input type="file" name="image_url" onchange="loadFile(this.files[0])" style="display: none;" multiple>
                            </span>
                            </label>
                            <label class="input-group-btn">
                            <span id="save" class="btn btn-primary" style="height:50px;line-height:35px;margin-left: 10px;z-index:1">
                                Upload And Save
                            </span>
                            </label>
                            {{--                        <label class="input-group-btn">--}}
                            {{--                            <span id="save" class="btn btn-primary" style="height:50px;line-height:35px;margin-left: 10px;z-index:1">--}}
                            {{--                                Save--}}
                            {{--                            </span>--}}
                            {{--                        </label>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (!empty($dataModel['view_incident_image']))
            <div style="margin-top:10px;text-align: center " id="images">
                <div style="">
                    <img id="img_show" src="{{ $dataModel['view_incident_image'] }}">
                    <input id="img_value" type="hidden" name="view_image_url" value="{{ $dataModel['incident_image'] }}" />
                </div>
            </div>
        @endif
    </form>
@endsection

@section('menu')
    @parent
    <li class="active-link">
        <a href="javascript:history.back()">
            <i class="demo-psi-home"></i>
            <span class="menu-title">
                <strong>Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
@endsection
<script>
    function delImage() {
        var msg = "您确定要删除吗？";
        if (confirm(msg)==true){
            $('#img_value').val('');
            $('#img_show').remove();
            return true;
        }else{
            return false;
        }
    }

    function loadFile(file){
        $("#filenames").val(file.name)
    }

    function onTimes(){
        setInterval(function(){
            var dates = new Date().format("yyyy-MM-dd hh:mm:ss");
            $('#date-time').val(dates);
        }, 1000)
    }

    Date.prototype.format = function(format) {
        /*
         * eg:format="YYYY-MM-dd hh:mm:ss";

         */
        var o = {
            "M+" :this.getMonth() + 1, // month
            "d+" :this.getDate(), // day
            "h+" :this.getHours(), // hour
            "m+" :this.getMinutes(), // minute
            "s+" :this.getSeconds(), // second
            "q+" :Math.floor((this.getMonth() + 3) / 3), // quarter
            "S" :this.getMilliseconds()
            // millisecond
        }
        if (/(y+)/.test(format)) {
            format = format.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for ( var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                    : ("00" + o[k]).substr(("" + o[k]).length));
            }
        }
        return format;
    }

    $(function(){
        jQuery.browser = {};
        jQuery.browser.msie = false;
        jQuery.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
        $( "#datepicker" ).datepicker();

        onTimes();

        $("#save").click(function(){
            var dateval = $('input[name="incident_report_time"]').val();
            if (!dateval) {
                alert("Incident Report Date Not Null!");
                return false;
            }

            $("#upload-license").submit();
            return false;
        });
    })
</script>

