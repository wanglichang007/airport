@extends('admin/header')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
<style>
    table {
        border-bottom: 1.1px solid #999ea2;
    }
    tr {
        border-bottom: 1.1px solid #999ea2
    }
    td {
        border-bottom: 1.1px solid #999ea2;
    }
    th {
        border-bottom: 1.1px solid #999ea2;
    }
</style>
@section('container')
    @parent
    <input type="hidden" name="infoChartsJson" value="{{ $infoChartsJson }}" id="infoChartsJson">
    <input type="hidden" name="ratioChartsJson" value="{{ $ratioChartsJson }}" id="ratioChartsJson">
    <div class="panel" style="margin-top:50px;padding-top:10px;width:96%;margin-left:2%;">
        <form action="{{ route('admin.hkaastaff.summary') }}" method="get" id="search-summary">
            <input type="hidden" name="type" value="{{ $type }}">
            <input type="hidden" name="brand" value="{{ $brand }}">
            <div class="form-group" style="text-align: center;margin-top: 10px;">
                @if (!empty($startTime))
                    <span style="color:black">Start Date:</span> <input type="text" name="start_time" value="{{ date("Y-m-d", $startTime) }}" id="start_datepicker" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;background-color:#eee;color:blue" >
                @else
                    <span style="color:black">Start Date:</span> <input type="text" name="start_time" value="" id="datepicker" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;background-color:#eee;color:blue">
                @endif
                &nbsp&nbsp
                @if (!empty($endTime))
                    <span style="color:black">End Date:</span> <input type="text" name="end_time" value="{{ date("Y-m-d", $endTime) }}" id="end_datepicker" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;background-color:#eee;color:blue" >
                @else
                    <span style="color:black">End Date:</span> <input type="text" name="end_time" value="" id="datepicker" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;background-color:#eee;color:blue">
                @endif
                <span style="color:black">Date Time:</span> <input type="text" disabled="disabled" name="DateTimes" class="form-control" id="date-time" style="height:50px;margin-right: 10px;width:20%;display:inline;color:blue">
                <label class="input-group-btn" style="width:10%;display:inline">
                        <span id="search" class="btn btn-primary" style="height:50px;line-height:35px;">
                            Search
                        </span>
                </label>
            </div>
        </form>
    </div>

    <div class="panel" style="margin-top:10px;padding-top:10px;width:96%;margin-left:2%;">
        <div class="form-group" style="margin-top: 10px;margin-left:20px;">
            <div style="height:50px;margin-right: 100px;width:20%;display:inline;color:black;font-weight:bold;font-size: 20px;">Summary: &nbsp&nbsp @if(!empty($typeInfo)) {{ $typeInfo['name'] }} @endif</div>
            @if(!empty($brandInfo))
                <div style="height:50px;margin-right: 100px;width:20%;display:inline;color:black;font-weight:bold;font-size: 20px;">Brand Name: &nbsp&nbsp @if(!empty($brandInfo)) {{ $brandInfo['name'] }} @endif</div>
            @endif
        </div>
    </div>

    <div class="panel" style="margin-top:10px;padding-top:10px;width:96%;margin-left:2%;">
        <div class="panel-body">
            <div style="width:45%;float:left;color:black;font-weight:bold;font-size: 20px;">Quantity Distribution</div>
            <div style="width:45%;float:left;margin-left:10%;color:black;font-weight:bold;font-size: 20px;">Percentage Distribution</div>
        </div>
        <div class="panel-body">
            <div id="charts" style="width:45%;float:left"></div>
            <div id="charts-one" style="width:45%;float:left;margin-left:10%"></div>
        </div>
    </div>

    <div class="panel" style="margin-top:20px;width:96%;margin-left:2%;padding-top:20px;">
        <div class="form-group" style="margin-left:10px;">
            <div style="margin-right: 100px;width:20%;display:inline;color:black;font-weight:bold;font-size: 20px;">HKAA Progress Summary</div>
        </div>
        <div class="panel-body">
            <table id="demo-dt-addrow" class="table table-striped" cellspacing="0" width="100%;">
                <thead>
                <tr style="background-color: white;">
                    <th style="text-align: center;width:50%"></th>
                    <th style="text-align: center;">Quantity</th>
                    <th style="text-align: center">%</th>
                    <th style="text-align: center">Use Permit</th>
                    <th style="text-align: center">Incident Report</th>
                </tr>
                </thead>
                <tbody id="from-id-content">
                @if (!empty($info))
                    <tr style="background-color: white;">
                        <td style="text-align: left;"><a style="color:blue;font-size: 18px;" href="{{ route('admin.hkaastaff.totalNumber', ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand]) }}">1 Total number of Maintenance Checklist</a></td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $info['totalNumber'] }}</td>
                        <td style="text-align: center;color:black;font-size: 18px;">100</td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $info['totalUserPermitNumber'] }}</td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $info['totalIncidentReportNumber'] }}</td>
                    </tr>

                    <tr style="background-color: white;">
                        <td style="text-align: left;"><a style="color:blue;font-size: 18px;" href="{{ route('admin.hkaastaff.normalConditions', ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand]) }}">2 Normal Conditions</a></td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $info['normalConditionsNumber'] }}</td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $ratio['normalConditionsRatio'] }}</td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $info['normalConditionsUserPermitNumber'] }}</td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $info['normalConditionsIncidentReportNumber'] }}</td>
                    </tr>

                    <tr style="background-color: white;">
                        <td style="text-align: left;"><a style="color:blue;font-size: 18px;" href="{{ route('admin.hkaastaff.followUpActions', ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand]) }}">3 Checklist with Follow Up Actions</a></td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $info['followUpActionsNumber'] }}</td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $ratio['followUpActionsRatio'] }}</td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $info['followUpActionsUserPermitNumber'] }}</td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $info['followUpActionsIncidentReportNumber'] }}</td>
                    </tr>

                    <tr style="background-color: white;">
                        <td style="text-align: left;"><a style="color:blue;font-size: 18px;" href="{{ route('admin.hkaastaff.pendingSubmission', ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand]) }}">4 Checklist Pending Submission</a></td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $info['pendingSubmissionNumber'] }}</td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $ratio['pendingSubmissionRatio'] }}</td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $info['pendingSubmissionUserPermitNumber'] }}</td>
                        <td style="text-align: center;color:black;font-size: 18px;">{{ $info['pendingSubmissionIncidentReportNumber'] }}</td>
                    </tr>
                @else
                    <tr style="background-color: white;">
                        <td style="text-align: center;color:black;font-size: 18px;">1</td>
                        <td style="color:black;font-size: 18px;">Total number of Maintenance Checklist</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                    </tr>

                    <tr style="background-color: white;">
                        <td style="text-align: center;color:black;font-size: 18px;">2</td>
                        <td style="color:black;font-size: 18px;">Normal Conditions</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                    </tr>

                    <tr style="background-color: white;">
                        <td style="text-align: center;color:black;font-size: 18px;">3</td>
                        <td style="color:black;font-size: 18px;">Checklist with Follow Up Actions</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                    </tr>

                    <tr style="background-color: white;">
                        <td style="text-align: center;color:black;font-size: 18px;">4</td>
                        <td style="color:black;font-size: 18px;">Checklist Pending Submission</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                        <td style="text-align: center;color:black;font-size: 18px;">0</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('menu')
    @parent
    <li class="active-link">
        <a href="{{ route('admin.hkaastaff') }}">
            <i class="fa fa-chevron-left"></i>
            <span class="menu-title">
                <strong style="color: white;">Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link" style="background-color: #658dba">
        <a href="javascript://">
            <i class="fa fa-line-chart"></i>
            <span class="menu-title">
                <strong>Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.hkaastaff.individual') }}">
            <i class="fa fa-wrench"></i>
            <span class="menu-title">
                <strong style="color: white;">Individual ID</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.hkaastaff.sendSummary', ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand]) }}">
            <i class="fa fa-envelope-o"></i>
            <span class="menu-title">
                <strong style="color: white;">Email Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.hkaastaff.downloadSummary", ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand]) }}">
            <i class="fa fa-file-excel-o"></i>
            <span class="menu-title">
                <strong style="color: white;">Export Excel</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.hkaastaff.downloadCrontab") }}">
            <i class="fa fa-cloud-download"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Monthly Schedule</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://cdn.highcharts.com.cn/highcharts/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
    function onTimes(){
        setInterval(function(){
            var dates = new Date().format("yyyy-MM-dd hh:mm:ss");
            $('#date-time').val(dates);
        }, 1000)
    }

    Date.prototype.format = function(format) {
        /*
         * eg:format="YYYY-MM-dd hh:mm:ss";

         */
        var o = {
            "M+" :this.getMonth() + 1, // month
            "d+" :this.getDate(), // day
            "h+" :this.getHours(), // hour
            "m+" :this.getMinutes(), // minute
            "s+" :this.getSeconds(), // second
            "q+" :Math.floor((this.getMonth() + 3) / 3), // quarter
            "S" :this.getMilliseconds()
            // millisecond
        }
        if (/(y+)/.test(format)) {
            format = format.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for ( var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                    : ("00" + o[k]).substr(("" + o[k]).length));
            }
        }
        return format;
    }

    $(function(){
        onTimes();

        var infoChartsJson = $('#infoChartsJson').val();
        var infoChartsJsonObj = JSON.parse(infoChartsJson);
        var ratioChartsJson = $('#ratioChartsJson').val();
        var ratioChartsJsonObj = JSON.parse(ratioChartsJson);

        Highcharts.setOptions({
            colors: ['#01BAF2', '#71BF45', '#FAA74B']
        });
        Highcharts.chart('charts', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Total <br>'+"<?php echo !empty($info['totalNumber']) ? $info['totalNumber'] : 0 ?>",
                y:210
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            legend:{
                enabled:false
            },
            // tooltip: {
            //     pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            // },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        formatter:function(){
                            return this.key+ ': ' + this.y;
                        }
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Composition',
                colorByPoint: true,
                innerSize: '70%',
                data: infoChartsJsonObj
            }]
        });



        Highcharts.setOptions({
            colors: ['#01BAF2', '#71BF45', '#FAA74B']
        });
        Highcharts.chart('charts-one', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Total <br>'+"<?php echo !empty($info['totalNumber']) ? "100%" : "0%" ?>",
                y:210
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            legend:{
                enabled:false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        formatter:function(){
                            return this.key+ ': ' + this.y + '%';
                        }
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Composition',
                colorByPoint: true,
                innerSize: '70%',
                data: ratioChartsJsonObj
            }]
        });



        jQuery.browser = {};
        jQuery.browser.msie = false;
        jQuery.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
        $( "#start_datepicker" ).datepicker();
        $( "#end_datepicker" ).datepicker();

        $('#search').click(function(){
            $('#search-summary').submit();
            return false;
        })
    })
</script>
