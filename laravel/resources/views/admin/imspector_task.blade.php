@extends('admin/header')
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
    table {
        border:1.1px solid #999ea2;
    }
    tbody tr {
        border:1.1px solid #999ea2
    }
    td {
        border:1.1px solid #999ea2;
    }
    th {
        border:1.1px solid #999ea2;
    }

    .tableFixHead {

        width: 100%;

        table-layout: fixed;

        border-collapse: collapse;

    }

    .tableFixHead tbody {

        display: block;

        width: 100%;

        overflow: auto;

        overflow-y:scroll;

    }

    .tableFixHead thead, tbody tr {

        display: block;

    }

    .tableFixHead th,

    .tableFixHead  td {

        /*padding: 5px 10px;*/

        /*width: 200px;*/

    }

    th {

        /*background: #daebfb;*/

    }

    .backgroundColorRed {
        background-color: red !important;
    }

    /*table thead {*/
    /*    width: calc( 100% - 1em )*/
    /*}*/
</style>
@section('container')
    @parent
    <div id="top-content" style="height: auto">
        <div class="panel" style="margin-top:10px;padding-top:17px;width:96%;margin-left:2%;">
            <div class="form-group" style="text-align: center;">
                <span style="color:black">System ID:</span> <input type="text" value="{{ $systemLogInfo['system_id'] }}" class="form-control" style="height:50px;margin-right: 10px;width:10%;display:inline;color:blue" disabled="disabled">
                <span style="color:black">Form ID:</span> <input type="text" value="{{ $systemLogInfo['from_id'] }}" class="form-control" style="height:50px;margin-right: 10px;width:10%;display:inline;color:blue" disabled="disabled">
                <span style="color:black">Date Time:</span> <input type="text" class="form-control" id="date-time" style="height:50px;width:20%;display:inline;color: blue" disabled="disabled">
            </div>
        </div>
        <form action="{{ route('admin.inspector.task') }}" method="post" enctype="multipart/form-data" id="save-task">
            {{ csrf_field() }}
            <input type="hidden" name="system_log_id" value="{{ $systemLogInfo['system_log_id'] }}">
            <div class="panel" style="margin-top:20px;width:96%;margin-left:2%;">
                <h3 style="margin-left:20px;">Maintenance Check List</h3>
                <div class="panel-body tableFixHead">
                    <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0">
                        <thead style="border-top:1.1px solid #999ea2;border-left:1.1px solid #999ea2;border-right:1.1px solid #999ea2;">
                        <tr>
                            <th id="thead-id" style="text-align: center;">ID</th>
                            <th id="thead-red" style="text-align: center;">Clause Ref.</th>
                            <th id="thead-name" style="text-align: center;">Task/任務描述</th>
                            <th id="thead-yes" style="text-align: center;">Yes</th>
                            <th id="thead-no" style="text-align: center;">No</th>
                            <th id="thead-na" style="text-align: center;">NA</th>
                            <th id="thead-remarks" style="text-align: center;">Remarks</th>
                            <th id="thead-photo" style="text-align: center;">Photo upload</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($taskInfo as $key=>$value)
                                <tr>
                                    @if(in_array($value['task_id'], array_keys($systemLogInfo['task'])))
                                        <input type="hidden" name="data[{{ $value['task_id'] }}][task_id]" value="{{ $value['task_id'] }}">
                                        <td class="tbody-id" style="text-align: center;">{{ $key+1 }}</td>
                                        <td class="tbody-red" style="text-align: center;">{{ $value['clause_no'] }}</td>
                                        <td class="tbody-name" align="left" style="color: black">{{ $value['name'] }}</td>
                                        @if(!empty($systemLogInfo['task'][$value['task_id']]['status']))
                                            <td class="tbody-yes" style="text-align: center;"><input style="zoom: 150%" type="checkbox" @if($systemLogInfo['task'][$value['task_id']]['status'] == 1) checked @endif value="1" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                            <td class="tbody-no" style="text-align: center;"><input style="zoom: 150%" type="checkbox" @if($systemLogInfo['task'][$value['task_id']]['status'] == 2) checked @endif value="2" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                            <td class="tbody-na" style="text-align: center;"><input style="zoom: 150%" type="checkbox" @if($systemLogInfo['task'][$value['task_id']]['status'] == 3) checked @endif value="3" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                        @else
                                            <td class="tbody-yes" style="text-align: center;"><input style="zoom: 150%" type="checkbox" value="1" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                            <td class="tbody-no" style="text-align: center;"><input style="zoom: 150%" type="checkbox" value="2" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                            <td class="tbody-na" style="text-align: center;"><input style="zoom: 150%" type="checkbox" value="3" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                        @endif
                                        <td class="tbody-remarks" style="text-align: center;"><textarea cols="6" rows="1" name="data[{{ $value['task_id'] }}][remarks]" style="color: #579EED">@if(!empty($systemLogInfo['task'][$value['task_id']]['remarks'])) {{ $systemLogInfo['task'][$value['task_id']]['remarks'] }} @endif</textarea></td>
                                        <td class="tbody-photo" style="text-align: center;">
                                            {{--                                <input type="file" name="file" id="uploadImage" onchange="postData(this)">--}}
{{--                                            <label class="input-group-btn">--}}
{{--                                                <span class="btn btn-primary">--}}
{{--                                                    <i class="glyphicon glyphicon-folder-open"></i>--}}
{{--                                                    Browse… <input type="file" name="file" onchange="postData(this, {{ $value['task_id'] }})" style="display: none;" multiple>--}}
{{--                                                </span>--}}
{{--                                            </label>--}}
{{--                                            @if(!empty($systemLogInfo['task'][$value['task_id']]['image']))--}}
{{--                                                <div class="src-image"><a target="_blank" href="{{ $systemLogInfo['task'][$value['task_id']]['view_image'] }}"><img src="{{ $systemLogInfo['task'][$value['task_id']]['view_image'] }}" style="margin-top:5px;" width="50px;" height="50px;"></a><input type="hidden" name="data[{{$value['task_id']}}][image]" value="{{ $systemLogInfo['task'][$value['task_id']]['image'] }}"></div>--}}
{{--                                            @endif--}}
                                            <a href="{{ route('admin.inspector.image', ['system_log_id' => $systemLogInfo['system_log_id'], 'task_id' => $value['task_id'], 'type' => 1]) }}">
                                                <label class="input-group-btn" style="margin-bottom:-1px;text-align: center;">
                                                    @if($value['inspector_upload_image'] == true)
                                                        <span id="search" class="btn btn-primary" style="height:25px;line-height:13px;">
                                                            View
                                                        </span>
                                                    @else
                                                        <span id="search" class="btn btn-success" style="height:25px;line-height:13px;">
                                                            Upload
                                                        </span>
                                                    @endif
                                                </label>
                                            </a>
                                        </td>
                                    @else
                                        <input type="hidden" name="data[{{ $value['task_id'] }}][task_id]" value="{{ $value['task_id'] }}">
                                        <td class="tbody-id" style="text-align: center;">{{ $key+1 }}</td>
                                        <td class="tbody-red" style="text-align: center;">{{ $value['clause_no'] }}</td>
                                        <td class="tbody-name" align="left" style="color: black">{{ $value['name'] }}</td>
                                        <td class="tbody-yes" style="text-align: center;"><input style="zoom: 150%" type="checkbox" value="1" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                        <td class="tbody-no" style="text-align: center;"><input style="zoom: 150%" type="checkbox" value="2" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                        <td class="tbody-na" style="text-align: center;"><input style="zoom: 150%" type="checkbox" value="3" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                        <td class="tbody-remarks" style="text-align: center;"><textarea cols="6" rows="1" name="data[{{ $value['task_id'] }}][remarks]" style="color: #579EED"></textarea></td>
                                        <td class="tbody-photo" style="text-align: center;">
                                            {{--                                <input type="file" name="file" id="uploadImage" onchange="postData(this)">--}}
{{--                                            <label class="input-group-btn">--}}
{{--                                                <span class="btn btn-primary">--}}
{{--                                                    <i class="glyphicon glyphicon-folder-open"></i>--}}
{{--                                                    Browse… <input type="file" name="file" onchange="postData(this, {{ $value['task_id'] }})" style="display: none;" multiple>--}}
{{--                                                </span>--}}
{{--                                            </label>--}}
                                            <a href="{{ route('admin.inspector.image', ['system_log_id' => $systemLogInfo['system_log_id'], 'task_id' => $value['task_id'], 'type' => 1]) }}">
                                                <label class="input-group-btn" style="margin-bottom:-1px;text-align: center;">
                                                    @if($value['inspector_upload_image'] == true)
                                                        <span id="search" class="btn btn-primary" style="height:25px;line-height:13px;">
                                                            View
                                                        </span>
                                                    @else
                                                        <span id="search" class="btn btn-success" style="height:25px;line-height:13px;">
                                                            Upload
                                                        </span>
                                                    @endif
                                                </label>
                                            </a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <span id="save" class="btn btn-primary" style="height:50px;line-height:35px;float:right;font-size:15px;">
                        Save And Next
                    </span>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('menu')
    @parent
{{--    <li class="active-link">--}}
{{--        <a href="{{ route('admin.inspector') }}">--}}
{{--            <i class="demo-psi-home"></i>--}}
{{--            <span class="menu-title">--}}
{{--                <strong>Home</strong>--}}
{{--            </span>--}}
{{--        </a>--}}
{{--    </li>--}}
    <li class="active-link">
        <a href="javascript:history.go(-1);">
            <i class="fa fa-chevron-left"></i>
            <span class="menu-title">
                <strong style="color: white;">Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.inspector.license', ['system_log_id' => $systemLogInfo['system_log_id']]) }}">
            <i class="fa fa-newspaper-o"></i>
            <span class="menu-title">
                <strong style="color: white;">USE Permit</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.inspector.report', ['system_log_id' => $systemLogInfo['system_log_id']]) }}">
            <i class="fa fa-list-alt"></i>
            <span class="menu-title">
                <strong style="color: white;">Incident Report</strong>
            </span>
        </a>
    </li>
    <li class="active-link" style="background-color: #658dba">
        <a href="javascript://">
            <i class="fa fa-table"></i>
            <span class="menu-title">
                <strong>Task Check List</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.inspector.download", ['system_log_id' => $systemLogInfo['system_log_id']]) }}">
            <i class="fa fa-file-excel-o"></i>
            <span class="menu-title">
                <strong style="color: white;">Export Excel</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
{{--    <li class="active-link">--}}
{{--        <a href="javascript://" id="save">--}}
{{--            <i class="demo-psi-home"></i>--}}
{{--            <span class="menu-title">--}}
{{--                <strong>Save And Next</strong>--}}
{{--            </span>--}}
{{--        </a>--}}
{{--    </li>--}}
@endsection

<script>
    function postData(self, task_id) {
        var formData = new FormData();
        formData.append("file", $(self)[0].files[0]);
        var url = '{{ route("admin.upload") }}';
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "post",
            data: formData,
            processData: false, // 告诉jQuery不要去处理发送的数据
            contentType: false, // 告诉jQuery不要去设置Content-Type请求头
            dataType: 'text',
            success: function(data) {
                var params = JSON.parse(data);
                var image_url = '<a target="_blank" href="'+params.viewPath+'"><img src="'+params.viewPath+'" style="margin-top:5px;" width="50px;" height="50px;"></a><input type="hidden" name="data['+task_id+'][image]" value="'+params.filePath+'">';
                $(self).parents("td").find('.src-image').remove();
                $(self).parents("td").append(image_url);
            },
            error: function(data) {

            }
        });
    }

    function onTimes(){
        setInterval(function(){
            var dates = new Date().format("yyyy-MM-dd hh:mm:ss");
            $('#date-time').val(dates);
        }, 1000)
    }

    Date.prototype.format = function(format) {
        /*
         * eg:format="YYYY-MM-dd hh:mm:ss";

         */
        var o = {
            "M+" :this.getMonth() + 1, // month
            "d+" :this.getDate(), // day
            "h+" :this.getHours(), // hour
            "m+" :this.getMinutes(), // minute
            "s+" :this.getSeconds(), // second
            "q+" :Math.floor((this.getMonth() + 3) / 3), // quarter
            "S" :this.getMilliseconds()
            // millisecond
        }
        if (/(y+)/.test(format)) {
            format = format.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for ( var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                    : ("00" + o[k]).substr(("" + o[k]).length));
            }
        }
        return format;
    }

    function setHeadWidth(){
        var thead_width = $("thead").width();

        var thead_id_width = 5 / 100 * thead_width;
        $('#thead-id').css("width", thead_id_width+'px');

        var thead_red_width = 15 / 100 * thead_width;
        $('#thead-red').css("width", thead_red_width+'px');

        var thead_name_width =  45 / 100  * thead_width;
        $('#thead-name').css("width", thead_name_width+'px');

        var thead_yes_width = 5 / 100 * thead_width;
        $('#thead-yes').css("width", thead_yes_width+'px');

        var thead_no_width = 5 / 100 * thead_width;
        $('#thead-no').css("width", thead_no_width+'px');

        var thead_na_width = 5 / 100 * thead_width;
        $('#thead-na').css("width", thead_na_width+'px');

        var thead_remarks_width = 10 / 100 * thead_width;
        $('#thead-remarks').css("width", thead_remarks_width+'px');

        var thead_photo_width = 10 / 100 * thead_width;
        $('#thead-photo').css("width", thead_photo_width+'px');

        $("tbody tr .tbody-id").css("width", thead_id_width+'px');
        $("tbody tr .tbody-red").css("width", thead_red_width+'px');
        $("tbody tr .tbody-name").css("width", thead_name_width+'px');
        $("tbody tr .tbody-yes").css("width", thead_yes_width+'px');
        $("tbody tr .tbody-no").css("width", thead_no_width+'px');
        $("tbody tr .tbody-na").css("width", thead_na_width+'px');
        $("tbody tr .tbody-remarks").css("width", thead_remarks_width+'px');
        $("tbody tr .tbody-photo").css("width", thead_photo_width+'px');
    }

    function checkTask(){
        var checked = false;
        $("tbody tr").each(function(){
            var yes_check = false;
            var no_check = false;
            var na_check = false;

            if ($(this).find('.tbody-yes').find("input[type='checkbox']").is(':checked') == true) {
                yes_check = true;
            }
            if ($(this).find('.tbody-no').find("input[type='checkbox']").is(':checked') == true) {
                no_check = true;
            }
            if ($(this).find('.tbody-na').find("input[type='checkbox']").is(':checked') == true) {
                na_check = true;
            }

            if (yes_check == false && no_check == false && na_check == false) {
                $(this).addClass('backgroundColorRed');
                checked = true;
            } else {
                $(this).removeClass('backgroundColorRed');
            }
        })

        return checked;
    }

    $(function(){
        onTimes();

        $(".status").change(function(){
            if ($(this).is(':checked')) {
                $(this).parents('tr').find('.status').prop("checked", false);
                $(this).prop("checked", true);
            }
        });

        $('#save').click(function(){
            var checked = checkTask();
            if (checked == true) {
                alert('item undone!');
                return false;
            }
            $('#save-task').submit();
            return false;
        });

        $("tbody").height(window.innerHeight * 60 / 100)

        setHeadWidth();
    })
</script>
