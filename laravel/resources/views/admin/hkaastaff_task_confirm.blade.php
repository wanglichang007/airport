@extends('admin/header')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
    table {
        border:1.1px solid #999ea2;
    }
    tr {
        border:1.1px solid #999ea2
    }
    td {
        border:1.1px solid #999ea2;
    }
    th {
        border:1.1px solid #999ea2;
    }

    .tableFixHead {

        width: 100%;

        table-layout: fixed;

        border-collapse: collapse;

    }

    .tableFixHead tbody {

        display: block;

        width: 100%;

        overflow: auto;

        overflow-y:scroll;

    }

    .tableFixHead thead, tbody tr {

        display: block;

    }

    .tableFixHead th,

    .tableFixHead  td {

        /*padding: 5px 10px;*/

        /*width: 200px;*/

    }

    th {

        /*background: #daebfb;*/

    }
</style>
@section('container')
    @parent
    <div id="top-content">
        <div class="panel" style="margin-top:10px;padding-top:17px;width:96%;margin-left:2%;">
            <div class="form-group" style="text-align: center">
                <span style="color:black">System ID:</span> <input type="text" value="{{ $systemLogInfo['system_id'] }}" class="form-control" style="height:50px;margin-right: 10px;width:10%;display:inline;color:blue" disabled="disabled">
                <span style="color:black">Form ID:</span> <input type="text" value="{{ $systemLogInfo['from_id'] }}" class="form-control" style="height:50px;margin-right: 10px;width:10%;display:inline;color:blue" disabled="disabled">
                <span style="color:black">Date Time:</span> <input type="text" class="form-control" id="date-time" style="height:50px;width:20%;display:inline;color: blue" disabled="disabled">
            </div>
        </div>
        <form action="{{ route('admin.hkaastaff.task') }}" method="post" enctype="multipart/form-data" id="save-task">
            {{ csrf_field() }}
            <input type="hidden" name="system_log_id" value="{{ $systemLogInfo['system_log_id'] }}">
            <div class="panel" style="margin-top:20px;width:96%;margin-left:2%;">
                <div>
                    <div class="col-sm-3" style="float:right;margin-right:10px;margin-bottom:10px;">
                        <h3 style="color:black;font-weight:bold;">HKAA Remarks</h3>
                        <textarea disabled="disabled" name="hkaa_remarks" rows="5" class="form-control">{{ $systemLogInfo['hkaa_remarks'] }}</textarea>
                    </div>
                    <div class="col-sm-12" style="float:right;margin-right:10px;margin-bottom:10px;">
                        <h3><span style="margin-left: 25px;color:blue">User ID： {{ $systemLogInfo['sign_name'] }}, Submission Date：{{ date("Y-m-d H:i:s", $systemLogInfo['sign_time']) }}</span></h3>
                    </div>
                </div>
                <div class="panel-body tableFixHead">
                    <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0">
                        <thead>
                        <tr>
                            <th colspan="8" style="text-align: center;font-size:24px;line-height: 65px;">Inspector Record</th>
                            <th colspan="2" style="text-align: center;font-size:24px;font-style: oblique;">Supervisor Remarks</th>
                        </tr>
                        <tr>
                            <th id="thead-id" style="text-align: center">ID</th>
                            <th id="thead-red" style="text-align: center;">Clause Ref.</th>
                            <th id="thead-name" style="text-align: center;width:60%">Task/任務描述</th>
                            <th id="thead-yes" style="text-align: center">Yes</th>
                            <th id="thead-no" style="text-align: center">No</th>
                            <th id="thead-na" style="text-align: center">NA</th>
                            <th id="thead-remarks" style="text-align: center;">Remarks</th>
                            <th id="thead-photo" style="text-align: center;">Photo upload</th>
                            <th id="thead-follow" style="text-align: center;font-style: oblique;">Follow Up</th>
                            <th id="thead-super-photo" style="text-align: center;font-style: oblique;">Photo Upload</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($taskInfo as $key=>$value)
                                @if(!empty($systemLogInfo['task'][$value['task_id']]['status']))
                                    @if($systemLogInfo['task'][$value['task_id']]['status'] == 2 || $systemLogInfo['task'][$value['task_id']]['status'] == 3 || !empty($systemLogInfo['task'][$value['task_id']]['supervisor_remarks']))
                                        <tr style="background-color: yellow">
                                    @else
                                        <tr>
                                    @endif
                                @else
                                    <tr>
                                    @endif
                                @if(in_array($value['task_id'], array_keys($systemLogInfo['task'])))
                                    <input type="hidden" name="data[{{ $value['task_id'] }}][task_id]" value="{{ $value['task_id'] }}">
                                    <td class="tbody-id" style="text-align: center;">{{ $key + 1 }}</td>
                                    <td class="tbody-red" style="text-align: center;">{{ $value['clause_no'] }}</td>
                                    <td class="tbody-name" align="left" style="width:60%;color: black">{{ $value['name'] }}</td>
                                    @if(!empty($systemLogInfo['task'][$value['task_id']]['status']))
                                        <td class="tbody-yes" style="text-align: center;"><input style="zoom: 150%" type="checkbox" disabled @if($systemLogInfo['task'][$value['task_id']]['status'] == 1) checked @endif value="1" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                        <td class="tbody-no" style="text-align: center;"><input style="zoom: 150%" type="checkbox" disabled @if($systemLogInfo['task'][$value['task_id']]['status'] == 2) checked @endif value="2" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                        <td class="tbody-na" style="text-align: center;"><input style="zoom: 150%" type="checkbox" disabled @if($systemLogInfo['task'][$value['task_id']]['status'] == 3) checked @endif value="3" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                    @else
                                        <td class="tbody-yes" style="text-align: center;"><input style="zoom: 150%" type="checkbox" disabled value="1" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                        <td class="tbody-no" style="text-align: center;"><input style="zoom: 150%" type="checkbox" disabled value="2" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                        <td class="tbody-na" style="text-align: center;"><input style="zoom: 150%" type="checkbox" disabled value="3" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                    @endif
                                    <td class="tbody-remarks" style="text-align: center;width:90px;text-overflow:ellipsis;">
                                        @if(!empty($systemLogInfo['task'][$value['task_id']]['remarks']))
                                            <input type="hidden" name="data[{{ $value['task_id'] }}][remarks]" value="{{ $systemLogInfo['task'][$value['task_id']]['remarks'] }}">
                                            <button onclick="javascript:return returnFalse()" class="btn btn-default add-tooltip" data-toggle="tooltip" data-container="body"
                                                    data-placement="left" data-original-title="{{ $systemLogInfo['task'][$value['task_id']]['remarks'] }}" style="width:90px;text-overflow:ellipsis;white-space:nowrap;overflow:hidden;color:#579EED">
                                                {{ $systemLogInfo['task'][$value['task_id']]['remarks'] }}</button>
                                        @endif
                                    </td>
                                    <td class="tbody-photo" style="text-align: center;">
                                        {{--                                        @if(!empty($systemLogInfo['task'][$value['task_id']]['image']))--}}
                                        {{--                                            <div class="src-image"><a target="_blank" href="{{ $systemLogInfo['task'][$value['task_id']]['view_image'] }}"><img src="{{ $systemLogInfo['task'][$value['task_id']]['view_image'] }}" style="margin-top:5px;" width="50px;" height="50px;"></a><input type="hidden" name="data[{{$value['task_id']}}][image]" value="{{ $systemLogInfo['task'][$value['task_id']]['image'] }}"></div>--}}
                                        {{--                                        @else--}}
                                        {{--                                            <input type="hidden" name="data[{{$value['task_id']}}][image]" value="">--}}
                                        {{--                                        @endif--}}
                                        @if($value['inspector_upload_image'] == true)
                                            <a href="{{ route('admin.hkaastaff.image', ['system_log_id' => $systemLogInfo['system_log_id'], 'task_id' => $value['task_id'], 'type' => 1]) }}">
                                                <label class="input-group-btn" style="margin-bottom:-1px;text-align: center;">
                                                <span id="search" class="btn btn-primary" style="height:25px;line-height:13px;">
                                                    View
                                                </span>
                                                </label>
                                            </a>
                                        @else
                                            <div style="background: #C0C0C0;opacity :0.7;width: 55px;height:25px;">
                                                View
                                            </div>
                                        @endif
                                    </td>
                                    <td class="tbody-follow">
                                        @if(!empty($systemLogInfo['task'][$value['task_id']]['supervisor_remarks']))
                                            <input type="hidden" name="data[{{ $value['task_id'] }}][remarks]" value="{{ $systemLogInfo['task'][$value['task_id']]['supervisor_remarks'] }}">
                                            <button onclick="javascript:return returnFalse()" class="btn btn-default add-tooltip" data-toggle="tooltip" data-container="body"
                                                    data-placement="left" data-original-title="{{ $systemLogInfo['task'][$value['task_id']]['supervisor_remarks'] }}" style="width:90px;text-overflow:ellipsis;white-space:nowrap;overflow:hidden;color: #579EED">
                                                {{ $systemLogInfo['task'][$value['task_id']]['supervisor_remarks'] }}</button>
                                        @endif
                                    </td>
                                    <td class="tbody-super-photo" style="text-align: center">
                                        {{--                                        @if(!empty($systemLogInfo['task'][$value['task_id']]['supervisor_image']))--}}
                                        {{--                                            <div class="src-image"><a target="_blank" href="{{ $systemLogInfo['task'][$value['task_id']]['view_supervisor_image'] }}"><img src="{{ $systemLogInfo['task'][$value['task_id']]['view_supervisor_image'] }}" style="margin-top:5px;" width="50px;" height="50px;"></a><input type="hidden" name="data[{{$value['task_id']}}][supervisor_image]" value="{{ $systemLogInfo['task'][$value['task_id']]['supervisor_image'] }}"></div>--}}
                                        {{--                                        @endif--}}
                                        @if($value['supervisor_upload_image'] == true)
                                            <a href="{{ route('admin.hkaastaff.image', ['system_log_id' => $systemLogInfo['system_log_id'], 'task_id' => $value['task_id'], 'type' => 2]) }}">
                                                <label class="input-group-btn" style="margin-bottom:-1px;text-align: center;">
                                                <span id="search" class="btn btn-primary" style="height:25px;line-height:13px;font-style: oblique;text-align: center">
                                                    View
                                                </span>
                                                </label>
                                            </a>
                                        @else
                                            <div style="background: #C0C0C0;opacity :0.7;width: 55px;height:25px;font-style: oblique;text-align: center;display: inline-block;">
                                                View
                                            </div>
                                        @endif
                                    </td>
                                    </tr>
                                @else
                                    <tr>
                                    <input type="hidden" name="data[{{ $value['task_id'] }}][task_id]" value="{{ $value['task_id'] }}">
                                        <td class="tbody-id" style="text-align: center;">{{ $key + 1 }}</td>
                                    <td class="tbody-red" style="text-align: center;">{{ $value['clause_no'] }}</td>
                                    <td class="tbody-name" align="left" style="width:60%;color: black">{{ $value['name'] }}</td>
                                    <td class="tbody-yes" style="text-align: center;"><input style="zoom: 150%" type="checkbox" disabled value="1" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                    <td class="tbody-no" style="text-align: center;"><input style="zoom: 150%" type="checkbox" disabled value="2" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                    <td class="tbody-na" style="text-align: center;"><input style="zoom: 150%" type="checkbox" disabled value="3" name="data[{{ $value['task_id'] }}][status]" class="status"></td>
                                    <td class="tbody-remarks" style="text-align: center;"></td>
                                    <td class="tbody-photo" style="text-align: center;">
                                        <div style="background: #C0C0C0;opacity :0.7;width: 55px;height:25px;">
                                            View
                                        </div>
                                    </td>
                                    <td class="tbody-follow"><textarea cols="10" rows="1" name="data[{{ $value['task_id'] }}][supervisor_remarks]" style="color: #579EED"></textarea></td>
                                    <td class="tbody-super-photo" style="text-align: center;">
                                        <div style="background: #C0C0C0;opacity :0.7;width: 55px;height:25px;font-style: oblique;text-align: center;display: inline-block;">
                                            View
                                        </div>
                                    </td>
                                    </tr>
                                @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
    <div id="middle-content" style="display: none">
        <highchart id="testchart" config="testconfig"></highchart>
    </div>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/5.0.7/highcharts.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<script src="https://github.com/devongovett/pdfkit/releases/download/v0.6.2/pdfkit.js"></script>
<script src="{{ url('/admin/js/jspdf.debug.js') }}"></script>
<script src="{{ url('/admin/js/rgbcolor.js') }}"></script>
<script src="{{ url('/admin/js/canvg.js') }}"></script>
<script src="{{ url('/admin/js/script.js') }}"></script>
@section('menu')
    @parent
    <li class="active-link">
        <a href="javascript:history.go(-1);">
            <i class="fa fa-chevron-left"></i>
            <span class="menu-title">
                <strong style="color: white;">Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.hkaastaff') }}">
            <i class="fa fa-line-chart"></i>
            <span class="menu-title">
                <strong style="color: white;">Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.hkaastaff.individual') }}">
            <i class="fa fa-wrench"></i>
            <span class="menu-title">
                <strong style="color: white;">Individual ID</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="javascript://" aria-expanded="true" style="background-color: #658dba">
            <i class="fa fa-list-alt"></i>
            <span class="menu-title">
                <strong>Task Check List</strong>
            </span>
        </a>
        <ul class="collapse in" aria-expanded="true" style="background-color:#212a33;">
            <li style="border: 1px solid #212a33;background-color: #658dba"><a href="javascript://" style="color:white;font-size: 13px;">Check List</a></li>
            <li style="color:white;"><a href="{{ route('admin.hkaastaff.checkUsePermit', ['system_log_id' => $systemLogInfo['system_log_id'], 'type' => 'confirm']) }}" style="font-size: 13px;">Use Permit</a></li>
            <li style="color:white;"><a href="{{ route('admin.hkaastaff.checkIncidentReport', ['system_log_id' => $systemLogInfo['system_log_id'], 'type' => 'confirm']) }}" style="font-size: 13px;">Incident Report</a></li>
        </ul>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.hkaastaff.send', ['system_log_id' => $systemLogInfo['system_log_id']]) }}">
            <i class="fa fa-envelope-o"></i>
            <span class="menu-title">
                <strong style="color: white;">Email Screen</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.hkaastaff.download", ['system_log_id' => $systemLogInfo['system_log_id']]) }}">
            <i class="fa fa-file-excel-o"></i>
            <span class="menu-title">
                <strong style="color: white;">Export Excel</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.hkaastaff.downloadCrontab") }}">
            <i class="fa fa-cloud-download"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Monthly Schedule</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
@endsection

<script>
    function postData(self, task_id) {
        var formData = new FormData();
        formData.append("file", $(self)[0].files[0]);
        var url = '{{ route("admin.upload") }}';
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "post",
            data: formData,
            processData: false, // 告诉jQuery不要去处理发送的数据
            contentType: false, // 告诉jQuery不要去设置Content-Type请求头
            dataType: 'text',
            success: function(data) {
                var params = JSON.parse(data);
                var image_url = '<a target="_blank" href="'+params.viewPath+'"><img src="'+params.viewPath+'" style="margin-top:5px;" width="50px;" height="50px;"></a><input type="hidden" name="data['+task_id+'][supervisor_image]" value="'+params.filePath+'">';
                $(self).parents("td").find('.src-image').remove();
                $(self).parents("td").append(image_url);
            },
            error: function(data) {

            }
        });
    }

    function returnFalse(){
        return false;
    }

    function onTimes(){
        setInterval(function(){
            var dates = new Date().format("yyyy-MM-dd hh:mm:ss");
            $('#date-time').val(dates);
        }, 1000)
    }

    Date.prototype.format = function(format) {
        /*
         * eg:format="YYYY-MM-dd hh:mm:ss";

         */
        var o = {
            "M+" :this.getMonth() + 1, // month
            "d+" :this.getDate(), // day
            "h+" :this.getHours(), // hour
            "m+" :this.getMinutes(), // minute
            "s+" :this.getSeconds(), // second
            "q+" :Math.floor((this.getMonth() + 3) / 3), // quarter
            "S" :this.getMilliseconds()
            // millisecond
        }
        if (/(y+)/.test(format)) {
            format = format.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for ( var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                    : ("00" + o[k]).substr(("" + o[k]).length));
            }
        }
        return format;
    }

    function setHeadWidth(){
        var thead_width = $("thead").width();

        var thead_id_width = 5 / 100 * thead_width;
        $('#thead-id').css("width", thead_id_width+'px');

        var thead_red_width = 10 / 100 * thead_width;
        $('#thead-red').css("width", thead_red_width+'px');

        var thead_name_width =  35 / 100  * thead_width;
        $('#thead-name').css("width", thead_name_width+'px');

        var thead_yes_width = 5 / 100 * thead_width;
        $('#thead-yes').css("width", thead_yes_width+'px');

        var thead_no_width = 5 / 100 * thead_width;
        $('#thead-no').css("width", thead_no_width+'px');

        var thead_na_width = 5 / 100 * thead_width;
        $('#thead-na').css("width", thead_na_width+'px');

        var thead_remarks_width = 10 / 100 * thead_width;
        $('#thead-remarks').css("width", thead_remarks_width+'px');

        var thead_photo_width = 5 / 100 * thead_width;
        $('#thead-photo').css("width", thead_photo_width+'px');

        var thead_follow_width = 10 / 100 * thead_width;
        $('#thead-follow').css("width", thead_follow_width+'px');

        var thead_super_photo_width = 10 / 100 * thead_width;
        $('#thead-super-photo').css("width", thead_super_photo_width+'px');


        $("tbody tr .tbody-id").css("width", thead_id_width+'px');
        $("tbody tr .tbody-red").css("width", thead_red_width+'px');
        $("tbody tr .tbody-name").css("width", thead_name_width+'px');
        $("tbody tr .tbody-yes").css("width", thead_yes_width+'px');
        $("tbody tr .tbody-no").css("width", thead_no_width+'px');
        $("tbody tr .tbody-na").css("width", thead_na_width+'px');
        $("tbody tr .tbody-remarks").css("width", thead_remarks_width+'px');
        $("tbody tr .tbody-photo").css("width", thead_photo_width+'px');
        $("tbody tr .tbody-follow").css("width", thead_follow_width+'px');
        $("tbody tr .tbody-super-photo").css("width", thead_super_photo_width+'px');
    }

    $(function(){
        onTimes();

        $(".status").change(function(){
            if ($(this).is(':checked')) {
                $(this).parents('tr').find('.status').prop("checked", false);
                $(this).prop("checked", true);
            }
        });

        $('#save').click(function(){
            $('.status').removeAttr("disabled");
            $('#save-task').submit();
            return false;
        });

        $("tbody").height(window.innerHeight * 60 / 100)

        setHeadWidth();
    })
</script>
