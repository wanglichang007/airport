@extends('admin/header')
@section('container')
    @parent
    <!--Page Title-->

    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="morris-chart-network" class="morris-full-content" style="display:none"></div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content" style="margin-top: 20px;">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Waiting for Review List</h3>
            </div>
            <div class="panel-body">
                <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="text-align: center">ID</th>
                        <th style="text-align: center">System ID</th>
                        <th style="text-align: center">From ID</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (!empty($data))
                        @foreach($data->items() as $value)
                            <tr>
                                <td style="text-align: center">{{ $value['system_log_id'] }}</td>
                                <td style="text-align: center">{{ $value['system_id'] }}</td>
                                <td style="text-align: center">{{ $value['from_id'] }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                @if (!empty($data))
                    {{ $data->links('manage.pages', ['data' => $data, $where]) }}
                @endif
            </div>
        </div>
    </div>
@endsection
@section('menu')
    @parent
    <li class="active-link">
        <a href="javascript:history.go(-1);">
            <i class="fa fa-chevron-left"></i>
            <span class="menu-title">
                <strong style="color: white;">Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.hkaastaff') }}">
            <i class="fa fa-line-chart"></i>
            <span class="menu-title">
                <strong style="color: white;">Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.hkaastaff.individual') }}">
            <i class="fa fa-wrench"></i>
            <span class="menu-title">
                <strong style="color: white;">Individual ID</strong>
            </span>
        </a>
    </li>
@endsection
