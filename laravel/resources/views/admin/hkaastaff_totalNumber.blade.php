@extends('admin/header')
@section('container')
    @parent
    <div class="panel" style="margin-top:10px;padding-top:17px;width:96%;margin-left:2%;text-align:center;">
        <div class="form-group">
            @if (!empty($startTime))
                <span style="color:black">Start Date:</span> <input type="text" disabled="disabled" name="start_time" value="{{ date("Y-m-d", $startTime) }}" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;color:blue" >
            @else
                <span style="color:black">Start Date:</span> <input type="text" disabled="disabled" name="start_time" value="" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;color:blue">
            @endif
            &nbsp&nbsp
            @if (!empty($endTime))
                <span style="color:black">End Date:</span> <input type="text" disabled="disabled" name="end_time" value="{{ date("Y-m-d", $endTime) }}" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;color:blue" >
            @else
                <span style="color:black">End Date:</span> <input type="text" disabled="disabled" name="end_time" value="" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;color:blue">
            @endif
            <span style="color:black">Date Time:</span> <input type="text" disabled="disabled" name="DateTimes" class="form-control" id="date-time" style="height:50px;margin-right: 10px;width:20%;display:inline;color:blue">
        </div>
    </div>
    <div class="panel" style="margin-top:20px;width:96%;margin-left:2%;padding-top:10px;">
        <h1 class="panel-title">
            <div style="height:50px;margin-right: 100px;width:20%;display:inline;color:black;font-weight:bold;font-size: 20px;">System Type: &nbsp&nbsp @if(!empty($typeInfo)) {{ $typeInfo['name'] }} @endif</div>
            @if(!empty($brandInfo))
                <div style="height:50px;margin-right: 100px;width:20%;display:inline;color:black;font-weight:bold;font-size: 20px;">Brand Name: &nbsp&nbsp @if(!empty($brandInfo)) {{ $brandInfo['name'] }} @endif</div>
            @endif
        </h1>
        <div class="panel-heading">
            <h1 class="panel-title">
                <span style="color:black;font-weight:bold;">1. Total number of Maintenance Checklist</span>
                <span style="float: right;color:#808080">Total Progress: {{ count($data['systemInfo']) }}</span>
            </h1>
        </div>
        <div class="panel-body">
            <table data-toggle="table" id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%;" data-sort-name="id">
                <thead>
                <tr>
                    <th data-field="system_id" data-align="center" data-sortable="true">
                        System ID
                    </th>
                    <th data-field="from_id" data-align="center" data-sortable="true">
                        Form ID
                    </th>
                    <th data-field="work_time" data-align="center" data-sortable="true">
                        Maintenance Schedule Date
                    </th>
                    <th data-field="sign_time" data-align="center" data-sortable="true">
                        Submit Date
                    </th>
                    <th data-field="task_supervisor_remark" data-align="center" data-sortable="true">
                        Follow Up
                    </th>
                    <th data-field="license_image" data-align="center" data-sortable="true">
                        USE Permit
                    </th>
                    <th data-field="license_expiry_time" data-align="center" data-sortable="true">
                        Expiry Date
                    </th>
                    <th data-field="incident_image" data-align="center" data-sortable="true">
                        Incident Report
                    </th>
                    <th data-field="incident_report_time" data-align="center" data-sortable="true">
                        Report Date
                    </th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($data['systemInfo']))
                    @foreach($data['systemInfo'] as $value)
                        @if(!empty($value['system_log']))
                            <tr>
                                <td><a style="color:blue;" href="{{ route('admin.hkaastaff.task', ['system_log_id' => $value['system_log']['system_log_id']]) }}">{{ $value['system_log']['system_id'] }}</a></td>
                                <td><a style="color:blue;" href="{{ route('admin.hkaastaff.task', ['system_log_id' => $value['system_log']['system_log_id']]) }}">{{ $value['system_log']['from_id'] }}</a></td>
                                <td>{{ date("Y-m-d", $value['system_log']['work_time']) }}</td>
                                <td>@if(!empty($value['system_log']['sign_time'])) {{ date("Y-m-d", $value['system_log']['sign_time']) }} @endif</td>
                                <td>
                                    @if(!empty($value['system_log']['task']))
                                        @foreach($value['system_log']['task'] as $v)
                                            @if(!empty($v['supervisor_remarks']))
                                                ✓
                                                @break
                                            @endif
                                        @endforeach
                                    @endif
                                </td>
                                <td>@if(!empty($value['system_log']['license_image'])) ✓ @endif</td>
                                <td>@if(!empty($value['system_log']['license_expiry_time'])) <a @if($value['system_log']['license_expiry_time'] < strtotime('+1 month')) style="color:red;"  @endif> {{ date("Y-m-d", $value['system_log']['license_expiry_time']) }} </a> @endif</td>
                                <td>@if(!empty($value['system_log']['incident_image'])) ✓ @endif</td>
                                <td>@if(!empty($value['system_log']['incident_report_time'])) {{ date("Y-m-d", $value['system_log']['incident_report_time']) }} @endif</td>
                            </tr>
                        @else
                            <tr>
                                <td>{{ $value['system_id'] }}</td>
                                <td>{{ $value['from_id'] }}</td>
                                <td>{{ date("Y-m-d", $value['work_time']) }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endif
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('menu')
    @parent
    <li class="active-link">
        <a href="javascript:history.go(-1);">
            <i class="fa fa-chevron-left"></i>
            <span class="menu-title">
                <strong style="color: white;">Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link" style="background-color: #658dba">
        <a href="javascript://">
            <i class="fa fa-line-chart"></i>
            <span class="menu-title">
                <strong>Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.hkaastaff.individual') }}">
            <i class="fa fa-wrench"></i>
            <span class="menu-title">
                <strong style="color: white;">Individual ID</strong>
            </span>
        </a>
    </li>

    <li class="active-link">
        <a href="{{ route('admin.hkaastaff.menuSend', ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand, 'menu' => 1]) }}">
            <i class="fa fa-envelope-o"></i>
            <span class="menu-title">
                <strong style="color: white;">Email Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.hkaastaff.menuDownload", ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand, 'menu' => 1]) }}">
            <i class="fa fa-file-excel-o"></i>
            <span class="menu-title">
                <strong style="color: white;">Export Excel</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.hkaastaff.downloadCrontab") }}">
            <i class="fa fa-cloud-download"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Monthly Schedule</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
@endsection

<script type="text/javascript" src="{{ url('/admin/js/jquery-2.2.4.min.js') }}"></script>
<script>
    function onTimes(){
        setInterval(function(){
            var dates = new Date().format("yyyy-MM-dd hh:mm:ss");
            $('#date-time').val(dates);
        }, 1000)
    }

    Date.prototype.format = function(format) {
        /*
         * eg:format="YYYY-MM-dd hh:mm:ss";

         */
        var o = {
            "M+" :this.getMonth() + 1, // month
            "d+" :this.getDate(), // day
            "h+" :this.getHours(), // hour
            "m+" :this.getMinutes(), // minute
            "s+" :this.getSeconds(), // second
            "q+" :Math.floor((this.getMonth() + 3) / 3), // quarter
            "S" :this.getMilliseconds()
            // millisecond
        }
        if (/(y+)/.test(format)) {
            format = format.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for ( var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                    : ("00" + o[k]).substr(("" + o[k]).length));
            }
        }
        return format;
    }

    function orderView(){
        $('.sortable').click(function(){
            var start_time = '{{ date('Y-m-d', $startTime) }}';
            var end_time = '{{ date('Y-m-d', $endTime) }}';
            var type = '{{ $type }}';
            var brand = '{{ $brand }}';
            var sort_key = $(this).text();
            sort_key = $.trim(sort_key);

            var sort = 'desc';
            if ($(this).hasClass('desc')) {
                sort = 'asc';
            }

            if ($(this).hasClass('asc')) {
                sort = 'desc';
            }

            var self = $(this);
            if (sort_key == 'System ID' || sort_key == 'Form ID') {
                $.get('{{ route("admin.hkaastaff.statisticsSort") }}', { start_time:start_time, end_time:end_time, type:type, brand:brand, sort_key:sort_key, sort:sort, menu:1 }, function (data) {
                    var info = data.split("----------");
                    $('tbody').html(info[0]);

                    if (info[1] == 'desc') {
                        self.removeClass('asc');
                        self.addClass('desc');
                    }

                    if (info[1] == 'asc') {
                        self.removeClass('desc');
                        self.addClass('asc');
                    }
                })

                $('.sortable').each(function(){
                    if ($(this) != self) {
                        $(this).removeClass('asc');
                        $(this).removeClass('desc');
                    }
                })
                return false;
            }
        })
    }

    $(function(){
        onTimes();

        window.setTimeout(orderView, 1000);
    })
</script>

