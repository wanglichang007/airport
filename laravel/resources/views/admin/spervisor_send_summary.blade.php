@extends('admin/header')
@section('container')
    @parent
    <div class="panel" style="margin-top:50px;">
        <div class="panel-heading">
            <h3 class="panel-title">
                Send Email Info</h3>
        </div>
        <!--Input Size-->
        <!--===================================================-->
        <form class="form-horizontal" action="{{ route('admin.supervisor.sendSummary') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="start_time" value="{{ $startTime }}">
            <input type="hidden" name="end_time" value="{{ $endTime }}">
            <input type="hidden" name="type" value="{{ $type }}">
            <input type="hidden" name="brand" value="{{ $brand }}">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-1 control-label" for="demo-is-inputsmall">
                        Recipient</label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Recipient" name="recipient" class="form-control input-sm" id="demo-is-inputsmall">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label" for="demo-is-inputsmall">
                        addCC</label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="addCC" name="addCC" class="form-control input-sm" id="demo-is-inputsmall">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label" for="demo-is-inputsmall">
                        Subject</label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Subject" name="subject" class="form-control input-sm" id="demo-is-inputsmall">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-1 control-label" for="demo-is-inputsmall">
                        Content</label>
                    <div class="col-sm-10">
                        <textarea placeholder="content" name="content" rows="20" class="form-control"></textarea>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-9 col-sm-offset-6">
                        <button class="btn btn-mint" type="submit">
                            Send</button>
                    </div>
                </div>
            </div>
        </form>
        <!--===================================================-->
        <!--End Input Size-->
    </div>
@endsection

@section('menu')
    @parent
    <li class="active-link">
        <a href="javascript:history.go(-1);">
            <i class="fa fa-chevron-left"></i>
            <span class="menu-title">
                <strong style="color: white;">Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.supervisor.downloadCrontab") }}">
            <i class="fa fa-cloud-download"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Monthly Schedule</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
    {{--    <li class="active-link">--}}
    {{--        <a href="{{ route('admin.supervisor.individual') }}">--}}
    {{--            <i class="demo-psi-home"></i>--}}
    {{--            <span class="menu-title">--}}
    {{--                <strong>Individual</strong>--}}
    {{--            </span>--}}
    {{--        </a>--}}
    {{--    </li>--}}
    {{--    <li class="active-link">--}}
    {{--        <a href="{{ route('admin.supervisor.individual') }}">--}}
    {{--            <i class="demo-psi-home"></i>--}}
    {{--            <span class="menu-title">--}}
    {{--                <strong>Next</strong>--}}
    {{--            </span>--}}
    {{--        </a>--}}
    {{--    </li>--}}
@endsection
