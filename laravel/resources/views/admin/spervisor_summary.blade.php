@extends('admin/header')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>
<script src="https://github.com/devongovett/pdfkit/releases/download/v0.6.2/pdfkit.js"></script>
<script src="{{ url('/admin/js/jspdf.debug.js') }}"></script>
<script src="{{ url('/admin/js/rgbcolor.js') }}"></script>
<script src="{{ url('/admin/js/canvg.js') }}"></script>
<script src="{{ url('/admin/js/script.js') }}"></script>
<style>
    table {
        border-bottom: 1.1px solid #999ea2;
    }
    tr {
        border-bottom: 1.1px solid #999ea2
    }
    td {
        border-bottom: 1.1px solid #999ea2;
    }
    th {
        border-bottom: 1.1px solid #999ea2;
    }
</style>
@section('container')
    @parent
    <input type="hidden" name="infoChartsJson" value="{{ $infoChartsJson }}" id="infoChartsJson">
    <input type="hidden" name="ratioChartsJson" value="{{ $ratioChartsJson }}" id="ratioChartsJson">
    <div id="content-all">
        <div id="top-content" class="panel" style="margin-top:10px;padding-top:10px;width:96%;margin-left:2%;">
            <form action="{{ route('admin.supervisor.summary') }}" method="get" id="search-summary">
                <input type="hidden" name="type" value="{{ $type }}">
                <input type="hidden" name="brand" value="{{ $brand }}">

                <div class="form-group" style="text-align: center;margin-top: 10px;">
                    @if (!empty($startTime))
                        <span style="color:black">Start Date:</span> <input type="text" name="start_time" value="{{ date("Y-m-d", $startTime) }}" id="start_datepicker" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;background-color:#eee;color:blue" >
                    @else
                        <span style="color:black">Start Date:</span> <input type="text" name="start_time" value="" id="datepicker" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;background-color:#eee;color:blue">
                    @endif
                    &nbsp&nbsp
                    @if (!empty($endTime))
                            <span style="color:black">End Date:</span> <input type="text" name="end_time" value="{{ date("Y-m-d", $endTime) }}" id="end_datepicker" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;background-color:#eee;color:blue" >
                    @else
                            <span style="color:black">End Date:</span> <input type="text" name="end_time" value="" id="datepicker" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;background-color:#eee;color:blue">
                    @endif
                        <span style="color:black">Date Time:</span> <input type="text" disabled="disabled" name="DateTimes" class="form-control" id="date-time" style="height:50px;margin-right: 10px;width:20%;display:inline;color:blue">
                        <label class="input-group-btn" style="width:10%;display:inline">
                            <span id="search" class="btn btn-primary" style="height:50px;line-height:35px;">
                                Search
                            </span>
                        </label>
                </div>
            </form>
        </div>

        <div id="title-content" class="panel" style="margin-top:10px;padding-top:10px;width:96%;margin-left:2%;">
            <div class="form-group" style="margin-top: 10px;margin-left:20px;">
                <div style="height:50px;margin-right: 100px;width:20%;display:inline;color:black;font-weight:bold;font-size: 20px;">Summary: &nbsp&nbsp @if(!empty($typeInfo)) {{ $typeInfo['name'] }} @endif</div>
            </div>
        </div>

        <div id="chartImage" class="panel" style="margin-top:10px;padding-top:10px;width:96%;margin-left:2%;">
            <div class="panel-body">
                <div style="width:45%;float:left;color:black;font-weight:bold;font-size: 20px;">Quantity Distribution</div>
                <div style="width:45%;float:left;margin-left:10%;color:black;font-weight:bold;font-size: 20px;">Percentage Distribution</div>
            </div>
            <div class="panel-body">
                <div id="charts" style="width:45%;float:left;"></div>
                <div id="charts-one" style="width:45%;float:left;margin-left:10%;"></div>
            </div>
{{--            <div class="panel-body">--}}
{{--                <div style="width:45%;float:left;"><img id="img" src="{{ url('/admin/img/logo.png') }}"></div>--}}
{{--            </div>--}}
        </div>

        <div id="bottom-content" class="panel" style="margin-top:20px;width:96%;margin-left:2%;padding-top:20px;">
            <div class="form-group" style="margin-left:10px;">
                <div style="margin-right: 100px;width:20%;display:inline;color:black;font-weight:bold;font-size: 20px;">Supervisor Progress Summary</div>
            </div>
            <div class="panel-body">
                <table id="demo-dt-addrow" class="table table-striped" cellspacing="0" width="100%;">
                    <thead>
                        <tr style="background-color: white;">
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;">Quantity</th>
                            <th style="text-align: center">%</th>
                            <th style="text-align: center">Use Permit</th>
                            <th style="text-align: center">Incident Report</th>
                        </tr>
                    </thead>
                    <tbody id="from-id-content">
                        @if (!empty($info))
                            <tr style="background-color: white;">
                                <td style="text-align: left;"><a style="color:blue;font-size: 18px;" href="{{ route('admin.supervisor.totalNumber', ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand]) }}">1 Total number of Maintenance Checklist</a></td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $info['totalNumber'] }}</td>
                                <td style="color:black;font-size: 18px;text-align: center">100</td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $info['totalUserPermitNumber'] }}</td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $info['totalIncidentReportNumber'] }}</td>
                            </tr>

                            <tr style="background-color: white;">
                                <td style="text-align: left;"><a style="color:blue;font-size: 18px;" href="{{ route('admin.supervisor.normalConditions', ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand]) }}">2 Normal Conditions</a></td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $info['normalConditionsNumber'] }}</td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $ratio['normalConditionsRatio'] }}</td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $info['normalConditionsUserPermitNumber'] }}</td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $info['normalConditionsIncidentReportNumber'] }}</td>
                            </tr>

                            <tr style="background-color: white;">
                                <td style="text-align: left;"><a style="color:blue;font-size: 18px;" href="{{ route('admin.supervisor.followUpActions', ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand]) }}">3 Checklist with Follow Up Actions</a></td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $info['followUpActionsNumber'] }}</td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $ratio['followUpActionsRatio'] }}</td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $info['followUpActionsUserPermitNumber'] }}</td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $info['followUpActionsIncidentReportNumber'] }}</td>
                            </tr>

                            <tr style="background-color: white;">
                                <td style="text-align: left;"><a style="color:blue;font-size: 18px;" href="{{ route('admin.supervisor.pendingSubmission', ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand]) }}">4 Checklist Pending Submission</a></td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $info['pendingSubmissionNumber'] }}</td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $ratio['pendingSubmissionRatio'] }}</td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $info['pendingSubmissionUserPermitNumber'] }}</td>
                                <td style="color:black;font-size: 18px;text-align: center">{{ $info['pendingSubmissionIncidentReportNumber'] }}</td>
                            </tr>
                        @else
                            <tr style="background-color: white;">
                                <td style="color:black;font-size: 18px;">1</td>
                                <td style="color:black;font-size: 18px;">Total number of Maintenance Checklist</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                            </tr>

                            <tr style="background-color: white;">
                                <td style="color:black;font-size: 18px;">2</td>
                                <td style="color:black;font-size: 18px;">Normal Conditions</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                            </tr>

                            <tr style="background-color: white;">
                                <td style="color:black;font-size: 18px;">3</td>
                                <td style="color:black;font-size: 18px;">Checklist with Follow Up Actions</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                            </tr>

                            <tr style="background-color: white;">
                                <td style="color:black;font-size: 18px;">4</td>
                                <td style="color:black;font-size: 18px;">Checklist Pending Submission</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                                <td style="color:black;font-size: 18px;text-align: center">0</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('menu')
    @parent
    <li class="active-link">
        <a href="{{ route('admin.supervisor') }}">
            <i class="fa fa-chevron-left"></i>
            <span class="menu-title">
                <strong style="color: white;">Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.supervisor.pendingReview') }}">
            <i class="fa fa-eye"></i>
            <span class="menu-title">
                <strong style="color: white;">Waiting Review @if(!empty($reviewNumber)) ({{ $reviewNumber }}) @endif</strong>
            </span>
        </a>
    </li>
    <li class="active-link" style="background-color: #658dba">
        <a href="javascript://">
            <i class="fa fa-line-chart"></i>
            <span class="menu-title">
                <strong>Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.supervisor.individual') }}">
            <i class="fa fa-wrench"></i>
            <span class="menu-title">
                <strong style="color: white;">Individual ID</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.supervisor.sendSummary', ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand]) }}">
{{--    <a href="javascript://" id="sendSummary">--}}
            <i class="fa fa-envelope-o"></i>
            <span class="menu-title">
                <strong style="color: white;">Email Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.supervisor.downloadSummary", ['start_time' => date('Y-m-d', $startTime), 'end_time' => date('Y-m-d', $endTime), 'type' => $type, 'brand' => $brand]) }}">
            <i class="fa fa-file-excel-o"></i>
            <span class="menu-title">
                <strong style="color: white;">Export Excel</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.supervisor.downloadCrontab") }}">
            <i class="fa fa-cloud-download"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Monthly Schedule</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
@endsection
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://cdn.highcharts.com.cn/highcharts/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>
<script>
    function onTimes(){
        setInterval(function(){
            var dates = new Date().format("yyyy-MM-dd hh:mm:ss");
            $('#date-time').val(dates);
        }, 1000)
    }

    Date.prototype.format = function(format) {
        /*
         * eg:format="YYYY-MM-dd hh:mm:ss";

         */
        var o = {
            "M+" :this.getMonth() + 1, // month
            "d+" :this.getDate(), // day
            "h+" :this.getHours(), // hour
            "m+" :this.getMinutes(), // minute
            "s+" :this.getSeconds(), // second
            "q+" :Math.floor((this.getMonth() + 3) / 3), // quarter
            "S" :this.getMilliseconds()
            // millisecond
        }
        if (/(y+)/.test(format)) {
            format = format.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for ( var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                    : ("00" + o[k]).substr(("" + o[k]).length));
            }
        }
        return format;
    }

    $(function(){
        onTimes();

        var infoChartsJson = $('#infoChartsJson').val();
        var infoChartsJsonObj = JSON.parse(infoChartsJson);
        var ratioChartsJson = $('#ratioChartsJson').val();
        var ratioChartsJsonObj = JSON.parse(ratioChartsJson);

        Highcharts.setOptions({
            colors: ['#01BAF2', '#71BF45', '#FAA74B']
        });
        Highcharts.chart('charts', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Total <br>'+"<?php echo !empty($info['totalNumber']) ? $info['totalNumber'] : 0 ?>",
                y:210
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            legend:{
                enabled:false
            },
            // tooltip: {
            //     pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            // },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        formatter:function(){
                            return this.key+ ': ' + this.y;
                        }
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Composition',
                colorByPoint: true,
                innerSize: '70%',
                data: infoChartsJsonObj
            }]
        });



        Highcharts.setOptions({
            colors: ['#01BAF2', '#71BF45', '#FAA74B']
        });
        Highcharts.chart('charts-one', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Total <br>'+"<?php echo !empty($info['totalNumber']) ? "100%" : "0%" ?>",
                y:210
            },
            credits: {
                enabled: false
            },
            navigation: {
                buttonOptions: {
                    enabled: false
                }
            },
            legend:{
                enabled:false
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        formatter:function(){
                            return this.key+ ': ' + this.y + '%';
                        }
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Composition',
                colorByPoint: true,
                innerSize: '70%',
                data: ratioChartsJsonObj
            }]
        });



        jQuery.browser = {};
        jQuery.browser.msie = false;
        jQuery.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
        $( "#start_datepicker" ).datepicker();
        $( "#end_datepicker" ).datepicker();

        $('#search').click(function(){
            $('#search-summary').submit();
            return false;
        });

        $('#sendSummary').click(function () {
            window.setTimeout(chartWithContentDownload, 2000);
        })
    })

    function chartWithContentDownload(){
        var height = $(".oldDiv").height()
        //克隆节点，默认为false，不复制方法属性，为true是全部复制。
        var body_width = $("body").width();
        $("body").width(body_width * 80 / 100);
        svgImage();
        var cloneDom = $("body").clone(true);
        //设置克隆节点的css属性，因为之前的层级为0，我们只需要比被克隆的节点层级低即可。
        cloneDom.css({
            "background-color": "white",
            "position": "absolute",
            "top": "0px",
            "z-index": "-1",
            "height": height
        });
        //将克隆节点动态追加到body后面。

        $("body").append(cloneDom);

        html2canvas(cloneDom[0]).then(function(canvas){
            function getBlobBydataURI(dataURI,type){
                var binary = atob(dataURI.split(',')[1]);
                var array = [];
                for(var i = 0; i < binary.length; i++) {
                    array.push(binary.charCodeAt(i));
                }
                return new Blob([new Uint8Array(array)], {type:type });
            }
            var imageData = canvas.toDataURL("image/jpeg");

            var formData = new FormData();

            var $Blob= getBlobBydataURI(imageData,'image/jpeg');

            formData.append('file',$Blob);
            var timestamp = new Date().getTime();
            formData.append('key',timestamp + '.png');//得到以当前时间命名的图片文件
            for (var pair  of formData.entries()) {

                if(typeof value == 'object'){
                    if(value.hasOwnProperty("name")){
                        pair[1].name = "1.jpg"
                    }
                }
            }

            $.ajax({
                url:'{{ route("admin.supervisor.testImage") }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type:'post',
                data:formData,
                async: true,
                crossDomain: true,
                contentType: false,
                processData: false,
                success: function(data){
                    console.log(data)
                }
            });
        })

        $("body").width(body_width);
    }

    function svgImage(){
        var svg = document.querySelector('svg');
        var canvas = document.createElement('canvas');
        var canvasIE = document.createElement('canvas');
        var context = canvas.getContext('2d');
        var DOMURL = window.URL || window.webkitURL || window;
        var data = (new XMLSerializer()).serializeToString(svg);
        canvg(canvas, data);
        var svgBlob = new Blob([data], {
            type: 'image/svg+xml;charset=utf-8'
        });

        var url = DOMURL.createObjectURL(svgBlob);

        // img.onload = function() {
        //     alert(1);
        //     context.canvas.width = $('#charts').find('svg').width();
        //     context.drawImage(img, 0, 0);
        //     // freeing up the memory as image is drawn to canvas
        //     DOMURL.revokeObjectURL(url);
        //
        //     var dataUrl;
        //     if (isIEBrowser()) { // Check of IE browser
        //         var svg = $('#testchart').highcharts().container.innerHTML;
        //         canvg(canvasIE, svg);
        //         dataUrl = canvasIE.toDataURL('image/JPEG');
        //     }
        //     else{
        //         dataUrl = canvas.toDataURL('image/JPEG');
        //     }
        //
        //     console.log(dataUrl);
        // };
        console.log(url);

        // var imgSrc
        // canvas.toBlob(function (url) {
        //     imgSrc = window.URL.createObjectURL(url)
        //     document.getElementById('img').src = imgSrc
        // })
    }

    function isIEBrowser(){
        var ieBrowser;
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // Internet Explorer
        {
            ieBrowser = true;
        }
        else  //Other browser
        {
            console.log('Other Browser');
            ieBrowser = false;
        }

        return ieBrowser;
    };
</script>
