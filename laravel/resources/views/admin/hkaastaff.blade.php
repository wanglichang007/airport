@extends('admin/header')
@section('container')
    @parent
    <form action="{{ route('admin.hkaastaff.summary') }}" method="get">

        <div class="panel-body" style="margin-top:20px;background-color: #F8F5F2;width:100%;">
            <div class="mar-ver pad-btm" style="text-align: center;">
                {{--                    <h5 class="h5 mar-no">電扶以及行人走道例行保養工作紀錄-紀錄輸入程序(Trial Version)</h5>--}}
                <span style="font-size:2rem;font-weight:bold;color:black">HKAA Management Progress Summary</span>
                <br>
                {{--                    <h3 class="h3 mar-no">PLANNEDMAINTEANCE-TASKCHECKLIST(ESCALATOR&WALKWAY)</h3>--}}
                <span style="font-size:2rem;font-weight:bold;color:black">System Selection</span>
            </div>
            @foreach($equipmentInfo as $key => $value)
                <div style="width:{{ round(100/count($equipmentInfo), 1) }}%;height:400px;float:left;text-align:center;">
                    <div style="height: 350px;text-align: center;line-height: 350px">
                        <img style="width:242px;height:240px;border-radius: 25px;border: 1px solid black;padding: 30px;" src="{{ $value['img'] }}">
                    </div>
                    <br>
                    <span style="font-size: 16px;font-weight: bold;color:black;text-align: center">
                        {{ $value['name'] }}
                        &nbsp
                        <input type="radio" name="type" @if($key == 0) checked @endif value="{{ $value['equipment_id'] }}">
                    </span>
                </div>
            @endforeach
        </div>


        <div class="panel-body" style="margin-top:20px;background-color: #F8F5F2;width:100%;">
        @foreach($brandInfo as $value)
            <div style="width:{{ round(100/count($brandInfo), 1) }}%;float:left;text-align: center">
                <span style="font-size: 16px;font-weight: bold;color:black">
                    {{ $value['name'] }}
                     &nbsp
                    <input type="radio" name="brand" value="{{ $value['brand_id'] }}">
                </span>
            </div>
        @endforeach
        </div>

        <div class="panel-body" style="margin-top:50px;background-color: #F8F5F2;width:100%;">
            <div class="row">
                <button class="btn btn-mint" type="submit" style="margin-right:10%;float: right">
                    View Summary</button>
            </div>
        </div>
    </form>
@endsection

@section('menu')
    @parent
    <li class="active-link" style="background-color: #658dba">
        <a href="javascript://">
            <i class="fa fa-line-chart"></i>
            <span class="menu-title">
                <strong>Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.hkaastaff.individual') }}">
            <i class="fa fa-wrench"></i>
            <span class="menu-title">
                <strong style="color: white;">Individual ID</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.hkaastaff.downloadCrontab") }}">
            <i class="fa fa-cloud-download"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Monthly Schedule</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
@endsection
