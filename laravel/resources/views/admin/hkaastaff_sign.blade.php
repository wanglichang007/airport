@extends('admin/header')
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('container')
    @parent
    <form action="{{ route('admin.hkaastaff.sign') }}" method="post" enctype="multipart/form-data" class="col-xs-12 col-sm-10 col-sm-offset-1 pad-hor" id="save-sign">
        {{ csrf_field() }}
        <input type="hidden" name="system_log_id" value="{{ $systemLogInfo['system_log_id'] }}">
        <input type="hidden" name="sign" id="sign">
        <div>
            <div style="float:left;">
                <div style="font-size:19px;color:black">Inspector User Sign:</div>
            </div>
            <div class="panel" style="margin-top:50px;width:80%;margin-left:20%;">
                <div class="panel-body">
                    <img src="{{ $systemLogInfo['view_sign'] }}">
                </div>
            </div>
        </div>

        <div>
            <div style="float:left;">
                <div style="font-size:19px;color:black">Supervisor User Sign:</div>
            </div>
            <div class="panel" style="margin-top:50px;width:80%;margin-left:20%;">
                <div class="panel-body">
                    <img src="{{ $systemLogInfo['view_supervisor_sign'] }}">
                </div>
            </div>
        </div>

        <div>
            <div style="float:left;">
                <div style="font-size:19px;color:black">HKAA User Sign:</div>
            </div>
            <div class="panel" style="margin-top:50px;width:80%;margin-left:20%;">
                <div class="panel-body" >
                    <div id="signature"></div>
                </div>
            </div>
        </div>
    </form>
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="{{ url('/admin/js/jSignature.js') }}"></script>
    <script src="{{ url('/admin/js/plugins/jSignature.CompressorBase30.js') }}"></script>
    <script src="{{ url('/admin/js/plugins/jSignature.CompressorSVG.js') }}"></script>
    <script src="{{ url('/admin/js/plugins/jSignature.UndoButton.js') }}"></script>
    <script src="{{ url('/admin/js/plugins/signhere/jSignature.SignHere.js') }}"></script>
    <script type="text/javascript">
        (function($) {
            var topics = {};
            $.publish = function(topic, args) {
                if (topics[topic]) {
                    var currentTopic = topics[topic],
                        args = args || {};

                    for (var i = 0, j = currentTopic.length; i < j; i++) {
                        currentTopic[i].call($, args);
                    }
                }
            };
            $.subscribe = function(topic, callback) {
                if (!topics[topic]) {
                    topics[topic] = [];
                }
                topics[topic].push(callback);
                return {
                    "topic": topic,
                    "callback": callback
                };
            };
            $.unsubscribe = function(handle) {
                var topic = handle.topic;
                if (topics[topic]) {
                    var currentTopic = topics[topic];

                    for (var i = 0, j = currentTopic.length; i < j; i++) {
                        if (currentTopic[i] === handle.callback) {
                            currentTopic.splice(i, 1);
                        }
                    }
                }
            };
        })(jQuery);

        $(document).ready(function() {
            var $sigdiv = $("#signature").jSignature({'UndoButton':true});

            $('#save').click(function(){
                var data = $sigdiv.jSignature('getData', "image")
                //$.publish(pubsubprefix + 'formatchanged')
                if (typeof data === 'string'){
                    $("#sign").val(data)
                    console.log(data)

                    $("#save-sign").submit();
                    return false;
                } else if($.isArray(data) && data.length === 2){
                    console.log(data.join(','))
                    $("#sign").val(data.join(','))
                    $("#save-sign").submit();
                    //$('textarea', $tools).val(data.join(','))
                    // $.publish(pubsubprefix + data[0], data);
                } else {
                    alert("Save Fail!");
                    // try {
                    //     $('textarea', $tools).val(JSON.stringify(data))
                    // } catch (ex) {
                    //     $('textarea', $tools).val('Not sure how to stringify this, likely binary, format.')
                    // }
                }
            })
        })

    </script>
@endsection
@section('menu')
    @parent
    <li class="active-link">
        <a href="javascript:history.go(-1);">
            <i class="fa fa-chevron-left"></i>
            <span class="menu-title">
                <strong style="color: white;">Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="javascript://" id="save">
            <i class="demo-psi-home"></i>
            <span class="menu-title">
                <strong style="color: red;">Endorse & Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.hkaastaff.downloadCrontab") }}">
            <i class="fa fa-cloud-download"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Monthly Schedule</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
@endsection


