@extends('admin/header')
@section('container')
    @parent
    <div class="panel-body" style="background-color: #F8F5F2">
        <div class="mar-ver pad-btm" style="text-align: center;">
            {{--                    <h5 class="h5 mar-no">電扶以及行人走道例行保養工作紀錄-紀錄輸入程序(Trial Version)</h5>--}}
            <span style="font-size:2rem;font-weight:bold;color:black">Supervisor Management Progress Summary</span>
            <br>
            {{--                    <h3 class="h3 mar-no">PLANNEDMAINTEANCE-TASKCHECKLIST(ESCALATOR&WALKWAY)</h3>--}}
            <span style="font-size:2rem;font-weight:bold;color:black">System Selection</span>
        </div>
        @foreach($equipmentInfo as $value)
            <div style="width:{{ round(100/count($equipmentInfo), 1) -1 }}%;float:left">
                <div style="height: 350px;text-align: center;line-height: 350px">
                    <a href="{{ route('admin.supervisor.summary', ['type' => $value['equipment_id']]) }}"><img style="width:242px;height:240px;border-radius: 25px;border: 1px solid black;padding: 30px;" src="{{ $value['img'] }}"></a>
                </div>
                <br>
                <label class="input-group-btn" style="margin-bottom:-1px;text-align: center;">
                    <a href="{{ route('admin.supervisor.summary', ['type' => $value['equipment_id']]) }}" style="font-size: 16px;font-weight: bold;color:black">
                        {{ $value['name'] }}
                    </a>
                </label>
            </div>
        @endforeach
    </div>
@endsection

@section('menu')
    @parent
    <li class="active-link">
        <a href="{{ route('admin.supervisor.pendingReview') }}">
            <i class="fa fa-eye"></i>
            <span class="menu-title">
                <strong style="color: white;">Waiting Review @if(!empty($reviewNumber)) ({{ $reviewNumber }}) @endif</strong>
            </span>
        </a>
    </li>
    <li class="active-link" style="background-color: #658dba">
        <a href="javascript://">
            <i class="fa fa-line-chart"></i>
            <span class="menu-title">
                <strong>Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.supervisor.individual') }}">
            <i class="fa fa-wrench"></i>
            <span class="menu-title">
                <strong style="color: white;">Individual ID</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.supervisor.downloadCrontab") }}">
            <i class="fa fa-cloud-download"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Monthly Schedule</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
{{--    <li class="active-link">--}}
{{--        <a href="{{ route('admin.supervisor.individual') }}">--}}
{{--            <i class="demo-psi-home"></i>--}}
{{--            <span class="menu-title">--}}
{{--                <strong>Next</strong>--}}
{{--            </span>--}}
{{--        </a>--}}
{{--    </li>--}}
@endsection
