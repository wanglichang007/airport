@extends('admin/header')
@section('container')
    @parent
    <!--Page Title-->

    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="morris-chart-network" class="morris-full-content" style="display:none"></div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content" style="margin-top: 20px;">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span>Waiting for Review List</span>
                    <span style="float: right;color:#808080">Total Progress: {{ count($data->items()) }}</span>
                </h3>
            </div>
            <div class="panel-body">
                <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th style="text-align: center">System ID</th>
                        <th style="text-align: center">From ID</th>
                        <th style="text-align: center">Submit Day</th>
                        <th style="text-align: center">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if (!empty($data))
                        @foreach($data->items() as $value)
                            <tr>
                                <td style="text-align: center">{{ $value['system_id'] }}</td>
                                <td style="text-align: center">{{ $value['from_id'] }}</td>
                                <td style="text-align: center">{{ date("Y-m-d H:i:s", $value['sign_time']) }}</td>
                                <td>
                                    <label class="input-group-btn" style="margin-bottom:-1px;text-align: center;">
                                        <a href="{{ route('admin.supervisor.task', ['system_log_id' => $value['system_log_id']]) }}">
                                            <span id="review" class="btn btn-primary" style="height:25px;line-height:13px;">
                                                Review
                                            </span>
                                        </a>
                                    </label>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                @if (!empty($data))
                    {{ $data->links('manage.pages', ['data' => $data, $where]) }}
                @endif
            </div>
        </div>
    </div>
@endsection
@section('menu')
    @parent
    <li class="active-link">
        <a href="javascript:history.go(-1);">
            <i class="fa fa-chevron-left"></i>
            <span class="menu-title">
                <strong style="color: white;">Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link" style="background-color: #658dba">
        <a href="javascript://">
            <i class="fa fa-eye"></i>
            <span class="menu-title">
                <strong>Waiting Review @if(!empty($reviewNumber)) ({{ $reviewNumber }}) @endif</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.supervisor') }}">
            <i class="fa fa-line-chart"></i>
            <span class="menu-title">
                <strong style="color: white;">Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.supervisor.individual') }}">
            <i class="fa fa-wrench"></i>
            <span class="menu-title">
                <strong style="color: white;">Individual ID</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.supervisor.downloadCrontab") }}">
            <i class="fa fa-cloud-download"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Monthly Schedule</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
@endsection
