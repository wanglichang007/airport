<!DOCTYPE html>
<html lang="en" ng-app="MyApp">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Admin</title>


    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>


    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ url('/admin/css/bootstrap.min.css') }}" rel="stylesheet">


    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{ url('/admin/css/nifty.min.css') }}" rel="stylesheet">


    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{ url('/admin/css/demo/nifty-demo-icons.min.css') }}" rel="stylesheet">


    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ url('/admin/css/demo/nifty-demo.min.css') }}" rel="stylesheet">



    <!--Morris.js [ OPTIONAL ]-->
{{--    <link href="{{ url('/admin/plugins/morris-js/morris.min.css') }}" rel="stylesheet">--}}


    <!--Magic Checkbox [ OPTIONAL ]-->
    <link href="{{ url('/admin/plugins/magic-check/css/magic-check.min.css') }}" rel="stylesheet">


    <!--Premium Solid Icons [ OPTIONAL ]-->
    <link href="{{ url('/admin/premium/icon-sets/icons/solid-icons/premium-solid-icons.min.css') }}" rel="stylesheet">


    <!--Bootstrap Table [ OPTIONAL ]-->
    <link href="{{ url('/admin/plugins/bootstrap-table/bootstrap-table.min.css') }}" rel="stylesheet">


    <!--X-editable [ OPTIONAL ]-->
    <link href="{{ url('/admin/plugins/x-editable/css/bootstrap-editable.css') }}" rel="stylesheet">


    <!--DataTables [ OPTIONAL ]-->
    <link href="{{ url('/admin/plugins/datatables/media/css/dataTables.bootstrap.css') }}" rel="stylesheet">
    <link href="{{ url('/admin/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}" rel="stylesheet">






    <!--JAVASCRIPT-->
    <!--=================================================-->

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{ url('/admin/plugins/pace/pace.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ url('/admin/css/jquery-ui.css') }}" />
    <script src="{{ url('/admin/plugins/pace/pace.min.js') }}"></script>


    <!--jQuery [ REQUIRED ]-->
    {{--<script src="/admin/js/jquery-2.2.4.min.js"></script>--}}
    <script type="text/javascript" src="{{ url('/admin/js/jquery-2.2.4.min.js') }}"></script>


    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="{{ url('/admin/js/bootstrap.min.js') }}"></script>


    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="{{ url('/admin/js/nifty.min.js') }}"></script>



    <script type="text/javascript" src="{{ url('/admin/js/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{ url('/admin/js/jquery-ui-slide.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('/admin/js/jquery-ui-timepicker-addon.js') }}"></script>


    <!--=================================================-->

    <!--Demo script [ DEMONSTRATION ]-->
    <script src="{{ url('/admin/js/demo/nifty-demo.min.js') }}"></script>


    <!--Morris.js [ OPTIONAL ]-->
{{--    <script src="{{ url('/admin/plugins/morris-js/morris.min.js') }}"></script>--}}
{{--    <script src="{{ url('/admin/plugins/morris-js/raphael-js/raphael.min.js') }}"></script>--}}


    <!--Sparkline [ OPTIONAL ]-->
    <script src="{{ url('/admin/plugins/sparkline/jquery.sparkline.min.js') }}"></script>


    <!--Specify page [ SAMPLE ]-->
{{--    <script src="{{ url('/admin/js/demo/dashboard.js') }}"></script>--}}
    <script src="{{ url('/admin/plugins/x-editable/js/bootstrap-editable.min.js') }}"></script>

    <!--Bootstrap Table [ OPTIONAL ]-->
    <script src="{{ url('/admin/plugins/bootstrap-table/bootstrap-table.min.js') }}"></script>


    <!--Bootstrap Table Extension [ OPTIONAL ]-->
    <script src="{{ url('/admin/plugins/bootstrap-table/extensions/editable/bootstrap-table-editable.js') }}"></script>


    <!--Font Awesome [ OPTIONAL ]-->
    <link href="{{ url('/admin/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">





    <!--=================================================

    REQUIRED
    You must include this in your project.


    RECOMMENDED
    This category must be included but you may modify which plugins or components which should be included in your project.


    OPTIONAL
    Optional plugins. You may choose whether to include it in your project or not.


    DEMONSTRATION
    This is to be removed, used for demonstration purposes only. This category must not be included in your project.


    SAMPLE
    Some script samples which explain how to initialize plugins or components. This category should not be included in your project.


    Detailed information and more samples can be found in the document.

    =================================================-->

</head>
<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->
<body style="background-color:#F8F5F2;font-size: 15px">
<div id="container" class="effect aside-float aside-bright mainnav-lg">

    <!--NAVBAR-->
    <!--===================================================-->
    <header id="navbar">
        <div id="navbar-container" class="boxed" style="background-color: #1f2a34">

            <!--Brand logo & name-->
            <!--================================-->
            {{--<div class="navbar-header">--}}
                {{--<a href="index.html" class="navbar-brand">--}}
                    {{--<img src="img/logo.png" alt="Nifty Logo" class="brand-icon">--}}
                    {{--<div class="brand-title">--}}
                        {{--<span class="brand-text">Nifty</span>--}}
                        {{--</div>--}}
                    {{--</a>--}}
                {{--</div>--}}
            <!--================================-->
            <!--End brand logo & name-->


            <!--Navbar Dropdown-->
            <!--================================-->
            <div class="navbar-content clearfix" style="text-align: center;padding-bottom: 5px;padding-top: 5px;">
                <span style="font-size: 15px;font-weight:bold;color:white">電動扶梯以及自動人行道例行保養工作紀錄-紀錄輸入程序(Trial Version)</span>
                <br>
                <span style="font-size: 15px;font-weight:bold;color:white">PLANNED MAINTENANCE-TASK CHECKLIST(ESCALATOR & WALKWAY)</span>
            </div>
            <!--================================-->
            <!--End Navbar Dropdown-->

        </div>
    </header>
    <!--===================================================-->
    <!--END NAVBAR-->
    <div class="boxed">
        <!--CONTENT CONTAINER-->
        <!--===================================================-->
        <div id="content-container" style="background-color: #F8F5F2;height:100%;overflow:auto">@yield("container")</div>
        <!--ASIDE-->
        <!--===================================================-->
        {{--<aside id="aside-container">--}}
            {{--<div id="aside">--}}
                {{--<div class="nano">--}}
                    {{--<div class="nano-content">--}}

                        {{--<!--Nav tabs-->--}}
                        {{--<!--================================-->--}}
                        {{--<ul class="nav nav-tabs nav-justified">--}}
                            {{--<li class="active">--}}
                                {{--<a href="#demo-asd-tab-1" data-toggle="tab">--}}
                                    {{--<i class="demo-pli-speech-bubble-7"></i>--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#demo-asd-tab-2" data-toggle="tab">--}}
                                    {{--<i class="demo-pli-information icon-fw"></i> Report--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--<li>--}}
                                {{--<a href="#demo-asd-tab-3" data-toggle="tab">--}}
                                    {{--<i class="demo-pli-wrench icon-fw"></i> Settings--}}
                                    {{--</a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--<!--================================-->--}}
                        {{--<!--End nav tabs-->--}}



                        {{--<!-- Tabs Content -->--}}
                        {{--<!--================================-->--}}
                        {{--<div class="tab-content">--}}

                            {{--<!--First tab (Contact list)-->--}}
                            {{--<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->--}}
                            {{--<div class="tab-pane fade in active" id="demo-asd-tab-1">--}}
                                {{--<p class="pad-hor mar-top text-semibold text-main">--}}
                                    {{--<span class="pull-right badge badge-warning">3</span> Family--}}
                                    {{--</p>--}}

                                {{--<!--Family-->--}}
                                {{--<div class="list-group bg-trans">--}}
                                    {{--<a href="#" class="list-group-item">--}}
                                        {{--<div class="media-left pos-rel">--}}
                                            {{--<img class="img-circle img-xs" src="img/profile-photos/2.png" alt="Profile Picture">--}}
                                            {{--<i class="badge badge-success badge-stat badge-icon pull-left"></i>--}}
                                            {{--</div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<p class="mar-no">Stephen Tran</p>--}}
                                            {{--<small class="text-muted">Availabe</small>--}}
                                            {{--</div>--}}
                                        {{--</a>--}}
                                    {{--<a href="#" class="list-group-item">--}}
                                        {{--<div class="media-left pos-rel">--}}
                                            {{--<img class="img-circle img-xs" src="img/profile-photos/7.png" alt="Profile Picture">--}}
                                            {{--</div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<p class="mar-no">Brittany Meyer</p>--}}
                                            {{--<small class="text-muted">I think so</small>--}}
                                            {{--</div>--}}
                                        {{--</a>--}}
                                    {{--<a href="#" class="list-group-item">--}}
                                        {{--<div class="media-left pos-rel">--}}
                                            {{--<img class="img-circle img-xs" src="img/profile-photos/1.png" alt="Profile Picture">--}}
                                            {{--<i class="badge badge-info badge-stat badge-icon pull-left"></i>--}}
                                            {{--</div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<p class="mar-no">Jack George</p>--}}
                                            {{--<small class="text-muted">Last Seen 2 hours ago</small>--}}
                                            {{--</div>--}}
                                        {{--</a>--}}
                                    {{--<a href="#" class="list-group-item">--}}
                                        {{--<div class="media-left pos-rel">--}}
                                            {{--<img class="img-circle img-xs" src="img/profile-photos/4.png" alt="Profile Picture">--}}
                                            {{--</div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<p class="mar-no">Donald Brown</p>--}}
                                            {{--<small class="text-muted">Lorem ipsum dolor sit amet.</small>--}}
                                            {{--</div>--}}
                                        {{--</a>--}}
                                    {{--<a href="#" class="list-group-item">--}}
                                        {{--<div class="media-left pos-rel">--}}
                                            {{--<img class="img-circle img-xs" src="img/profile-photos/8.png" alt="Profile Picture">--}}
                                            {{--<i class="badge badge-warning badge-stat badge-icon pull-left"></i>--}}
                                            {{--</div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<p class="mar-no">Betty Murphy</p>--}}
                                            {{--<small class="text-muted">Idle</small>--}}
                                            {{--</div>--}}
                                        {{--</a>--}}
                                    {{--<a href="#" class="list-group-item">--}}
                                        {{--<div class="media-left pos-rel">--}}
                                            {{--<img class="img-circle img-xs" src="img/profile-photos/9.png" alt="Profile Picture">--}}
                                            {{--<i class="badge badge-danger badge-stat badge-icon pull-left"></i>--}}
                                            {{--</div>--}}
                                        {{--<div class="media-body">--}}
                                            {{--<p class="mar-no">Samantha Reid</p>--}}
                                            {{--<small class="text-muted">Offline</small>--}}
                                            {{--</div>--}}
                                        {{--</a>--}}
                                    {{--</div>--}}

                                {{--<hr>--}}
                                {{--<p class="pad-hor text-semibold text-main">--}}
                                    {{--<span class="pull-right badge badge-success">Offline</span> Friends--}}
                                    {{--</p>--}}

                                {{--<!--Works-->--}}
                                {{--<div class="list-group bg-trans">--}}
                                    {{--<a href="#" class="list-group-item">--}}
                                        {{--<span class="badge badge-purple badge-icon badge-fw pull-left"></span> Joey K. Greyson--}}
                                        {{--</a>--}}
                                    {{--<a href="#" class="list-group-item">--}}
                                        {{--<span class="badge badge-info badge-icon badge-fw pull-left"></span> Andrea Branden--}}
                                        {{--</a>--}}
                                    {{--<a href="#" class="list-group-item">--}}
                                        {{--<span class="badge badge-success badge-icon badge-fw pull-left"></span> Johny Juan--}}
                                        {{--</a>--}}
                                    {{--<a href="#" class="list-group-item">--}}
                                        {{--<span class="badge badge-danger badge-icon badge-fw pull-left"></span> Susan Sun--}}
                                        {{--</a>--}}
                                    {{--</div>--}}


                                {{--<hr>--}}
                                {{--<p class="pad-hor mar-top text-semibold text-main">News</p>--}}

                                {{--<div class="pad-hor">--}}
                                    {{--<p class="text-muted">Lorem ipsum dolor sit amet, consectetuer--}}
                                        {{--<a data-title="45%" class="add-tooltip text-semibold" href="#">adipiscing elit</a>, sed diam nonummy nibh. Lorem ipsum dolor sit amet.--}}
                                        {{--</p>--}}
                                    {{--<small class="text-muted"><em>Last Update : Des 12, 2014</em></small>--}}
                                    {{--</div>--}}


                                {{--</div>--}}
                            {{--<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->--}}
                            {{--<!--End first tab (Contact list)-->--}}


                            {{--<!--Second tab (Custom layout)-->--}}
                            {{--<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->--}}
                            {{--<div class="tab-pane fade" id="demo-asd-tab-2">--}}

                                {{--<!--Monthly billing-->--}}
                                {{--<div class="pad-all">--}}
                                    {{--<p class="text-semibold text-main">Billing &amp; reports</p>--}}
                                    {{--<p class="text-muted">Get <strong>$5.00</strong> off your next bill by making sure your full payment reaches us before August 5, 2016.</p>--}}
                                    {{--</div>--}}
                                {{--<hr class="new-section-xs">--}}
                                {{--<div class="pad-all">--}}
                                    {{--<span class="text-semibold text-main">Amount Due On</span>--}}
                                    {{--<p class="text-muted text-sm">August 17, 2016</p>--}}
                                    {{--<p class="text-2x text-thin text-main">$83.09</p>--}}
                                    {{--<button class="btn btn-block btn-success mar-top">Pay Now</button>--}}
                                    {{--</div>--}}


                                {{--<hr>--}}

                                {{--<p class="pad-hor text-semibold text-main">Additional Actions</p>--}}

                                {{--<!--Simple Menu-->--}}
                                {{--<div class="list-group bg-trans">--}}
                                    {{--<a href="#" class="list-group-item"><i class="demo-pli-information icon-lg icon-fw"></i> Service Information</a>--}}
                                    {{--<a href="#" class="list-group-item"><i class="demo-pli-mine icon-lg icon-fw"></i> Usage Profile</a>--}}
                                    {{--<a href="#" class="list-group-item"><span class="label label-info pull-right">New</span><i class="demo-pli-credit-card-2 icon-lg icon-fw"></i> Payment Options</a>--}}
                                    {{--<a href="#" class="list-group-item"><i class="demo-pli-support icon-lg icon-fw"></i> Message Center</a>--}}
                                    {{--</div>--}}


                                {{--<hr>--}}

                                {{--<div class="text-center">--}}
                                    {{--<div><i class="demo-pli-old-telephone icon-3x"></i></div>--}}
                                    {{--Questions?--}}
                                    {{--<p class="text-lg text-semibold text-main"> (415) 234-53454 </p>--}}
                                    {{--<small><em>We are here 24/7</em></small>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--<!--End second tab (Custom layout)-->--}}
                            {{--<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->--}}


                            {{--<!--Third tab (Settings)-->--}}
                            {{--<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->--}}
                            {{--<div class="tab-pane fade" id="demo-asd-tab-3">--}}
                                {{--<ul class="list-group bg-trans">--}}
                                    {{--<li class="pad-top list-header">--}}
                                        {{--<p class="text-semibold text-main mar-no">Account Settings</p>--}}
                                        {{--</li>--}}
                                    {{--<li class="list-group-item">--}}
                                        {{--<div class="pull-right">--}}
                                            {{--<input class="toggle-switch" id="demo-switch-1" type="checkbox" checked>--}}
                                            {{--<label for="demo-switch-1"></label>--}}
                                            {{--</div>--}}
                                        {{--<p class="mar-no">Show my personal status</p>--}}
                                        {{--<small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small>--}}
                                        {{--</li>--}}
                                    {{--<li class="list-group-item">--}}
                                        {{--<div class="pull-right">--}}
                                            {{--<input class="toggle-switch" id="demo-switch-2" type="checkbox" checked>--}}
                                            {{--<label for="demo-switch-2"></label>--}}
                                            {{--</div>--}}
                                        {{--<p class="mar-no">Show offline contact</p>--}}
                                        {{--<small class="text-muted">Aenean commodo ligula eget dolor. Aenean massa.</small>--}}
                                        {{--</li>--}}
                                    {{--<li class="list-group-item">--}}
                                        {{--<div class="pull-right">--}}
                                            {{--<input class="toggle-switch" id="demo-switch-3" type="checkbox">--}}
                                            {{--<label for="demo-switch-3"></label>--}}
                                            {{--</div>--}}
                                        {{--<p class="mar-no">Invisible mode </p>--}}
                                        {{--<small class="text-muted">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </small>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}


                                {{--<hr>--}}

                                {{--<ul class="list-group pad-btm bg-trans">--}}
                                    {{--<li class="list-header"><p class="text-semibold text-main mar-no">Public Settings</p></li>--}}
                                    {{--<li class="list-group-item">--}}
                                        {{--<div class="pull-right">--}}
                                            {{--<input class="toggle-switch" id="demo-switch-4" type="checkbox" checked>--}}
                                            {{--<label for="demo-switch-4"></label>--}}
                                            {{--</div>--}}
                                        {{--Online status--}}
                                        {{--</li>--}}
                                    {{--<li class="list-group-item">--}}
                                        {{--<div class="pull-right">--}}
                                            {{--<input class="toggle-switch" id="demo-switch-5" type="checkbox" checked>--}}
                                            {{--<label for="demo-switch-5"></label>--}}
                                            {{--</div>--}}
                                        {{--Show offline contact--}}
                                        {{--</li>--}}
                                    {{--<li class="list-group-item">--}}
                                        {{--<div class="pull-right">--}}
                                            {{--<input class="toggle-switch" id="demo-switch-6" type="checkbox" checked>--}}
                                            {{--<label for="demo-switch-6"></label>--}}
                                            {{--</div>--}}
                                        {{--Show my device icon--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}



                                {{--<hr>--}}

                                {{--<p class="pad-hor text-semibold text-main mar-no">Task Progress</p>--}}
                                {{--<div class="pad-all">--}}
                                    {{--<p>Upgrade Progress</p>--}}
                                    {{--<div class="progress progress-sm">--}}
                                        {{--<div class="progress-bar progress-bar-success" style="width: 15%;"><span class="sr-only">15%</span></div>--}}
                                        {{--</div>--}}
                                    {{--<small class="text-muted">15% Completed</small>--}}
                                    {{--</div>--}}
                                {{--<div class="pad-hor">--}}
                                    {{--<p>Database</p>--}}
                                    {{--<div class="progress progress-sm">--}}
                                        {{--<div class="progress-bar progress-bar-danger" style="width: 75%;"><span class="sr-only">75%</span></div>--}}
                                        {{--</div>--}}
                                    {{--<small class="text-muted">17/23 Database</small>--}}
                                    {{--</div>--}}

                                {{--</div>--}}
                            {{--<!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->--}}
                            {{--<!--Third tab (Settings)-->--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</aside>--}}
        <!--===================================================-->
        <!--END ASIDE-->
        <!--MAIN NAVIGATION-->
        <!--===================================================-->
        <nav id="mainnav-container">
            <div id="mainnav">

                <!--Menu-->
                <!--================================-->
                <div id="mainnav-menu-wrap">
                    <div class="nano">
                        <div class="nano-content" style="background-color:#1f2a34;">

                            <!--Profile Widget-->
                            <!--================================-->
{{--                            <div id="mainnav-profile" class="mainnav-profile">--}}
{{--                                <div class="profile-wrap" style="background-image: none">--}}
{{--                                    <span class="pull-right dropdown-toggle">--}}
{{--                                            <i class="dropdown-caret"></i>--}}
{{--                                        </span>--}}
{{--                                    <p class="mnp-name" style="color:red">Login ID:&nbsp&nbsp{{ session('airportUser.name') }}</p>--}}
{{--                                    <p class="mnp-name" style="color:red"><a href="{{ route('admin.personal.uppassword') }}" >Setting</a></p>--}}
{{--                                    <p class="mnp-name" style="color:red"><a href="{{ route('admin.logout') }}">Logout</a></p>--}}
{{--                                </div>--}}
{{--                                <div id="profile-nav" class="collapse list-group bg-trans">--}}
{{--                                    --}}{{--<a href="#" class="list-group-item">--}}
{{--                                    --}}{{--<i class="demo-pli-male icon-lg icon-fw"></i> View Profile--}}
{{--                                    --}}{{--</a>--}}
{{--                                    <a href="{{ route('admin.personal.uppassword') }}" class="list-group-item">--}}
{{--                                        <i class="demo-pli-gear icon-lg icon-fw"></i> Change Password--}}
{{--                                    </a>--}}
{{--                                    --}}{{--<a href="#" class="list-group-item">--}}
{{--                                    --}}{{--<i class="demo-pli-information icon-lg icon-fw"></i> Help--}}
{{--                                    --}}{{--</a>--}}
{{--                                    <a href="{{ route('admin.logout') }}" class="list-group-item">--}}
{{--                                        <i class="demo-pli-unlock icon-lg icon-fw"></i> Logout--}}
{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            </div>--}}


                            <!--Shortcut buttons-->
                            <!--================================-->

                            <!--================================-->
                            <!--End shortcut buttons-->


                            <ul id="mainnav-menu" class="list-group">
                                <li class="active-link">
                                    <a href="javascript://">
                                        <span class="menu-title">
                                            <strong ><span style="color: white">Login ID:</span>&nbsp&nbsp<span style="color: #579EED">{{ session('airportUser.name') }}</span></strong>
                                        </span>
                                    </a>
                                </li>
                                <li class="active-link">
                                    <a href="{{ route('admin.personal.uppassword') }}">
                                        <i class="fa fa-gear"></i>
                                        <span class="menu-title">
                                            <strong style="color: white;font-size: 16px;">Setting</strong>
                                        </span>
                                    </a>
                                </li>
{{--                                <li class="active-link">--}}
{{--                                    <a href="{{ route('admin.logout') }}">--}}
{{--                                        <i class="fa fa-power-off"></i>--}}
{{--                                        <span class="menu-title">--}}
{{--                                            <strong style="color: white;font-size: 16px;">Logout</strong>--}}
{{--                                        </span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
                                @yield("menu")
                            </ul>
                        </div>
                    </div>
                </div>
                <!--================================-->
                <!--End menu-->

            </div>
        </nav>
        <!--===================================================-->
        <!--END MAIN NAVIGATION-->

    </div>
    <!-- FOOTER -->
    <!--===================================================-->
    {{--<footer id="footer">--}}

        {{--<!-- Visible when footer positions are fixed -->--}}
        {{--<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->--}}
        {{--<div class="show-fixed pull-right">--}}
            {{--You have <a href="#" class="text-bold text-main"><span class="label label-danger">3</span> pending action.</a>--}}
            {{--</div>--}}



        {{--<!-- Visible when footer positions are static -->--}}
        {{--<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->--}}
        {{--<div class="hide-fixed pull-right pad-rgt">--}}
            {{--14GB of <strong>512GB</strong> Free.--}}
            {{--</div>--}}



        {{--<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->--}}
        {{--<!-- Remove the class "show-fixed" and "hide-fixed" to make the content always appears. -->--}}
        {{--<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->--}}

        {{--<p class="pad-lft">&#0169; 2016 Your Company</p>--}}



        {{--</footer>--}}
    <!--===================================================-->
    <!-- END FOOTER -->


    <!-- SCROLL PAGE BUTTON -->
    <!--===================================================-->
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
    <!--===================================================-->



</div>
<!--===================================================-->
<!-- END OF CONTAINER -->



<!-- SETTINGS - DEMO PURPOSE ONLY -->
<!--===================================================-->
{{--<div id="demo-set" class="demo-set">--}}
    {{--<div id="demo-set-body" class="demo-set-body collapse">--}}
        {{--<div id="demo-set-alert"></div>--}}
        {{--<div class="pad-hor bord-btm clearfix">--}}
            {{--<div class="pull-right pad-top">--}}
                {{--<button id="demo-btn-close-settings" class="btn btn-trans">--}}
                    {{--<i class="pci-cross pci-circle icon-lg"></i>--}}
                    {{--</button>--}}
                {{--</div>--}}
            {{--<div class="media">--}}
                {{--<div class="media-left">--}}
                    {{--<i class="demo-pli-gear icon-2x"></i>--}}
                    {{--</div>--}}
                {{--<div class="media-body">--}}
                    {{--<span class="text-semibold text-lg text-main">Costomize</span>--}}
                    {{--<p class="text-muted text-sm">Customize Nifty's layout, sidebars, and color schemes.</p>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--<div class="demo-set-content clearfix">--}}
            {{--<div class="col-xs-6 col-md-3">--}}
                {{--<div class="pad-all bg-gray-light">--}}
                    {{--<p class="text-semibold text-main text-lg">Layout</p>--}}
                    {{--<p class="text-semibold text-main">Boxed Layout</p>--}}
                    {{--<div class="pad-btm">--}}
                        {{--<div class="pull-right">--}}
                            {{--<input id="demo-box-lay" class="toggle-switch" type="checkbox">--}}
                            {{--<label for="demo-box-lay"></label>--}}
                            {{--</div>--}}
                        {{--Boxed Layout--}}
                        {{--</div>--}}
                    {{--<div class="pad-btm">--}}
                        {{--<div class="pull-right">--}}
                            {{--<button id="demo-box-img" class="btn btn-sm btn-trans" disabled><i class="pci-hor-dots"></i></button>--}}
                            {{--</div>--}}
                        {{--Background Images <span class="label label-primary">New</span>--}}
                        {{--</div>--}}

                    {{--<hr class="new-section-xs bord-no">--}}
                    {{--<p class="text-semibold text-main">Animations</p>--}}
                    {{--<div class="pad-btm">--}}
                        {{--<div class="pull-right">--}}
                            {{--<input id="demo-anim" class="toggle-switch" type="checkbox" checked>--}}
                            {{--<label for="demo-anim"></label>--}}
                            {{--</div>--}}
                        {{--Enable Animations--}}
                        {{--</div>--}}
                    {{--<div class="pad-btm">--}}
                        {{--<div class="select pull-right">--}}
                            {{--<select id="demo-ease">--}}
                                {{--<option value="effect" selected>ease (Default)</option>--}}
                                {{--<option value="easeInQuart">easeInQuart</option>--}}
                                {{--<option value="easeOutQuart">easeOutQuart</option>--}}
                                {{--<option value="easeInBack">easeInBack</option>--}}
                                {{--<option value="easeOutBack">easeOutBack</option>--}}
                                {{--<option value="easeInOutBack">easeInOutBack</option>--}}
                                {{--<option value="steps">Steps</option>--}}
                                {{--<option value="jumping">Jumping</option>--}}
                                {{--<option value="rubber">Rubber</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--Transitions--}}
                        {{--</div>--}}

                    {{--<hr class="new-section-xs bord-no">--}}

                    {{--<p class="text-semibold text-main text-lg">Header / Navbar</p>--}}
                    {{--<div>--}}
                        {{--<div class="pull-right">--}}
                            {{--<input id="demo-navbar-fixed" class="toggle-switch" type="checkbox">--}}
                            {{--<label for="demo-navbar-fixed"></label>--}}
                            {{--</div>--}}
                        {{--Fixed Position--}}
                        {{--</div>--}}

                    {{--<hr class="new-section-xs bord-no">--}}

                    {{--<p class="text-semibold text-main text-lg">Footer</p>--}}
                    {{--<div class="pad-btm">--}}
                        {{--<div class="pull-right">--}}
                            {{--<input id="demo-footer-fixed" class="toggle-switch" type="checkbox">--}}
                            {{--<label for="demo-footer-fixed"></label>--}}
                            {{--</div>--}}
                        {{--Fixed Position--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--<div class="col-lg-9 pos-rel">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-lg-4">--}}
                        {{--<div class="pad-all">--}}
                            {{--<p class="text-semibold text-main text-lg">Sidebars</p>--}}
                            {{--<p class="text-semibold text-main">Navigation</p>--}}
                            {{--<div class="mar-btm">--}}
                                {{--<div class="pull-right">--}}
                                    {{--<input id="demo-nav-fixed" class="toggle-switch" type="checkbox">--}}
                                    {{--<label for="demo-nav-fixed"></label>--}}
                                    {{--</div>--}}
                                {{--Fixed Position--}}
                                {{--</div>--}}
                            {{--<div class="mar-btm">--}}
                                {{--<div class="pull-right">--}}
                                    {{--<input id="demo-nav-profile" class="toggle-switch" type="checkbox" checked>--}}
                                    {{--<label for="demo-nav-profile"></label>--}}
                                    {{--</div>--}}
                                {{--Widget Profil <small class="label label-primary">New</small>--}}
                                {{--</div>--}}
                            {{--<div class="mar-btm">--}}
                                {{--<div class="pull-right">--}}
                                    {{--<input id="demo-nav-shortcut" class="toggle-switch" type="checkbox" checked>--}}
                                    {{--<label for="demo-nav-shortcut"></label>--}}
                                    {{--</div>--}}
                                {{--Shortcut Buttons--}}
                                {{--</div>--}}
                            {{--<div class="mar-btm">--}}
                                {{--<div class="pull-right">--}}
                                    {{--<input id="demo-nav-coll" class="toggle-switch" type="checkbox">--}}
                                    {{--<label for="demo-nav-coll"></label>--}}
                                    {{--</div>--}}
                                {{--Collapsed Mode--}}
                                {{--</div>--}}

                            {{--<div class="clearfix">--}}
                                {{--<div class="select pad-btm pull-right">--}}
                                    {{--<select id="demo-nav-offcanvas">--}}
                                        {{--<option value="none" selected disabled="disabled">-- Select Mode --</option>--}}
                                        {{--<option value="push">Push</option>--}}
                                        {{--<option value="slide">Slide in on top</option>--}}
                                        {{--<option value="reveal">Reveal</option>--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--Off-Canvas--}}
                                {{--</div>--}}

                            {{--<p class="text-semibold text-main">Aside</p>--}}
                            {{--<div class="mar-btm">--}}
                                {{--<div class="pull-right">--}}
                                    {{--<input id="demo-asd-vis" class="toggle-switch" type="checkbox">--}}
                                    {{--<label for="demo-asd-vis"></label>--}}
                                    {{--</div>--}}
                                {{--Visible--}}
                                {{--</div>--}}
                            {{--<div class="mar-btm">--}}
                                {{--<div class="pull-right">--}}
                                    {{--<input id="demo-asd-fixed" class="toggle-switch" type="checkbox" checked>--}}
                                    {{--<label for="demo-asd-fixed"></label>--}}
                                    {{--</div>--}}
                                {{--Fixed Position--}}
                                {{--</div>--}}
                            {{--<div class="mar-btm">--}}
                                {{--<div class="pull-right">--}}
                                    {{--<input id="demo-asd-float" class="toggle-switch" type="checkbox" checked>--}}
                                    {{--<label for="demo-asd-float"></label>--}}
                                    {{--</div>--}}
                                {{--Floating <span class="label label-primary">New</span>--}}
                                {{--</div>--}}
                            {{--<div class="mar-btm">--}}
                                {{--<div class="pull-right">--}}
                                    {{--<input id="demo-asd-align" class="toggle-switch" type="checkbox">--}}
                                    {{--<label for="demo-asd-align"></label>--}}
                                    {{--</div>--}}
                                {{--Left Side--}}
                                {{--</div>--}}
                            {{--<div>--}}
                                {{--<div class="pull-right">--}}
                                    {{--<input id="demo-asd-themes" class="toggle-switch" type="checkbox">--}}
                                    {{--<label for="demo-asd-themes"></label>--}}
                                    {{--</div>--}}
                                {{--Dark Version--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--<div class="col-lg-8">--}}
                        {{--<div id="demo-theme">--}}
                            {{--<div class="row bg-gray-light pad-top">--}}
                                {{--<p class="text-semibold text-main text-lg pad-lft">Color Schemes</p>--}}
                                {{--<div class="demo-theme-btn col-md-4 pad-ver">--}}
                                    {{--<p class="text-semibold text-main">Header</p>--}}
                                    {{--<div class="demo-justify-theme">--}}
                                        {{--<a href="#" class="demo-theme demo-a-light add-tooltip" data-theme="theme-light" data-type="a" data-title="(A). Light">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-a-navy add-tooltip" data-theme="theme-navy" data-type="a" data-title="(A). Navy Blue">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-a-ocean add-tooltip" data-theme="theme-ocean" data-type="a" data-title="(A). Ocean">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--<div class="demo-justify-theme">--}}
                                        {{--<a href="#" class="demo-theme demo-a-lime add-tooltip" data-theme="theme-lime" data-type="a" data-title="(A). Lime">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-a-purple add-tooltip" data-theme="theme-purple" data-type="a" data-title="(A). Purple">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-a-dust add-tooltip" data-theme="theme-dust" data-type="a" data-title="(A). Dust">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--<div class="demo-justify-theme">--}}
                                        {{--<a href="#" class="demo-theme demo-a-mint add-tooltip" data-theme="theme-mint" data-type="a" data-title="(A). Mint">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-a-yellow add-tooltip" data-theme="theme-yellow" data-type="a" data-title="(A). Yellow">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-a-well-red add-tooltip" data-theme="theme-well-red" data-type="a" data-title="(A). Well Red">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--<div class="demo-justify-theme">--}}
                                        {{--<a href="#" class="demo-theme demo-a-coffee add-tooltip" data-theme="theme-coffee" data-type="a" data-title="(A). Coffee">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-a-prickly-pear add-tooltip" data-theme="theme-prickly-pear" data-type="a" data-title="(A). Prickly pear">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-a-dark add-tooltip" data-theme="theme-dark" data-type="a" data-title="(A). Dark">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--<div class="demo-theme-btn col-md-4 pad-ver">--}}
                                    {{--<p class="text-semibold text-main">Brand</p>--}}
                                    {{--<div class="demo-justify-theme">--}}
                                        {{--<a href="#" class="demo-theme demo-b-light add-tooltip" data-theme="theme-light" data-type="b" data-title="(B). Light">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-b-navy add-tooltip" data-theme="theme-navy" data-type="b" data-title="(B). Navy Blue">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-b-ocean add-tooltip" data-theme="theme-ocean" data-type="b" data-title="(B). Ocean">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--<div class="demo-justify-theme">--}}
                                        {{--<a href="#" class="demo-theme demo-b-lime add-tooltip" data-theme="theme-lime" data-type="b" data-title="(B). Lime">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-b-purple add-tooltip" data-theme="theme-purple" data-type="b" data-title="(B). Purple">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-b-dust add-tooltip" data-theme="theme-dust" data-type="b" data-title="(B). Dust">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--<div class="demo-justify-theme">--}}
                                        {{--<a href="#" class="demo-theme demo-b-mint add-tooltip" data-theme="theme-mint" data-type="b" data-title="(B). Mint">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-b-yellow add-tooltip" data-theme="theme-yellow" data-type="b" data-title="(B). Yellow">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-b-well-red add-tooltip" data-theme="theme-well-red" data-type="b" data-title="(B). Well red">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--<div class="demo-justify-theme">--}}
                                        {{--<a href="#" class="demo-theme demo-b-coffee add-tooltip" data-theme="theme-coffee" data-type="b" data-title="(B). Coofee">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-b-prickly-pear add-tooltip" data-theme="theme-prickly-pear" data-type="b" data-title="(B). Prickly pear">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-b-dark add-tooltip" data-theme="theme-dark" data-type="b" data-title="(B). Dark">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--<div class="demo-theme-btn col-md-4 pad-ver">--}}
                                    {{--<p class="text-semibold text-main">Navigation</p>--}}
                                    {{--<div class="demo-justify-theme">--}}
                                        {{--<a href="#" class="demo-theme demo-c-light add-tooltip" data-theme="theme-light" data-type="c" data-title="(C). Light">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-c-navy add-tooltip" data-theme="theme-navy" data-type="c" data-title="(C). Navy Blue">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-c-ocean add-tooltip" data-theme="theme-ocean" data-type="c" data-title="(C). Ocean">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--<div class="demo-justify-theme">--}}
                                        {{--<a href="#" class="demo-theme demo-c-lime add-tooltip" data-theme="theme-lime" data-type="c" data-title="(C). Lime">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-c-purple add-tooltip" data-theme="theme-purple" data-type="c" data-title="(C). Purple">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-c-dust add-tooltip" data-theme="theme-dust" data-type="c" data-title="(C). Dust">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--<div class="demo-justify-theme">--}}
                                        {{--<a href="#" class="demo-theme demo-c-mint add-tooltip" data-theme="theme-mint" data-type="c" data-title="(C). Mint">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-c-yellow add-tooltip" data-theme="theme-yellow" data-type="c" data-title="(C). Yellow">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-c-well-red add-tooltip" data-theme="theme-well-red" data-type="c" data-title="(C). Well Red">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--<div class="demo-justify-theme">--}}
                                        {{--<a href="#" class="demo-theme demo-c-coffee add-tooltip" data-theme="theme-coffee" data-type="c" data-title="(C). Coffee">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-c-prickly-pear add-tooltip" data-theme="theme-prickly-pear" data-type="c" data-title="(C). Prickly pear">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--<a href="#" class="demo-theme demo-c-dark add-tooltip" data-theme="theme-dark" data-type="c" data-title="(C). Dark">--}}
                                            {{--<div class="demo-theme-brand"></div>--}}
                                            {{--<div class="demo-theme-head"></div>--}}
                                            {{--<div class="demo-theme-nav"></div>--}}
                                            {{--</a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--<div id="demo-bg-boxed" class="demo-bg-boxed">--}}
                    {{--<div class="demo-bg-boxed-content">--}}
                        {{--<p class="text-semibold text-main text-lg mar-no">Background Images</p>--}}
                        {{--<p class="text-sm text-muted">Add an image to replace the solid background color</p>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-lg-4 text-justify">--}}
                                {{--<p class="text-semibold text-main">Blurred</p>--}}
                                {{--<div id="demo-blurred-bg" class="text-justify">--}}
                                    {{--<!--Blurred Backgrounds-->--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--<div class="col-lg-4 text-justify">--}}
                                {{--<p class="text-semibold text-main">Polygon &amp; Geometric</p>--}}
                                {{--<div id="demo-polygon-bg" class="text-justify">--}}
                                    {{--<!--Polygon Backgrounds-->--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--<div class="col-lg-4 text-justify">--}}
                                {{--<p class="text-semibold text-main">Abstract</p>--}}
                                {{--<div id="demo-abstract-bg" class="text-justify">--}}
                                    {{--<!--Abstract Backgrounds-->--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--<div class="demo-bg-boxed-footer">--}}
                        {{--<button id="demo-close-boxed-img" class="btn btn-primary">Close</button>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--<button id="demo-set-btn" class="btn" data-toggle="collapse" data-target="#demo-set-body"><i class="demo-psi-gear icon-lg"></i></button>--}}
    {{--</div>--}}
<!--===================================================-->
<!-- END SETTINGS -->


</body>
</html>
