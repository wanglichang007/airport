@extends('admin/header')
{{--<link href="{{ url('/admin/plugins/dropzone/dropzone.min.css') }}" rel="stylesheet">--}}
{{--<script src="{{ url('/admin/plugins/dropzone/dropzone.min.js') }}"></script>--}}
{{--<script src="{{ url('/admin/js/demo/form-file-upload.js') }}"></script>--}}
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
@section('container')
    @parent
    <div class="panel" style="margin-top:10px;padding-top:17px;width:96%;margin-left:2%;">
        <div class="form-group" style="text-align: center">
            <span style="color:black">System ID:</span> <input type="text" value="{{ $systemLogInfo['system_id'] }}" class="form-control" style="height:50px;margin-right: 10px;width:10%;display:inline;color:blue" disabled="disabled">
            <span style="color:black">Form ID:</span> <input type="text" value="{{ $systemLogInfo['from_id'] }}" class="form-control" style="height:50px;margin-right: 10px;width:10%;display:inline;color:blue" disabled="disabled">
            @if (!empty($systemLogInfo['incident_report_time']))
                <span style="color:black">Incident Report Date:</span> <input type="text" name="incident_report_time" disabled="disabled" value="{{ date("Y-m-d", $systemLogInfo['incident_report_time']) }}" class="form-control" style="height:50px;width:20%;display:inline;color:blue" >
            @else
                <span style="color:black">Incident Report Date:</span> <input type="text" name="incident_report_time" disabled="disabled" value="" id="datepicker" class="form-control" style="height:50px;margin-right: 10px;width:20%;display:inline;color: blue">
            @endif
            <span style="color:black">Date Time:</span> <input type="text" class="form-control" id="date-time" style="height:50px;width:20%;display:inline;color: blue" disabled="disabled">
        </div>
    </div>
    @if (!empty($systemLogInfo['view_incident_image']))
        <div style="margin-top:10px;text-align: center " id="images">
            <div style="">
                <img id="img_show" src="{{ $systemLogInfo['view_incident_image'] }}">
            </div>
        </div>
    @endif
@endsection

@section('menu')
    @parent
    <li class="active-link">
        <a href="javascript:history.go(-1);">
            <i class="fa fa-chevron-left"></i>
            <span class="menu-title">
                <strong style="color: white;">Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.hkaastaff') }}">
            <i class="fa fa-line-chart"></i>
            <span class="menu-title">
                <strong style="color: white;">Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.hkaastaff.individual') }}">
            <i class="fa fa-wrench"></i>
            <span class="menu-title">
                <strong style="color: white;">Individual ID</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="javascript://" aria-expanded="true" style="background-color: #658dba">
            <i class="fa fa-list-alt"></i>
            <span class="menu-title">
                <strong>Task Check List</strong>
            </span>
        </a>
        <ul class="collapse in" aria-expanded="true" style="background-color:#212a33;">
            <li style="color:white;"><a href="{{ route('admin.hkaastaff.confirm', ['system_log_id' => $systemLogInfo['system_log_id']]) }}" style="font-size: 13px;">Check List</a></li>
            <li style="color:white;"><a href="{{ route('admin.hkaastaff.checkUsePermit', ['system_log_id' => $systemLogInfo['system_log_id'], 'type' => 'confirm']) }}" style="font-size: 13px;">Use Permit</a></li>
            <li style="border: 1px solid #212a33;background-color: #658dba"><a href="javascript://" style="color:white;font-size: 13px;">Incident Report</a></li>
        </ul>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.hkaastaff.downloadCrontab") }}">
            <i class="fa fa-cloud-download"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Monthly Schedule</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
@endsection
<script>
    function delImage() {
        var msg = "您确定要删除吗？";
        if (confirm(msg)==true){
            $('#img_value').val('');
            $('#img_show').remove();
            return true;
        }else{
            return false;
        }
    }

    function loadFile(file){
        $("#filenames").val(file.name)
    }

    function onTimes(){
        setInterval(function(){
            var dates = new Date().format("yyyy-MM-dd hh:mm:ss");
            $('#date-time').val(dates);
        }, 1000)
    }

    Date.prototype.format = function(format) {
        /*
         * eg:format="YYYY-MM-dd hh:mm:ss";

         */
        var o = {
            "M+" :this.getMonth() + 1, // month
            "d+" :this.getDate(), // day
            "h+" :this.getHours(), // hour
            "m+" :this.getMinutes(), // minute
            "s+" :this.getSeconds(), // second
            "q+" :Math.floor((this.getMonth() + 3) / 3), // quarter
            "S" :this.getMilliseconds()
            // millisecond
        }
        if (/(y+)/.test(format)) {
            format = format.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
        }
        for ( var k in o) {
            if (new RegExp("(" + k + ")").test(format)) {
                format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k]
                    : ("00" + o[k]).substr(("" + o[k]).length));
            }
        }
        return format;
    }

    $(function(){
        jQuery.browser = {};
        jQuery.browser.msie = false;
        jQuery.browser.version = 0;
        if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
        $( "#datepicker" ).datepicker();

        onTimes();

        $("#save").click(function(){
            $("#upload-license").submit();
            return false;
        });
    })
</script>

