@extends('admin/header')
@section('container')
    @parent
    <div class="row pad-ver">
        <form action="javascript://" method="get" class="col-xs-12 col-sm-10 col-sm-offset-1 pad-hor" style="margin-top:150px;" id="system-id">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="System ID" name="systemId" style="height:50px;" autofocus>
                <label class="input-group-btn">
                    <span id="search" class="btn btn-primary" style="height:50px;line-height:35px;">
                        Next
                    </span>
                </label>
            </div>

            <div id="formid" style="margin-top:30px;display:none">
            </div>
            <div class="panel" id="select-tasks" style="margin-top:20px;padding-top:50px;display:none">
                <div class="panel-body">
                    <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%;">
                        <thead>
                        <tr>
                            <th style="text-align: center;width:80%">Form ID</th>
                            <th style="text-align: center">Action</th>
                        </tr>
                        </thead>
                        <tbody id="from-id-content">

                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
@endsection

@section('menu')
    @parent
    <li class="active-link">
        <a href="javascript:history.go(-1);">
            <i class="fa fa-chevron-left"></i>
            <span class="menu-title">
                <strong style="color: white;">Back</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.supervisor.pendingReview') }}">
            <i class="fa fa-eye"></i>
            <span class="menu-title">
                <strong style="color: white;">Waiting Review @if(!empty($reviewNumber)) ({{ $reviewNumber }}) @endif</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.supervisor') }}">
            <i class="fa fa-line-chart"></i>
            <span class="menu-title">
                <strong style="color: white;">Summary</strong>
            </span>
        </a>
    </li>
    <li class="active-link"  style="background-color: #658dba">
        <a href="javascript://">
            <i class="fa fa-wrench"></i>
            <span class="menu-title">
                <strong>Individual ID</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route("admin.supervisor.downloadCrontab") }}">
            <i class="fa fa-cloud-download"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Monthly Schedule</strong>
            </span>
        </a>
    </li>
    <li class="active-link">
        <a href="{{ route('admin.logout') }}">
            <i class="fa fa-power-off"></i>
            <span class="menu-title">
                <strong style="color: white;font-size: 16px;">Logout</strong>
            </span>
        </a>
    </li>
@endsection

<script type="text/javascript" src="{{ url('/admin/js/jquery-2.2.4.min.js') }}"></script>
<script>
    $(function(){
        $(document).keyup(function(event){
            if(event.keyCode ==13){
                $("#search").trigger("click");
            }
        });

        $('#search').click(function(){
            getSystemId();
        })
    })

    function getSystemId(){
        var systemId = $("input[name=systemId]").val();
        $.get("{{ route('admin.inspector.formid') }}", { systemId:systemId }, function (data) {
            if (data.code != 0) {
                $('#select-tasks').hide();
                $('#formid').show();
                $('#formid').text(data.message);
            } else {
                $('#select-tasks').show();
                $('#formid').hide();
                $('#from-id-content').html("");
                $.each(data.data, function(index, value){
                    var htm = '<tr><td style="text-align: center;color: black">'+value.from_id+'</td><td style="text-align: center">';
                    if(value.isNewInput == true) {
                        htm += '<label class="input-group-btn" style="display:inline;">' +
                            '<span class="btn btn-primary" disabled="disabled" style="height:30px;line-height:15px;width:100px;">' +
                            'New Input' +
                            '</span>' +
                            '</label>';
                    } else {
                        if (value.systemLogStatus == 0) {
                            htm += '<label class="input-group-btn" style="display:inline;">' +
                                '<span class="btn btn-primary" disabled="disabled" style="height:30px;line-height:15px;width:100px;">' +
                                'Re-Visit' +
                                '</span>' +
                                '</label>';
                        } else if (value.systemLogStatus == 1) {
                            htm += '<label class="input-group-btn" style="display:inline;">' +
                                '<a href="{{ route("admin.supervisor.task") }}?system_log_id='+value.systemLogId+'">' +
                                '<span class="btn btn-primary" style="height:30px;line-height:15px;width:120px;">' +
                                'Review' +
                                '</span>' +
                                '</a>' +
                                '</label>';
                        } else if (value.systemLogStatus == 2) {
                            htm += '<label class="input-group-btn" style="display:inline;">' +
                                '<a href="{{ route("admin.supervisor.task") }}?system_log_id='+value.systemLogId+'">' +
                                '<span class="btn btn-primary" style="height:30px;line-height:15px;width:120px;">' +
                                'Review Passed' +
                                '</span>' +
                                '</a>' +
                                '</label>';
                        }
                    }

                    $('#from-id-content').append(htm);
                })
            }
        }, 'json');
    }
</script>

