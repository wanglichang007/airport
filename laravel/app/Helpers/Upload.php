<?php

namespace App\Helpers;
use Illuminate\Support\Facades\Storage;

class Upload{
    public function uploadImage($file){
        $status= ['code' => 0, 'msg' => 'Success'];

        $originalName = $file->getClientOriginalName(); // 文件原名

        $tmp = explode('.', $originalName);
        $suffix = end($tmp);

        $tim = time();
        $path = date('Y', $tim).'/'.date('m', $tim).'/'.date('d', $tim).'/'.md5($originalName).'.'.$suffix;
        $src_path=$file->getRealPath();

        Storage::disk('local')->put('/image/'.$path, file_get_contents($src_path));

        $image_url = env('IMAGE_URL');
        $status['filePath'] = '/storage/'.$path;
        $status['viewPath'] = $image_url.'/storage/'.$path;
        return $status;
    }

    public function uploadCrontab($file){
        $status= ['code' => 0, 'msg' => 'Success'];

        $originalName = $file->getClientOriginalName(); // 文件原名

        $tmp = explode('.', $originalName);
        $suffix = end($tmp);

        $tim = time();
        $path = 'systemCron.'.$suffix;
        $src_path=$file->getRealPath();

        Storage::disk('local')->put('/image/'.$path, file_get_contents($src_path));

        $image_url = env('IMAGE_URL');
        $status['filePath'] = '/storage/'.$path;
        $status['viewPath'] = $image_url.'/storage/'.$path;
        return $status;
    }
}
