<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class SystemLog extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'system_log';

    /**
     * @var string
     */
    protected $primaryKey = 'system_log_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * 获取一条记录
     *
     */
    public function getOne($params=[]){
        if(isset($params[$this->primaryKey])){
            $data = self::find($params[$this->primaryKey]);
        }else{
            $data = self::where($params)->orderBy('system_log_id', 'desc')->first();
        }

        if (!empty($data)) {
            $data = $data->toArray();
            if (!empty($data['task'])) {
                $data['task'] = json_decode($data['task'], true);
                foreach ($data['task'] as $key=>$value) {
                    if (!empty($value['image'])) {
                        $data['task'][$key]['view_image'] = env("IMAGE_URL").$value['image'];
                    }
                    if (!empty($value['supervisor_image'])) {
                        $data['task'][$key]['view_supervisor_image'] = env("IMAGE_URL").$value['supervisor_image'];
                    }
                }
            } else {
                $data['task'] = [];
            }

            if (!empty($data['sign'])) {
                $data['view_sign'] = env("IMAGE_URL").$data['sign'];
            }

            if (!empty($data['supervisor_sign'])) {
                $data['view_supervisor_sign'] = env("IMAGE_URL").$data['supervisor_sign'];
            }

            if (!empty($data['hkaa_sign'])) {
                $data['view_hkaa_sign'] = env("IMAGE_URL").$data['hkaa_sign'];
            }

            if (!empty($data['license_image'])) {
                $data['view_license_image'] = env("IMAGE_URL").$data['license_image'];
            }

            if (!empty($data['incident_image'])) {
                $data['view_incident_image'] = env("IMAGE_URL").$data['incident_image'];
            }
            return $data;
        }
        return [];
    }


    /**
     * 获取多条记录
     *
     */
    public function getMore($params=[],$offset=0,$limit=2000,$orderBy='asc'){
        $res = self::where($params)->orderBy($this->primaryKey, $orderBy)->offset($offset)->limit($limit)->get()->toArray();
        return $res;
    }

    /**
     * 修改一条记录
     *
     */
    public function updateOne($primaryKey,$params=[]){
        $id = (int)$primaryKey;
        $flag = self::where($this->primaryKey, $id)->update($params);
        return $flag;
    }

    //创建
    public function createOne($data) {
        $res = self::create($data);
        return $res->system_log_id;
    }

    //获取列表
    public function getList($params = []) {
        $where = !empty($params['where']) ? $params['where'] : [];
        $pageSize = !empty($params['pageSize']) ? $params['pageSize'] : 20;
        return self::where($where)->orderBy($this->primaryKey, 'desc')->paginate($pageSize);
    }

    //Inspector_Record Supervisor_Comment模板
    public function insectorSupervisorBlade($systemLogId, $type){
        $systemLogInfo = $this->getOne(['system_log_id' => $systemLogId]);
        if (empty($systemLogInfo['task'])) {
            $systemModel = new System();
            $systemLogInfo['task'] = $systemModel->getTaskBySystemIdFromId($systemLogInfo['system_id'], $systemLogInfo['from_id']);
        }
        $taskModel = new Task();

        $startEndTime = '<tr></tr><tr>';
        $startEndTime .= '<td style="width:300%">';
        $startEndTime .= "System ID:  ".$systemLogInfo['system_id'];
        $startEndTime .= '</td>';
        $startEndTime .= '<td style="width:300%">';
        $startEndTime .= "Form ID:  ".$systemLogInfo['from_id'];
        $startEndTime .= '</td>';
        $startEndTime .= '<td style="width:300%">';
        $startEndTime .= "Date Time:  ".date("Y-m-d H:i:s", time());
        $startEndTime .= '</td>';
        $startEndTime .= '</tr><tr></tr>';


        if ($type == 'hkaa') {
            $startEndTime .= '<tr>';
            $startEndTime .= '<td style="width:300%">';
            $startEndTime .= 'HKAA Remarks: ';
            $startEndTime .= '</td>';
            $startEndTime .= $systemLogInfo['hkaa_remarks'];
            $startEndTime .= '<td style="width:300%">';
            $startEndTime .= '</td>';
            $startEndTime .= '</tr><tr></tr>';
        }

        if ($type == 'supervisor'){
            $startEndTime .= '<tr>';
            $startEndTime .= '<td style="width:300%">';
            $startEndTime .= 'Supervisor Review';
            $startEndTime .= '</td>';
            $startEndTime .= '</tr><tr></tr>';
        }

        if ($type == 'inspector'){
            $startEndTime .= '<tr>';
            $startEndTime .= '<td style="width:300%">';
            $startEndTime .= 'Maintenance Check List';
            $startEndTime .= '</td>';
            $startEndTime .= '</tr><tr></tr>';
        }

        $html = '';
        if (!empty($systemLogInfo['task'])) {
            $html = '
                <table id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%;" border="1">
                    '.$startEndTime.'
                    <thead>
                        <tr>
                            <th colspan="7" style="text-align: center;">Inspector Record</th>
                            <th colspan="1" style="text-align: center;">Supervisor Remarks</th>
                        </tr>
                        <tr>
                            <th style="text-align: center">ID</th>
                            <th style="text-align: center;width:300%">Clause Ref.</th>
                            <th style="text-align: center;width:500%">Task/任務描述</th>
                            <th style="text-align: center">Yes</th>
                            <th style="text-align: center">No</th>
                            <th style="text-align: center">NA</th>
                            <th style="text-align: center;width:300%">Remarks</th>
                            <th style="text-align: center;width:300%">Follow Up</th>
                        </tr>
                    </thead>
                    <tbody id="from-id-content">
                ';

            $trs = '';
            $i = 0;
            foreach($systemLogInfo['task'] as $key => $value) {
                $taskInfo = $taskModel->getOne(['task_id' => $value['task_id']]);
                $i++;
                $tr = '<tr style="height:100px;"><td style="text-align: center;">'.$i.'</td>';
                $tr .= '<td style="text-align: center;">'.$taskInfo['clause_no'].'</td>';
                $tr .= '<td style="text-align: center;">'.$taskInfo['name'].'</td>';
                $tr .= '<td style="text-align: center;">';
                if (isset($value['status']) && $value['status'] == 1){
                    $tr .= '☑';
                }
                $tr .= '</td>';
                $tr .= '<td style="text-align: center;">';
                if (isset($value['status']) && $value['status'] == 2){
                    $tr .= '☑';
                }
                $tr .= '</td>';
                $tr .= '<td style="text-align: center;">';
                if (isset($value['status']) && $value['status'] == 3){
                    $tr .= '☑';
                }
                $tr .= '</td>';
                if (!empty($value['remarks'])) {
                    $tr .= '<td style="text-align: center;">'.$value['remarks'].'</td>';
                } else {
                    $tr .= '<td style="text-align: center;"></td>';
                }

                if (!empty($value['supervisor_remarks'])) {
                    $tr .= '<td style="text-align: center;">'.$value['supervisor_remarks'].'</td>';
                } else {
                    $tr .= '<td style="text-align: center;"></td>';
                }

                $tr .= '</tr>';

                $trs .= $tr;
            }

            $html .= $trs;

            $html .= '</tbody></table>';
        }

        return $html;
    }

    //spervisor_summary
    public function spervisorSummaryBlade($startTime, $endTime, $type = 0, $brand = 0){
        $systemCrontabModel = new SystemCrontab();
        $data = $systemCrontabModel->getChartsByTime($startTime, $endTime, $type, $brand);

        $startEndTime = '<tr></tr><tr>';
        $startEndTime .= '<td style="width:300%">';
        $startEndTime .= "Start Data:  ".date("Y-m-d", $startTime);
        $startEndTime .= '</td>';
        $startEndTime .= '<td style="width:300%">';
        $startEndTime .= "End Data:  ".date("Y-m-d", $endTime);
        $startEndTime .= '</td>';
        $startEndTime .= '<td style="width:300%">';
        $startEndTime .= "Date Time:  ".date("Y-m-d H:i:s", time());
        $startEndTime .= '</td>';
        $startEndTime .= '</tr>';

        $typeInfo = [];
        $brandInfo = [];
        $typeModel = new Equipment();
        $brandModel = new Brand();
        if (!empty($type)) {
            $typeInfo = $typeModel->getOne(['equipment_id' => $type]);
        }

        if (!empty($brand)) {
            $brandInfo = $brandModel->getOne(['brand_id' => $brand]);
        }
        $typeBrand = '';
        if (!empty($typeInfo)) {
            $typeBrand .= '<tr></tr><tr>';
            $typeBrand .= '<th style="width:300%">';
            $typeBrand .= 'Summary:  '.$typeInfo['name'];
            $typeBrand .= '</th>';
            if (!empty($brandInfo)) {
                $typeBrand .= '<th style="width:300%">';
                $typeBrand .= 'Brand Name:  '.$brandInfo['name'];
                $typeBrand .= '</th>';
            }
            $typeBrand .= '</tr>';
        }

        $title = '<tr></tr><tr><td style="width:300%">Summary</td></tr><tr></tr>';

        $html = '<table id="demo-dt-addrow" class="table table-striped" cellspacing="0" width="100%;">
                '.$startEndTime.$typeBrand.$title.'
                <thead>
                    <tr style="background-color: white;">
                        <th style="text-align: center;"></th>
                        <th style="text-align: center;width:500%"></th>
                        <th style="text-align: center;">Quantity</th>
                        <th style="text-align: center">%</th>
                        <th style="text-align: center">Use Permit</th>
                        <th style="text-align: center">Incident Report</th>
                    </tr>
                </thead>
                <tbody id="from-id-content">';
        if (!empty($data)) {
            $normalConditionsRatio = round($data['normalConditionsNumber'] / $data['totalNumber'] * 100, 1);
            $followUpActionsRatio = round($data['followUpActionsNumber'] / $data['totalNumber'] * 100, 1);
            $ratio = [
                'totalRatio' => 100,
                'normalConditionsRatio' => $normalConditionsRatio,
                'followUpActionsRatio' => $followUpActionsRatio,
                'pendingSubmissionRatio' => 100 - $normalConditionsRatio - $followUpActionsRatio
            ];

            $html .= '<tr style="background-color: white;">
                            <td style="text-align: center;">1</td>
                            <td>Total number of Maintenance Checklist</td>
                            <td style="text-align: center;">'.$data['totalNumber'].'</td>
                            <td style="text-align: center;">100</td>
                            <td style="text-align: center;">'.$data['totalUserPermitNumber'].'</td>
                            <td style="text-align: center;">'.$data['totalIncidentReportNumber'].'</td>
                        </tr>';

            $html .= '<tr style="background-color: white;">
                            <td style="text-align: center;">2</td>
                            <td>Normal Conditions</td>
                            <td style="text-align: center;">'.$data['normalConditionsNumber'].'</td>
                            <td style="text-align: center;">'.$ratio['normalConditionsRatio'].'</td>
                            <td style="text-align: center;">'.$data['normalConditionsUserPermitNumber'].'</td>
                            <td style="text-align: center;">'.$data['normalConditionsIncidentReportNumber'].'</td>
                        </tr>';

            $html .= '<tr style="background-color: white;">
                            <td style="text-align: center;">3</td>
                            <td>Checklist with Follow Up Actions</td>
                            <td style="text-align: center;">'.$data['followUpActionsNumber'].'</td>
                            <td style="text-align: center;">'.$ratio['followUpActionsRatio'].'</td>
                            <td style="text-align: center;">'.$data['followUpActionsUserPermitNumber'].'</td>
                            <td style="text-align: center;">'.$data['followUpActionsIncidentReportNumber'].'</td>
                        </tr>';

            $html .= '<tr style="background-color: white;">
                            <td style="text-align: center;">4</td>
                            <td>Checklist Pending Submission</td>
                            <td style="text-align: center;">'.$data['pendingSubmissionNumber'].'</td>
                            <td style="text-align: center;">'.$ratio['pendingSubmissionRatio'].'</td>
                            <td style="text-align: center;">'.$data['pendingSubmissionUserPermitNumber'].'</td>
                            <td style="text-align: center;">'.$data['pendingSubmissionIncidentReportNumber'].'</td>
                        </tr>';
        } else {
            $html .= '<tr style="background-color: white;">
                            <td style="text-align: center;">1</td>
                            <td>Total number of Maintenance Checklist</td>
                            <td style="text-align: center;">0</td>
                            <td style="text-align: center;">0</td>
                            <td style="text-align: center;">0</td>
                            <td style="text-align: center;">0</td>
                        </tr>

                        <tr style="background-color: white;">
                            <td style="text-align: center;">2</td>
                            <td>Normal Conditions</td>
                            <td style="text-align: center;">0</td>
                            <td style="text-align: center;">0</td>
                            <td style="text-align: center;">0</td>
                            <td style="text-align: center;">0</td>
                        </tr>

                        <tr style="background-color: white;">
                            <td style="text-align: center;">3</td>
                            <td>Checklist with Follow Up Actions</td>
                            <td style="text-align: center;">0</td>
                            <td style="text-align: center;">0</td>
                            <td style="text-align: center;">0</td>
                            <td style="text-align: center;">0</td>
                        </tr>

                        <tr style="background-color: white;">
                            <td style="text-align: center;">4</td>
                            <td>Checklist Pending Submission</td>
                            <td style="text-align: center;">0</td>
                            <td style="text-align: center;">0</td>
                            <td style="text-align: center;">0</td>
                            <td style="text-align: center;">0</td>
                        </tr>';
        }

        $html .= '</tbody>
            </table>';

        return $html;
    }

    //1、2、3、4四种统计列表模板
    public function menuListBlade($menu, $startTime, $endTime, $type = 0, $brand = 0){
        $systemCrontabModel = new SystemCrontab();
        if ($menu == 1) {       //Total Number of Equipment统计列表
            $data = $systemCrontabModel->getTotalData($startTime, $endTime, $type, $brand);
            $systemInfo = $data['systemInfo'];
            foreach ($systemInfo as $key=>$value) {
                if (!empty($value['system_log'])) {
                    $systemInfo[$key]['work_time'] = $value['system_log']['work_time'];
                    $systemInfo[$key]['license_image'] = $value['system_log']['license_image'];
                    $systemInfo[$key]['license_expiry_time'] = $value['system_log']['license_expiry_time'];
                    $systemInfo[$key]['incident_image'] = $value['system_log']['incident_image'];
                    $systemInfo[$key]['incident_report_time'] = $value['system_log']['incident_report_time'];
                    $systemInfo[$key]['sign_time'] = $value['system_log']['sign_time'];
                    $systemInfo[$key]['task'] = $value['system_log']['task'];
                } else {
                    $systemInfo[$key]['work_time'] = 0;
                    $systemInfo[$key]['license_image'] = '';
                    $systemInfo[$key]['license_expiry_time'] = 0;
                    $systemInfo[$key]['incident_image'] = '';
                    $systemInfo[$key]['incident_report_time'] = 0;
                    $systemInfo[$key]['sign_time'] = 0;
                    $systemInfo[$key]['task'] = [];
                }
            }
            $data = $systemInfo;
        } else if ($menu == 2) {        //Normal Conditions统计列表
            $data = $systemCrontabModel->getNormalConditionsData($startTime, $endTime, $type, $brand);
        } else if ($menu == 3) {        //Checklist with Follow Up Actions统计列表
            $data = $systemCrontabModel->getFollowUpActionsData($startTime, $endTime, $type, $brand);
        } else if ($menu == 4) {        //Checklist Pending Submission统计列表
            $data = $systemCrontabModel->getPendingSubmissionData($startTime, $endTime, $type, $brand);
            foreach ($data as $key=>$value) {
                if (!empty($value['systemLogData'])) {
                    $data[$key]['work_time'] = $value['systemLogData']['work_time'];
                    $data[$key]['license_image'] = $value['systemLogData']['license_image'];
                    $data[$key]['license_expiry_time'] = $value['systemLogData']['license_expiry_time'];
                    $data[$key]['incident_image'] = $value['systemLogData']['incident_image'];
                    $data[$key]['incident_report_time'] = $value['systemLogData']['incident_report_time'];
                    $data[$key]['sign_time'] = $value['systemLogData']['sign_time'];
                    $data[$key]['task'] = $value['systemLogData']['task'];
                } else {
                    $data[$key]['work_time'] = 0;
                    $data[$key]['license_image'] = '';
                    $data[$key]['license_expiry_time'] = 0;
                    $data[$key]['incident_image'] = '';
                    $data[$key]['incident_report_time'] = 0;
                    $data[$key]['sign_time'] = 0;
                    $data[$key]['task'] = [];
                }
            }
        }

        $typeInfo = [];
        $brandInfo = [];
        $typeModel = new Equipment();
        $brandModel = new Brand();
        if (!empty($type)) {
            $typeInfo = $typeModel->getOne(['equipment_id' => $type]);
        }

        if (!empty($brand)) {
            $brandInfo = $brandModel->getOne(['brand_id' => $brand]);
        }

        $startEndTime = '<tr></tr><tr>';
        $startEndTime .= '<td style="width:300%">';
        $startEndTime .= "Start Data:  ".date("Y-m-d", $startTime);
        $startEndTime .= '</td>';
        $startEndTime .= '<td style="width:300%">';
        $startEndTime .= "End Data:  ".date("Y-m-d", $endTime);
        $startEndTime .= '</td>';
        $startEndTime .= '<td style="width:300%">';
        $startEndTime .= "Date Time:  ".date("Y-m-d H:i:s", time());
        $startEndTime .= '</td>';
        $startEndTime .= '</tr>';

        $typeBrand = '';
        if (!empty($typeInfo)) {
            $typeBrand .= '<tr></tr><tr>';
            $typeBrand .= '<th style="width:300%">';
            $typeBrand .= 'System Type:  '.$typeInfo['name'];
            $typeBrand .= '</th>';
            if (!empty($brandInfo)) {
                $typeBrand .= '<th style="width:300%">';
                $typeBrand .= 'Brand Name:  '.$brandInfo['name'];
                $typeBrand .= '</th>';
            }
            $typeBrand .= '</tr>';
        }

        $titleNumber = '';
        if ($menu == 1) {
            $titleNumber .= '<tr></tr><tr>';
            $titleNumber .= '<td style="width:300%">';
            $titleNumber .= '1. Total number of Maintenance Checklist';
            $titleNumber .= '</td>';
            $titleNumber .= '</tr>';
            $titleNumber .= '<tr></tr>';
            $titleNumber .= '<tr>';
            $titleNumber .= '<td style="width:300%">';
            $titleNumber .= 'Total Progress:'.count($data);
            $titleNumber .= '</td>';
            $titleNumber .= '</tr><tr></tr>';
        } else if ($menu == 2) {
            $titleNumber .= '<tr></tr><tr>';
            $titleNumber .= '<td style="width:300%">';
            $titleNumber .= '2. Normal Conditions';
            $titleNumber .= '</td>';
            $titleNumber .= '</tr>';
            $titleNumber .= '<tr></tr>';
            $titleNumber .= '<tr>';
            $titleNumber .= '<td style="width:300%">';
            $titleNumber .= 'Total Progress:'.count($data);
            $titleNumber .= '</td>';
            $titleNumber .= '</tr><tr></tr>';
        } else if ($menu == 3) {
            $titleNumber .= '<tr></tr><tr>';
            $titleNumber .= '<td style="width:300%">';
            $titleNumber .= '3. Checklist with Follow Up Actions';
            $titleNumber .= '</td>';
            $titleNumber .= '</tr>';
            $titleNumber .= '<tr></tr>';
            $titleNumber .= '<tr>';
            $titleNumber .= '<td style="width:300%">';
            $titleNumber .= 'Total Progress:'.count($data);
            $titleNumber .= '</td>';
            $titleNumber .= '</tr><tr></tr>';
        } else if ($menu == 4) {
            $titleNumber .= '<tr></tr><tr>';
            $titleNumber .= '<td style="width:300%">';
            $titleNumber .= '4. Checklist Pending Submission';
            $titleNumber .= '</td>';
            $titleNumber .= '</tr>';
            $titleNumber .= '<tr></tr>';
            $titleNumber .= '<tr>';
            $titleNumber .= '<td style="width:300%">';
            $titleNumber .= 'Total Progress:'.count($data);
            $titleNumber .= '</td>';
            $titleNumber .= '</tr><tr></tr>';
        }

        $html = '<table data-toggle="table" id="demo-dt-addrow" class="table table-striped table-bordered" cellspacing="0" width="100%;" data-sort-name="id">
                <thead>
                    '.$startEndTime.$typeBrand.$titleNumber.'
                    <tr>
                        <th data-field="system_id" data-align="center" data-sortable="true" style="width:300%">
                            System ID
                        </th>
                        <th data-field="from_id" data-align="center" style="width:300%">
                            Form ID
                        </th>
                        <th data-field="work_time" data-align="center" data-sortable="true" style="width:300%">
                            Scheduled Data
                        </th>
                        <th data-field="license_image" data-align="center" style="width:300%">
                            USE Permit
                        </th>
                        <th data-field="license_expiry_time" data-align="center" data-sortable="true" style="width:300%">
                            Expiry Date
                        </th>
                        <th data-field="incident_image" data-align="center" style="width:300%">
                            Incident Report
                        </th>
                        <th data-field="incident_report_time" data-align="center" data-sortable="true" style="width:300%">
                            RPT Date
                        </th>
                        <th data-field="incident_report_time" data-align="center" data-sortable="true" style="width:300%">
                            Submit Date
                        </th>
                        <th data-field="task_supervisor_remark" data-align="center" style="width:300%">
                            Follow Up
                        </th>
                    </tr>
                </thead>
                <tbody>';

        foreach ($data as $value) {
            $html .= '<tr>';
            $html .= '<td>'.$value['system_id'].'</td>';
            $html .= '<td>'.$value['from_id'].'</td>';
            if (!empty($value['work_time'])) {
                $html .= '<td>'.date("Y-m-d", $value['work_time']).'</td>';
            } else {
                $html .= '<td></td>';
            }

            if (!empty($value['license_image'])) {
                $html .= '<td>✓</td>';
            } else {
                $html .= '<td></td>';
            }
            if (!empty($value['license_expiry_time'])) {
                $html .= '<td>'.date('Y-m-d', $value['license_expiry_time']).'</td>';
            } else {
                $html .= '<td></td>';
            }
            if (!empty($value['incident_image'])) {
                $html .= '<td>✓</td>';
            } else {
                $html .= '<td></td>';
            }
            if (!empty($value['incident_report_time'])) {
                $html .= '<td>'.date('Y-m-d', $value['incident_report_time']).'</td>';
            } else {
                $html .= '<td></td>';
            }
            if (!empty($value['sign_time'])) {
                $html .= '<td>'.date('Y-m-d', $value['sign_time']).'</td>';
            } else {
                $html .= '<td></td>';
            }
            $html .= '<td>';
            if (!empty($value['task'])) {
                foreach ($value['task'] as $v) {
                    if (!empty($v['supervisor_remarks'])) {
                        $html .= '✓';
                        break;
                    }
                }
            }
            $html .= '</td>';
            $html .= '</tr>';
        }

        $html .= '</tbody>
            </table>';

        return $html;
    }
}
