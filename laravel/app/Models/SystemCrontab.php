<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemCrontab extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'system_crontab';

    /**
     * @var string
     */
    protected $primaryKey = 'system_crontab_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * 获取一条记录
     *
     */
    public function getOne($params=[]){
        if(isset($params[$this->primaryKey])){
            $data = self::find($params[$this->primaryKey]);
        }else{
            $data = self::where($params)->orderBy('system_crontab_id', 'desc')->first();
        }

        if (!empty($data)) {
            return $data->toArray();
        }
        return [];
    }

    /**
     * 获取多条记录
     *
     */
    public function getMore($params=[]){
        $res = self::where($params)->orderBy('system_crontab_id', 'desc')->get()->toArray();
        return $res;
    }

    /**
     * 修改一条记录
     *
     */
    public function updateOne($primaryKey,$params=[]){
        $id = (int)$primaryKey;
        $flag = self::where($this->primaryKey, $id)->update($params);
        return $flag;
    }


    //创建
    public function createOne($data) {
        $res = self::create($data);
        return $res->system_crontab_id;
    }

    //获取列表
    public function getList($params = []) {
        $where = !empty($params['where']) ? $params['where'] : [];
        $pageSize = !empty($params['pageSize']) ? $params['pageSize'] : 20;
        return self::where($where)->orderBy($this->primaryKey, 'desc')->paginate($pageSize);
    }

    //根据时间段/类型/品牌获取图表页数据
    public function getChartsByTime($startTime, $endTime, $type = 0, $brand = 0) {
        $totalData = $this->getTotalData($startTime, $endTime, $type, $brand);
        $normalConditionsData = $this->getNormalConditionsData($startTime, $endTime, $type, $brand);
        $followUpActionsData = $this->getFollowUpActionsData($startTime, $endTime, $type, $brand);
        $pendingSubmissionData = $this->getPendingSubmissionData($startTime, $endTime, $type, $brand);

        //获取Total Number of Equipment数据
        $totalNumber = count($totalData['systemInfo']);
        $totalUserPermitNumber = 0;
        $totalIncidentReportNumber = 0;
        foreach($totalData['systemLogInfo'] as $value) {
            if (!empty($value['license_image'])) {
                $totalUserPermitNumber += 1;
            }
            if (!empty($value['incident_image'])) {
                $totalIncidentReportNumber += 1;
            }
        }
        //获取Normal Conditions数据
        $normalConditionsNumber = count($normalConditionsData);
        $normalConditionsUserPermitNumber = 0;
        $normalConditionsIncidentReportNumber = 0;
        foreach($normalConditionsData as $value) {
            if (!empty($value['license_image'])) {
                $normalConditionsUserPermitNumber += 1;
            }
            if (!empty($value['incident_image'])) {
                $normalConditionsIncidentReportNumber += 1;
            }
        }
        //获取Checklist with Follow Up Actions数据
        $followUpActionsNumber = count($followUpActionsData);
        $followUpActionsUserPermitNumber = 0;
        $followUpActionsIncidentReportNumber = 0;
        foreach($followUpActionsData as $value) {
            if (!empty($value['license_image'])) {
                $followUpActionsUserPermitNumber += 1;
            }
            if (!empty($value['incident_image'])) {
                $followUpActionsIncidentReportNumber += 1;
            }
        }
        //获取Checklist Pending Submission数据
        $pendingSubmissionNumber = $totalNumber - $normalConditionsNumber - $followUpActionsNumber;
        $pendingSubmissionUserPermitNumber = 0;
        $pendingSubmissionIncidentReportNumber = 0;
        foreach($pendingSubmissionData as $value) {
            if (!empty($value['systemLogData']['license_image'])) {
                $pendingSubmissionUserPermitNumber += 1;
            }
            if (!empty($value['systemLogData']['incident_image'])) {
                $pendingSubmissionIncidentReportNumber += 1;
            }
        }

        if ($totalNumber != 0) {
            return [
                'totalNumber' => $totalNumber,
                'normalConditionsNumber' => $normalConditionsNumber,
                'followUpActionsNumber' => $followUpActionsNumber,
                'pendingSubmissionNumber' => $pendingSubmissionNumber,
                'totalUserPermitNumber' => $totalUserPermitNumber,
                'totalIncidentReportNumber' => $totalIncidentReportNumber,
                'normalConditionsUserPermitNumber' => $normalConditionsUserPermitNumber,
                'normalConditionsIncidentReportNumber' => $normalConditionsIncidentReportNumber,
                'followUpActionsUserPermitNumber' => $followUpActionsUserPermitNumber,
                'followUpActionsIncidentReportNumber' => $followUpActionsIncidentReportNumber,
                'pendingSubmissionUserPermitNumber' => $pendingSubmissionUserPermitNumber,
                'pendingSubmissionIncidentReportNumber' => $pendingSubmissionIncidentReportNumber
            ];
        } else {
            return [];
        }

    }

    /*
     * 获取Total Number of Equipment数据
     */
    public function getTotalData($startTime, $endTime, $type = 0, $brand = 0){
        $needWork = $this->getMore([["work_time", ">=", $startTime], ["work_time", "<=", $endTime]]);

        $res = [];
        $system = [];
        $systemLog = [];
        $systemModel = new System();
        $systemLogModel = new SystemLog();

        if (!empty($needWork)) {
            if (!empty($type)) {
                //类型、品牌都存在
                if (!empty($brand)) {
                    //验证systemId是否属于这些类型或品牌
                    foreach ($needWork as $value) {
                        $systemInfo = $systemModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id'], 'system_type_id' => $type, 'brand_type_id' => $brand]);
                        if (!empty($systemInfo)) {
                            $systemInfo['system_log'] = [];
                            $systemInfo['work_time'] = $value['work_time'];

                            //获取已经进行的systemLog
                            $defStartTime = date("Y-m-01 00:00:00", strtotime(date("Y-m-d", $value['work_time'])));
                            $defEndTime = date('Y-m-d 23:59:59', strtotime("$defStartTime +1 month -1 day"));
                            $systemLogInfo = $systemLogModel->getOne(['s_id' => $systemInfo['id'], ['create_time', '>=', strtotime($defStartTime)], ['create_time', '<=', strtotime($defEndTime)]]);
                            if (!empty($systemLogInfo)) {
                                //将计划工作日期放进
                                $systemLogInfo['work_time'] = $value['work_time'];
                                $systemLog[] = $systemLogInfo;
                                $systemInfo['system_log'] = $systemLogInfo;
                            }

                            $system[] = $systemInfo;
                        }
                    }
                } else {
                    //验证systemId是否属于这些类型
                    foreach ($needWork as $value) {
                        $systemInfo = $systemModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id'], 'system_type_id' => $type]);
                        if (!empty($systemInfo)) {
                            $systemInfo['system_log'] = [];
                            $systemInfo['work_time'] = $value['work_time'];

                            //获取已经进行的systemLog
                            $defStartTime = date("Y-m-01 00:00:00", strtotime(date("Y-m-d", $value['work_time'])));
                            $defEndTime = date('Y-m-d 23:59:59', strtotime("$defStartTime +1 month -1 day"));
                            $systemLogInfo = $systemLogModel->getOne(['s_id' => $systemInfo['id'], ['create_time', '>=', strtotime($defStartTime)], ['create_time', '<=', strtotime($defEndTime)]]);
                            if (!empty($systemLogInfo)) {
                                //将计划工作日期放进
                                $systemLogInfo['work_time'] = $value['work_time'];
                                $systemLog[] = $systemLogInfo;
                                $systemInfo['system_log'] = $systemLogInfo;
                            }

                            $system[] = $systemInfo;
                        }
                    }
                }
            } else {
                foreach ($needWork as $value) {
                    $systemInfo = $systemModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id']]);
                    if (!empty($systemInfo)) {
                        $systemInfo['system_log'] = [];
                        $systemInfo['work_time'] = $value['work_time'];

                        //获取已经进行的systemLog
                        $defStartTime = date("Y-m-01 00:00:00", strtotime(date("Y-m-d", $value['work_time'])));
                        $defEndTime = date('Y-m-d 23:59:59', strtotime("$defStartTime +1 month -1 day"));
                        $systemLogInfo = $systemLogModel->getOne(['s_id' => $systemInfo['id'], ['create_time', '>=', strtotime($defStartTime)], ['create_time', '<=', strtotime($defEndTime)]]);
                        if (!empty($systemLogInfo)) {
                            //将计划工作日期放进
                            $systemLogInfo['work_time'] = $value['work_time'];
                            $systemLog[] = $systemLogInfo;
                            $systemInfo['system_log'] = $systemLogInfo;
                        }

                        $system[] = $systemInfo;
                    }
                }
            }
        }

        $res['systemInfo'] = $system;
        $res['systemLogInfo'] = $systemLog;

        return $res;
    }

    /*
     * 获取Normal Conditions数据
     */
    public function getNormalConditionsData($startTime, $endTime, $type = 0, $brand = 0){
        $needWork = $this->getMore([["work_time", ">=", $startTime], ["work_time", "<=", $endTime]]);

        $res = [];
        $systemModel = new System();
        $systemLogModel = new SystemLog();
        if (!empty($needWork)) {
            if (!empty($type)) {
                //类型、品牌都存在
                if (!empty($brand)) {
                    //验证systemId是否属于这些类型或品牌
                    foreach ($needWork as $value) {
                        $systemInfo = $systemModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id'], 'system_type_id' => $type, 'brand_type_id' => $brand]);
                        if (!empty($systemInfo)) {
                            $systemLogInfo = $systemLogModel->getOne(['s_id' => $systemInfo['id']]);
                            if (!empty($systemLogInfo)) {
                                //将计划工作日期放进
                                $systemLogInfo['work_time'] = $value['work_time'];
                                //验证已经开始做的，并且supervisor角色是否已签名（即完成）//最新修改inspector签名即算完成
                                $defStartTime = date("Y-m-01 00:00:00", strtotime(date("Y-m-d", $value['work_time'])));
                                $defEndTime = date('Y-m-d 23:59:59', strtotime("$defStartTime +1 month -1 day"));
                                if ($systemLogInfo['status'] >= 1 && $systemLogInfo['create_time'] >= strtotime($defStartTime) && $systemLogInfo['create_time'] <= strtotime($defEndTime)) {
                                    //判断是否是Normal Conditions数据
                                    //Follow up、NA、NO这三种情况存在为非正常情况，即Checklist with Follow Up Actions数据
                                    if (!empty($systemLogInfo['task'])) {
                                        $task = $systemLogInfo['task'];
                                        $isFollowUp = false;
                                        foreach ($task as $taskVal) {
                                            if (!empty($taskVal['status'])) {
                                                if ($taskVal['status'] != 1 || !empty($taskVal['supervisor_remarks'])) {
                                                    $isFollowUp = true;
                                                    break;
                                                }
                                            } else {
                                                $isFollowUp = true;
                                                break;
                                            }
                                        }
                                    } else {
                                        $isFollowUp = true;
                                    }


                                    if ($isFollowUp == false) {
                                        $res[] = $systemLogInfo;
                                    }

                                }
                            }
                        }
                    }
                } else {
                    //验证systemId是否属于这些品牌
                    foreach ($needWork as $value) {
                        $systemInfo = $systemModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id'], 'system_type_id' => $type]);
                        if (!empty($systemInfo)) {
                            $systemLogInfo = $systemLogModel->getOne(['s_id' => $systemInfo['id']]);
                            if (!empty($systemLogInfo)) {
                                //将计划工作日期放进
                                $systemLogInfo['work_time'] = $value['work_time'];
                                //验证已经开始做的，并且supervisor角色是否已签名（即完成）//最新修改inspector签名即算完成
                                $defStartTime = date("Y-m-01 00:00:00", strtotime(date("Y-m-d", $value['work_time'])));
                                $defEndTime = date('Y-m-d 23:59:59', strtotime("$defStartTime +1 month -1 day"));
                                if ($systemLogInfo['status'] >= 1 && $systemLogInfo['create_time'] >= strtotime($defStartTime) && $systemLogInfo['create_time'] <= strtotime($defEndTime)) {
                                    //判断是否是Normal Conditions数据
                                    //Follow up、NA、NO这三种情况存在为非正常情况，即Checklist with Follow Up Actions数据
                                    if (!empty($systemLogInfo['task'])) {
                                        $task = $systemLogInfo['task'];
                                        $isFollowUp = false;
                                        foreach ($task as $taskVal) {
                                            if (!empty($taskVal['status'])) {
                                                if ($taskVal['status'] != 1 || !empty($taskVal['supervisor_remarks'])) {
                                                    $isFollowUp = true;
                                                    break;
                                                }
                                            } else {
                                                $isFollowUp = true;
                                                break;
                                            }
                                        }
                                    } else {
                                        $isFollowUp = true;
                                    }

                                    if ($isFollowUp == false) {
                                        $res[] = $systemLogInfo;
                                    }

                                }
                            }
                        }
                    }
                }
            } else {
                foreach ($needWork as $value) {
                    $systemInfo = $systemModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id']]);
                    if (!empty($systemInfo)) {
                        $systemLogInfo = $systemLogModel->getOne(['s_id' => $systemInfo['id']]);
                        if (!empty($systemLogInfo)) {
                            //将计划工作日期放进
                            $systemLogInfo['work_time'] = $value['work_time'];
                            //验证已经开始做的，并且supervisor角色是否已签名（即完成）//最新修改inspector签名即算完成
                            $defStartTime = date("Y-m-01 00:00:00", strtotime(date("Y-m-d", $value['work_time'])));
                            $defEndTime = date('Y-m-d 23:59:59', strtotime("$defStartTime +1 month -1 day"));
                            if ($systemLogInfo['status'] >= 1 && $systemLogInfo['create_time'] >= strtotime($defStartTime) && $systemLogInfo['create_time'] <= strtotime($defEndTime)) {
                                //判断是否是Normal Conditions数据
                                //Follow up、NA、NO这三种情况存在为非正常情况，即Checklist with Follow Up Actions数据
                                if (!empty($systemLogInfo['task'])) {
                                    $task = $systemLogInfo['task'];
                                    $isFollowUp = false;
                                    foreach ($task as $taskVal) {
                                        if (!empty($taskVal['status'])) {
                                            if ($taskVal['status'] != 1 || !empty($taskVal['supervisor_remarks'])) {
                                                $isFollowUp = true;
                                                break;
                                            }
                                        } else {
                                            $isFollowUp = true;
                                            break;
                                        }
                                    }
                                } else {
                                    $isFollowUp = true;
                                }

                                if ($isFollowUp == false) {
                                    $res[] = $systemLogInfo;
                                }

                            }
                        }
                    }
                }
            }
        }

        return $res;
    }

    /*
     * 获取Checklist with Follow Up Actions数据
     */
    public function getFollowUpActionsData($startTime, $endTime, $type = 0, $brand = 0){
        $needWork = $this->getMore([["work_time", ">=", $startTime], ["work_time", "<=", $endTime]]);

        $res = [];
        $systemModel = new System();
        $systemLogModel = new SystemLog();
        if (!empty($needWork)) {
            if (!empty($type)) {
                //类型、品牌都存在
                if (!empty($brand)) {
                    //验证systemId是否属于这些类型或品牌
                    foreach ($needWork as $value) {
                        $systemInfo = $systemModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id'], 'system_type_id' => $type, 'brand_type_id' => $brand]);
                        if (!empty($systemInfo)) {
                            $systemLogInfo = $systemLogModel->getOne(['s_id' => $systemInfo['id']]);
                            if (!empty($systemLogInfo)) {
                                //将计划工作日期放进
                                $systemLogInfo['work_time'] = $value['work_time'];
                                //验证已经开始做的，并且supervisor角色是否已签名（即完成）//最新修改inspector签名即算完成
                                $defStartTime = date("Y-m-01 00:00:00", strtotime(date("Y-m-d", $value['work_time'])));
                                $defEndTime = date('Y-m-d 23:59:59', strtotime("$defStartTime +1 month -1 day"));
                                if ($systemLogInfo['status'] >= 1 && $systemLogInfo['create_time'] >= strtotime($defStartTime) && $systemLogInfo['create_time'] <= strtotime($defEndTime)) {
                                    //判断是否是Normal Conditions数据
                                    //Follow up、NA、NO这三种情况存在为非正常情况，即Checklist with Follow Up Actions数据
                                    if (!empty($systemLogInfo['task'])) {
                                        $task = $systemLogInfo['task'];
                                        foreach ($task as $taskVal) {
                                            if (!empty($taskVal['status'])) {
                                                if ($taskVal['status'] != 1 || !empty($taskVal['supervisor_remarks'])) {
                                                    $res[] = $systemLogInfo;
                                                    break;
                                                }
                                            } else {
                                                $res[] = $systemLogInfo;
                                                break;
                                            }
                                        }
                                    } else {
                                        $res[] = $systemLogInfo;
                                    }
                                }
                            }
                        }
                    }
                } else {
                    //验证systemId是否属于这些品牌
                    foreach ($needWork as $value) {
                        $systemInfo = $systemModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id'], 'system_type_id' => $type]);
                        if (!empty($systemInfo)) {
                            $systemLogInfo = $systemLogModel->getOne(['s_id' => $systemInfo['id']]);
                            if (!empty($systemLogInfo)) {
                                //将计划工作日期放进
                                $systemLogInfo['work_time'] = $value['work_time'];
                                //验证已经开始做的，并且supervisor角色是否已签名（即完成）//最新修改inspector签名即算完成
                                $defStartTime = date("Y-m-01 00:00:00", strtotime(date("Y-m-d", $value['work_time'])));
                                $defEndTime = date('Y-m-d 23:59:59', strtotime("$defStartTime +1 month -1 day"));
                                if ($systemLogInfo['status'] >= 1 && $systemLogInfo['create_time'] >= strtotime($defStartTime) && $systemLogInfo['create_time'] <= strtotime($defEndTime)) {
                                    //判断是否是Normal Conditions数据
                                    //Follow up、NA、NO这三种情况存在为非正常情况，即Checklist with Follow Up Actions数据
                                    if (!empty($systemLogInfo['task'])) {
                                        $task = $systemLogInfo['task'];
                                        foreach ($task as $taskVal) {
                                            if (!empty($taskVal['status'])) {
                                                if ($taskVal['status'] != 1 || !empty($taskVal['supervisor_remarks'])) {
                                                    $res[] = $systemLogInfo;
                                                    break;
                                                }
                                            } else {
                                                $res[] = $systemLogInfo;
                                                break;
                                            }
                                        }
                                    } else {
                                        $res[] = $systemLogInfo;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                foreach ($needWork as $value) {
                    $systemInfo = $systemModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id']]);
                    if (!empty($systemInfo)) {
                        $systemLogInfo = $systemLogModel->getOne(['s_id' => $systemInfo['id']]);
                        if (!empty($systemLogInfo)) {
                            //将计划工作日期放进
                            $systemLogInfo['work_time'] = $value['work_time'];
                            //验证已经开始做的，并且supervisor角色是否已签名（即完成）//最新修改inspector签名即算完成
                            $defStartTime = date("Y-m-01 00:00:00", strtotime(date("Y-m-d", $value['work_time'])));
                            $defEndTime = date('Y-m-d 23:59:59', strtotime("$defStartTime +1 month -1 day"));
                            if ($systemLogInfo['status'] >= 1 && $systemLogInfo['create_time'] >= strtotime($defStartTime) && $systemLogInfo['create_time'] <= strtotime($defEndTime)) {
                                //判断是否是Normal Conditions数据
                                //Follow up、NA、NO这三种情况存在为非正常情况，即Checklist with Follow Up Actions数据
                                if (!empty($systemLogInfo['task'])) {
                                    $task = $systemLogInfo['task'];
                                    foreach ($task as $taskVal) {
                                        if (!empty($taskVal['status'])) {
                                            if ($taskVal['status'] != 1 || !empty($taskVal['supervisor_remarks'])) {
                                                $res[] = $systemLogInfo;
                                                break;
                                            }
                                        } else {
                                            $res[] = $systemLogInfo;
                                            break;
                                        }
                                    }
                                } else {
                                    $res[] = $systemLogInfo;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $res;
    }

    /*
     * 获取Checklist Pending Submission数据
     */
    public function getPendingSubmissionData($startTime, $endTime, $type = 0, $brand = 0){
        $needWork = $this->getMore([["work_time", ">=", $startTime], ["work_time", "<=", $endTime]]);

        $res = [];
        $systemModel = new System();
        $systemLogModel = new SystemLog();
        if (!empty($needWork)) {
            if (!empty($type)) {
                //类型、品牌都存在
                if (!empty($brand)) {
                    //验证systemId是否属于这些类型或品牌
                    foreach ($needWork as $value) {
                        $systemInfo = $systemModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id'], 'system_type_id' => $type, 'brand_type_id' => $brand]);
                        if (!empty($systemInfo)) {
                            $systemInfo['work_time'] = $value['work_time'];
                            $systemLogInfo = $systemLogModel->getOne(['s_id' => $systemInfo['id']]);
                            if (!empty($systemLogInfo)) {
                                //将计划工作日期放进
                                $systemLogInfo['work_time'] = $value['work_time'];
                                //验证已经开始做的，并且supervisor角色是否已签名（即完成）
                                if ($systemLogInfo['status'] < 2 && empty($systemLogInfo['sign_time'])) {
                                    $systemInfo['systemLogData'] = $systemLogInfo;
                                    $res[] = $systemInfo;
                                }
                            } else {
                                $systemInfo['systemLogData'] = [];
                                $res[] = $systemInfo;
                            }
                        }
                    }
                } else {
                    //验证systemId是否属于这些品牌
                    foreach ($needWork as $value) {
                        $systemInfo = $systemModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id'], 'system_type_id' => $type]);
                        if (!empty($systemInfo)) {
                            $systemInfo['work_time'] = $value['work_time'];
                            $systemLogInfo = $systemLogModel->getOne(['s_id' => $systemInfo['id']]);
                            if (!empty($systemLogInfo)) {
                                //将计划工作日期放进
                                $systemLogInfo['work_time'] = $value['work_time'];
                                //验证已经开始做的，并且supervisor角色是否已签名（即完成）
                                if ($systemLogInfo['status'] < 2 && empty($systemLogInfo['sign_time'])) {
                                    $systemInfo['systemLogData'] = $systemLogInfo;
                                    $res[] = $systemInfo;
                                }
                            } else {
                                $systemInfo['systemLogData'] = [];
                                $res[] = $systemInfo;
                            }
                        }
                    }
                }
            } else {
                foreach ($needWork as $value) {
                    $systemInfo = $systemModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id']]);
                    if (!empty($systemInfo)) {
                        $systemInfo['work_time'] = $value['work_time'];
                        $systemLogInfo = $systemLogModel->getOne(['s_id' => $systemInfo['id']]);
                        if (!empty($systemLogInfo)) {
                            //将计划工作日期放进
                            $systemLogInfo['work_time'] = $value['work_time'];
                            //验证已经开始做的，并且supervisor角色是否已签名（即完成）
                            if ($systemLogInfo['status'] < 2 && empty($systemLogInfo['sign_time'])) {
                                $systemInfo['systemLogData'] = $systemLogInfo;
                                $res[] = $systemInfo;
                            }
                        } else {
                            $systemInfo['systemLogData'] = [];
                            $res[] = $systemInfo;
                        }
                    }
                }
            }
        }

        return $res;
    }

    public function truncateCrontab(){
        self::truncate();

        return true;
    }
}
