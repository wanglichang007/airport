<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'task';

    /**
     * @var string
     */
    protected $primaryKey = 'task_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * 获取一条记录
     *
     */
    public function getOne($params=[]){
        if(isset($params[$this->primaryKey])){
            $data = self::find($params[$this->primaryKey]);
        }else{
            $data = self::where($params)->first();
        }

        if (!empty($data)) {
            return $data->toArray();
        }
        return [];
    }

    //创建
    public function createOne($data) {
        $res = self::create($data);
        return $res->task_id;
    }

    /**
     * 修改一条记录
     *
     */
    public function updateOne($primaryKey,$params=[]){
        $id = (int)$primaryKey;
        $flag = self::where($this->primaryKey, $id)->update($params);
        return $flag;
    }
}
