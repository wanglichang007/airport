<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use Codedge\Fpdf\Fpdf\Fpdf;

class Base extends Model
{
    use HasFactory;

    /*
     * 下载html结构的excel文档
     */
    public function downloadExcel($html, $fileName = ''){
        if (!empty($html)) {
            $pathName = $this->saveExcelFile($html, $fileName);

            if (empty($fileName)) {
                $tim = time();
                $fileName = $tim.rand(100, 999);
            }

            header('Content-Description: File Transfer');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename='.$fileName.'.xlsx');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize(public_path($pathName)));
            readfile(public_path($pathName));
            exit;
        }
    }

    /*
     * 发送邮件
     * $recipient 收件人
     * $attachments 附件
     * $content 内容
     * $addCC 抄送
     */
    public function sendEmail($recipient, $addCC, $attachments, $content, $subject){
        $status = [
            'status' => 0,
            'message' => 'Send Success!'
        ];
        $mail = new PHPMailer(true);

        try {
            $mail->SMTPDebug = SMTP::DEBUG_OFF;                      //Enable verbose debug output
            $mail->isSMTP();                                            //Send using SMTP
            $mail->Host       = 'smtp.qq.com';                     //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
            $mail->Username   = '2408201483@qq.com';                     //SMTP username
            $mail->Password   = 'skvrsoaskqfkdifi';                               //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
            $mail->Port       = 465;

            //Recipients
            $mail->setFrom('2408201483@qq.com');
            $mail->addAddress($recipient);     //Add a recipient
            //$mail->addAddress('ellen@example.com');               //Name is optional
            //$mail->addReplyTo('info@example.com', 'Information');
            if (!empty($addCC)) {
                $mail->addCC($addCC);
            }
            //$mail->addBCC('bcc@example.com');

            //Attachments
            if (!empty($attachments)) {
                $mail->addAttachment($attachments);         //Add attachments
            }
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

            //Content
            $mail->isHTML(true);                                  //Set email format to HTML
            if (!empty($content)) {
                $mail->Subject = $subject;
                $mail->Body    = $content;
                //$mail->AltBody = !empty($content['AltBody']) ? $content['AltBody'] : '';
            } else {
                $mail->Body    = 'Please!';
            }

            $mail->send();
        } catch (Exception $e) {
            $status = [
                'status' => 1,
                'message' => $mail->ErrorInfo
            ];
        }

        return $status;
    }

    /*
     * 生成html结构的Excel文件
     */
    public function saveExcelFile($html, $fileName = null) {
        if (!empty($html)) {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Html();
            $spreadsheet = $reader->loadFromString($html);
            $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->setIncludeCharts(true);
            $tim = time();
            $path = date('Y', $tim).'/'.date('m', $tim).'/'.date('d', $tim).'/';
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            }
            if (!empty($fileName)) {
                $pathName = $path.$fileName.'.xlsx';
            } else {
                $pathName = $path.$tim.rand(100, 999).'.xlsx';
            }
            $writer->save($pathName);

            return $pathName;
        }
    }

    /*
     *
     */
    public function downloadPdf($systemLogId){
        $systemLogModel = new SystemLog();
        $systemLogTaskImageModel = new SystemLogTaskImage();
        $taskModel = new Task();

        $taskImage = $systemLogTaskImageModel->getMore(['system_log_id' => $systemLogId], 0, 999, 'asc', 'task_id');
        $premitReportImage = $systemLogModel->getOne(['system_log_id' => $systemLogId]);
        $pdfModel = new Fpdf;

        $output = false;
        if (!empty($taskImage)) {
            $output = true;
            foreach ($taskImage as $value) {
                $taskInfo = $taskModel->getOne(['task_id' => $value['task_id']]);
                if (!empty($taskInfo)) {
                    $userType = '';
                    if ($value['type'] == 1) {
                        $userType = 'Inspector';
                    }

                    if ($value['type'] == 2) {
                        $userType = 'Supervisor';
                    }

                    $title = 'ID: '.$taskInfo['task_id'].', Clause Ref: '.$taskInfo['clause_no'].', User Type: '.$userType;
                    $pdfModel->AddPage(); //增加一页
                    $pdfModel->SetFont('Courier','',20); //设置字体样式
                    $pdfModel->Cell(0,0, $title);
                    $pdfModel->Image(public_path($value['image']),20,20,0,0); //增加一张图片，文件名为 sight.jpg
                }
            }
        }

        if (!empty($premitReportImage)) {
            if (!empty($premitReportImage['license_image'])) {
                $output = true;
                $pdfModel->AddPage(); //增加一页
                $pdfModel->SetFont('Courier','I',20); //设置字体样式
                $pdfModel->Cell(0,0,'Use Permit');
                $pdfModel->Image(public_path($premitReportImage['license_image']),20,20,0,0); //增加一张图片，文件名为 sight.jpg
            }
            if (!empty($premitReportImage['incident_image'])) {
                $output = true;
                $pdfModel->AddPage(); //增加一页
                $pdfModel->SetFont('Courier','I',20); //设置字体样式
                $pdfModel->Cell(0,0,'Incident Report');
                $pdfModel->Image(public_path($premitReportImage['incident_image']),20,20,0,0); //增加一张图片，文件名为 sight.jpg
            }
        }

        if (!empty($output)) {
            $tim = time();
            $filePath = storage_path('app/image/'.date('Y', $tim).'/'.date('m', $tim).'/'.date('d', $tim).'/');
            if (!is_dir($filePath)) {
                mkdir($filePath, 0755);
            }
            $fileName = $filePath.$premitReportImage['system_id'].'-'.$premitReportImage['from_id'].'-'.date("Y", $tim).'-'.date("m", $tim).'-'.date('d', $tim).'.pdf';
            $pdfModel->Output('F', $fileName);

            return $fileName;
        }

        return null;
    }
}
