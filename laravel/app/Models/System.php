<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'system';

    /**
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * 获取一条记录
     *
     */
    public function getOne($params=[]){
        if(isset($params[$this->primaryKey])){
            $data = self::find($params[$this->primaryKey]);
        }else{
            $data = self::where($params)->first();
        }

        if (!empty($data)) {
            return $data->toArray();
        }
        return [];
    }

    /**
     * 获取多条记录
     *
     */
    public function getMore($params=[],$offset=0,$limit=100){
        $res = self::where($params)->orderBy('id', 'asc')->offset($offset)->limit($limit)->get()->toArray();
        return $res;
    }

    /**
     * 修改一条记录
     *
     */
    public function updateOne($primaryKey,$params=[]){
        $id = (int)$primaryKey;
        $flag = self::where($this->primaryKey, $id)->update($params);
        return $flag;
    }

    /*
     * 根据systemId fromId获取必填的task
     */
    public function getTaskBySystemIdFromId($systemId, $fromId){
        $systemInfo = $this->getOne(['system_id' => $systemId, 'from_id' => $fromId]);

        if (!empty($systemInfo['task'])) {
            $task = json_decode($systemInfo['task'], true);

            $taskModel = new Task();
            $taskInfos = [];
            foreach ($task as $value) {
                $taskInfo = $taskModel->getOne(['task_id' => $value]);
                $taskInfos[] = $taskInfo;
            }

            return $taskInfos;
        }

        return [];
    }


    //创建
    public function createOne($data) {
        $res = self::create($data);
        return $res->id;
    }

    //获取列表
    public function getList($params = []) {
        $where = !empty($params['where']) ? $params['where'] : [];
        $pageSize = !empty($params['pageSize']) ? $params['pageSize'] : 20;
        return self::where($where)->orderBy($this->primaryKey, 'desc')->paginate($pageSize);
    }
}
