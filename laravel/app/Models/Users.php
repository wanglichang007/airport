<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @var string
     */
    protected $primaryKey = 'users_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * 获取一条记录
     *
     */
    public function getOne($params=[]){
        if(isset($params[$this->primaryKey])){
            $data = self::find($params[$this->primaryKey]);
        }else{
            $data = self::where($params)->first();
        }

        if (!empty($data)) {
            return $data->toArray();
        }
        return [];
    }

    //创建
    public function createOne($data) {
        $res = self::create($data);
        return $res->users_id;
    }

    /**
     * 修改一条记录
     *
     */
    public function updateOne($primaryKey,$params=[]){
        $id = (int)$primaryKey;
        $flag = self::where($this->primaryKey, $id)->update($params);
        return $flag;
    }

    /**
     * 获取多条记录
     *
     */
    public function getMore($params=[],$offset=0,$limit=20,$orderBy='asc'){
        $res = self::where($params)->orderBy($this->primaryKey, $orderBy)->offset($offset)->limit($limit)->get()->toArray();
        return $res;
    }

    //获取列表
    public function getList($params = []) {
        $where = !empty($params['where']) ? $params['where'] : [];
        $pageSize = !empty($params['pageSize']) ? $params['pageSize'] : 20;
        return self::where($where)->orderBy($this->primaryKey, 'desc')->paginate($pageSize);
    }

    public function deleteAll($where){
        return self::where($where)->delete();
    }
}
