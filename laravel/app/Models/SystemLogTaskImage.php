<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SystemLogTaskImage extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'system_log_task_image';

    /**
     * @var string
     */
    protected $primaryKey = 'system_log_task_image_id';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * 获取一条记录
     *
     */
    public function getOne($params=[]){
        if(isset($params[$this->primaryKey])){
            $data = self::find($params[$this->primaryKey]);
        }else{
            $data = self::where($params)->first();
        }

        if (!empty($data)) {
            return $data->toArray();
        }
        return [];
    }

    //创建
    public function createOne($data) {
        $res = self::create($data);
        return $res->system_log_task_image_id;
    }

    /**
     * 获取多条记录
     *
     */
    public function getMore($params=[],$offset=0,$limit=20,$orderBy='asc', $orderName = ''){
        if (empty($orderName)) {
            $orderName = $this->primaryKey;
        }
        $res = self::where($params)->orderBy($orderName, $orderBy)->offset($offset)->limit($limit)->get()->toArray();
        if (!empty($res)) {
            foreach($res as $key=>$value) {
                $res[$key]['view_image'] = env("IMAGE_URL").$value['image'];
            }
        }
        return $res;
    }

    //获取列表
    public function getList($params = []) {
        $where = !empty($params['where']) ? $params['where'] : [];
        $pageSize = !empty($params['pageSize']) ? $params['pageSize'] : 20;
        return self::where($where)->orderBy($this->primaryKey, 'desc')->paginate($pageSize);
    }

    //删除
    public function deleteAll($where){
        return self::where($where)->delete();
    }
}
