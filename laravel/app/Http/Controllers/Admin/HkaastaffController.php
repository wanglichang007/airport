<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Upload;
use App\Http\Controllers\Controller;
use App\Models\Base;
use App\Models\Brand;
use App\Models\Equipment;
use App\Models\System;
use App\Models\SystemCrontab;
use App\Models\SystemLog;
use App\Models\SystemLogTaskImage;
use App\Models\Task;
use Illuminate\Http\Request;
use ZipArchive;
error_reporting(E_ALL);
ini_set('display_errors','1');

class HkaastaffController extends BaseUserController
{
    //
    public function index(Equipment $equipmentModel, Brand $brandModel){
        $equipmentInfo = $equipmentModel->getMore();
        $brandInfo = $brandModel->getMore();

        return view('admin.hkaastaff', ['equipmentInfo' => $equipmentInfo, 'brandInfo' => $brandInfo]);
    }

    public function pendingReview(SystemLog $systemLogModel){
        $pendingReview = $systemLogModel->getList(['where' => ['status' => 1]]);
        return view('admin.hkaastaff_pendingReview', ['data' => $pendingReview, 'where' => []]);
    }

    public function individual(){
        return view('admin.hkaastaff_individual');
    }

    public function task(Request $request, System $systemModel, SystemLog $systemLogModel, Task $taskModel, SystemLogTaskImage $systemLogTaskImageModel){
        if($request->isMethod('post')) {
            $hkaaRemarks = $request->input('hkaa_remarks');
            $systemLogId = $request->input('system_log_id');

            if (empty($systemLogId)) {
                return $this->msg('systemLogId Not allowed to be empty!');
            }

            $systemLogModel->updateOne($systemLogId, ['hkaa_remarks' => nl2br($hkaaRemarks)]);

            return redirect()->route("admin.hkaastaff.confirm", ['system_log_id' => $systemLogId]);
        } else {
            $systemLogId = $request->input('system_log_id');

            $systemLogInfo = $systemLogModel->getOne(['system_log_id' => $systemLogId]);
            if (empty($systemLogInfo)) {
                return $this->msg("The system id does not exist Or under submit!");
            }

            if (!empty($systemLogInfo['hkaa_remarks'])) {
                $systemLogInfo['hkaa_remarks'] = $this->br2nl($systemLogInfo['hkaa_remarks']);
            }

            //组装task必填项
            $taskInfo = $systemModel->getTaskBySystemIdFromId($systemLogInfo['system_id'], $systemLogInfo['from_id']);
            //查询当前systemLogId下的当前task是否存储了图片
            foreach ($taskInfo as $key=> $value) {
                $taskImage = $systemLogTaskImageModel->getOne(['system_log_id' => $systemLogInfo['system_log_id'], 'task_id' => $value['task_id'], 'type' => 1]);
                if (!empty($taskImage)) {
                    $taskInfo[$key]['inspector_upload_image'] = true;
                } else {
                    $taskInfo[$key]['inspector_upload_image'] = false;
                }

                $sTaskImage = $systemLogTaskImageModel->getOne(['system_log_id' => $systemLogInfo['system_log_id'], 'task_id' => $value['task_id'], 'type' => 2]);
                if (!empty($sTaskImage)) {
                    $taskInfo[$key]['supervisor_upload_image'] = true;
                } else {
                    $taskInfo[$key]['supervisor_upload_image'] = false;
                }
            }

            $systemInfo = $systemModel->getOne(['system_id' => $systemLogInfo['system_id'], 'from_id' => $systemLogInfo['from_id']]);

            return view('admin.hkaastaff_task', ['systemInfo' => $systemInfo, 'systemLogInfo' => $systemLogInfo, 'taskInfo' => $taskInfo]);
        }
    }

    private function br2nl($text) {
        return preg_replace('/<br\\s*?\/??>/i', '', $text);
    }

    public function confirm(Request $request, SystemLog $systemLogModel, System $systemModel, Upload $upload, SystemLogTaskImage $systemLogTaskImageModel){
        $systemLogId = $request->input('system_log_id');
        if (empty($systemLogId)) {
            return $this->msg("SystemLogId Not Null!");
        }

        $systemLogInfo = $systemLogModel->getOne(['system_log_id' => $systemLogId]);
        if (empty($systemLogInfo)) {
            return $this->msg("The system id does not exist Or under submit!");
        }

        if (!empty($systemLogInfo['hkaa_remarks'])) {
            $systemLogInfo['hkaa_remarks'] = $this->br2nl($systemLogInfo['hkaa_remarks']);
        }

        //组装task必填项
        $taskInfo = $systemModel->getTaskBySystemIdFromId($systemLogInfo['system_id'], $systemLogInfo['from_id']);
        //查询当前systemLogId下的当前task是否存储了图片
        foreach ($taskInfo as $key=> $value) {
            $taskImage = $systemLogTaskImageModel->getOne(['system_log_id' => $systemLogInfo['system_log_id'], 'task_id' => $value['task_id'], 'type' => 1]);
            if (!empty($taskImage)) {
                $taskInfo[$key]['inspector_upload_image'] = true;
            } else {
                $taskInfo[$key]['inspector_upload_image'] = false;
            }

            $sTaskImage = $systemLogTaskImageModel->getOne(['system_log_id' => $systemLogInfo['system_log_id'], 'task_id' => $value['task_id'], 'type' => 2]);
            if (!empty($sTaskImage)) {
                $taskInfo[$key]['supervisor_upload_image'] = true;
            } else {
                $taskInfo[$key]['supervisor_upload_image'] = false;
            }
        }

        $systemInfo = $systemModel->getOne(['id' => $systemLogInfo['s_id']]);

        return view('admin.hkaastaff_task_confirm', ['systemLogInfo' => $systemLogInfo, 'taskInfo' => $taskInfo, 'systemInfo' => $systemInfo]);
    }

    public function summary(Request $request, SystemCrontab $systemCrontabModel, Equipment $equipmentModel, Brand $brandModel){
        $defStartTime = date("Y-m-01", strtotime(date("Y-m-d")));
        $defEndTime = date('Y-m-d', strtotime("$defStartTime +1 month -1 day"));
        $startTime = $request->input('start_time', $defStartTime);
        $endTime = $request->input('end_time', $defEndTime);
        $type = $request->input('type', 0);
        $brand = $request->input('brand', 0);

        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime.' 23:59:59');

        $info = $systemCrontabModel->getChartsByTime($startTime, $endTime, $type, $brand);
        if (!empty($info)) {
            $normalConditionsRatio = round($info['normalConditionsNumber'] / $info['totalNumber'] * 100, 1);
            $followUpActionsRatio = round($info['followUpActionsNumber'] / $info['totalNumber'] * 100, 1);
            $ratio = [
                'totalRatio' => 100,
                'normalConditionsRatio' => $normalConditionsRatio,
                'followUpActionsRatio' => $followUpActionsRatio,
                'pendingSubmissionRatio' => 100 - $normalConditionsRatio - $followUpActionsRatio
            ];

            $infoCharts = [
                [
                    'name' => '2、 Normal Conditions',
                    'y' => $info['normalConditionsNumber']
                ],
                [
                    'name' => '3、 Checklist with Follow Up Actions',
                    'y' => $info['followUpActionsNumber']
                ],
                [
                    'name' => '4、 Checklist Pending Submission',
                    'y' => $info['pendingSubmissionNumber']
                ]
            ];

            $ratioCharts = [
                [
                    'name' => '2、 Normal Conditions Ratio',
                    'y' => $ratio['normalConditionsRatio']
                ],
                [
                    'name' => '3、 Checklist with Follow Up Actions Ratio',
                    'y' => $ratio['followUpActionsRatio']
                ],
                [
                    'name' => '4、 Checklist Pending Submission Ratio',
                    'y' => $ratio['pendingSubmissionRatio']
                ]
            ];

        } else {
            $ratio = [
                'totalRatio' => 0,
                'normalConditionsRatio' => 0,
                'followUpActionsRatio' => 0,
                'pendingSubmissionRatio' => 0
            ];

            $infoCharts = [
                [
                    'name' => '2、 Normal Conditions',
                    'y' => 0
                ],
                [
                    'name' => '3、 Checklist with Follow Up Actions',
                    'y' => 0
                ],
                [
                    'name' => '4、 Checklist Pending Submission',
                    'y' => 0
                ]
            ];

            $ratioCharts = [
                [
                    'name' => '2、 Normal Conditions Ratio',
                    'y' => 0
                ],
                [
                    'name' => '3、 Checklist with Follow Up Actions Ratio',
                    'y' => 0
                ],
                [
                    'name' => '4、 Checklist Pending Submission Ratio',
                    'y' => 0
                ]
            ];
        }

        $infoChartsJson = json_encode($infoCharts, true);
        $ratioChartsJson = json_encode($ratioCharts, true);

        $typeInfo = [];
        $brandInfo = [];
        if (!empty($type)) {
            $typeInfo = $equipmentModel->getOne(['equipment_id' => $type]);
        }
        if (!empty($brand)) {
            $brandInfo = $brandModel->getOne(['brand_id' => $brand]);
        }

        return view("admin.hkaastaff_summary", ['info' => $info,
            'ratio' => $ratio,
            'infoChartsJson' => $infoChartsJson,
            'ratioChartsJson' => $ratioChartsJson,
            'startTime' => $startTime,
            'endTime' => $endTime,
            'type' => $type,
            'brand' => $brand,
            'typeInfo' => $typeInfo,
            'brandInfo' => $brandInfo
        ]);
    }

    public function download(Request $request, SystemLog $systemLogModel, Base $baseModel){
        $systemLogId = $request->input('system_log_id');
        $systemInfo = $systemLogModel->getOne(['system_log_id' => $systemLogId]);

        $data = $systemLogModel->insectorSupervisorBlade($systemLogId, 'hkaa');

        $tim = time();
        $fileName = $systemInfo['system_id'].'-'.$systemInfo['from_id'].'-'.date("Y", $tim).'-'.date('m', $tim).'-'.date('d', $tim);
        $excelFilePath = $baseModel->saveExcelFile($data, $fileName);

        $pdfFilePath = $baseModel->downloadPdf($systemLogId);

        $zipFilePath = storage_path('app/image/'.date('Y', $tim).'/'.date('m', $tim).'/'.date('d', $tim).'/');
        if (!is_dir($zipFilePath)) {
            mkdir($zipFilePath, 0755);
        }
        $zipFilename = $zipFilePath.$fileName.'.zip';
        $zip = new ZipArchive;
        if ($zip->open($zipFilename, ZIPARCHIVE::CREATE)!==TRUE) {
            return $this->msg('zip faild!');
        }

        if (!empty($excelFilePath)) {
            $zip->addFile($excelFilePath, $fileName.'.xlsx');
        }
        if (!empty($pdfFilePath)) {
            $zip->addFile($pdfFilePath, $fileName.'.pdf');
        }

        $zip->close();

        return response()->download($zipFilename);
    }

    public function downloadSummary(Request $request, SystemLog $systemLogModel, Base $baseModel){
        $startTime = $request->input('start_time');
        $endTime = $request->input('end_time');
        $type = $request->input('type');
        $brand = $request->input('brand');

        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime.' 23:59:59');

        $data = $systemLogModel->spervisorSummaryBlade($startTime, $endTime, $type, $brand);

        $tim = time();
        $fileName = 'Summary-'.date("Y", $tim).'-'.date("m", $tim).'-'.date("d", $tim);
        $baseModel->downloadExcel($data, $fileName);
    }

    public function send(Request $request, Base $baseModel, SystemLog $systemLogModel){
        if ($request->isMethod('post')) {
            $systemLogId = $request->input('system_log_id');
            $recipient = $request->input('recipient');
            $addCC = $request->input('addCC');
            $content = $request->input('content');
            $subject = $request->input('subject');

            if (empty($systemLogId) || empty($recipient)) {
                return $this->msg('operation failed!');
            }

            $html = $systemLogModel->insectorSupervisorBlade($systemLogId, 'hkaa');

            $filePath = $baseModel->saveExcelFile($html);

            $res = $baseModel->sendEmail($recipient, $addCC, $filePath, $content, $subject);
            if ($res['status'] == 0) {
                return $this->msg('Send Success!', 'admin.supervisor.confirm', ['system_log_id' => $systemLogId]);
            } else {
                return $this->msg('Send Faild!');
            }
        } else {
            $systemLogId = $request->input('system_log_id');
            if (empty($systemLogId)) {
                return $this->msg('operation failed!');
            }

            return view('admin.hkaastaff_send_email', ['systemLogId' => $systemLogId]);
        }
    }

    public function sendSummary(Request $request, SystemLog $systemLogModel, Base $baseModel){
        if ($request->isMethod('post')) {
            $startTime = $request->input('start_time');
            $endTime = $request->input('end_time');
            $type = $request->input('type');
            $brand = $request->input('brand');
            $recipient = $request->input('recipient');
            $addCC = $request->input('addCC');
            $content = $request->input('content');
            $subject = $request->input('subject');

            $html = $systemLogModel->spervisorSummaryBlade($startTime, $endTime, $type, $brand);
            $filePath = $baseModel->saveExcelFile($html);

            $res = $baseModel->sendEmail($recipient, $addCC, $filePath, $content, $subject);
            if ($res['status'] == 0) {
                return $this->msg('Send Success!', 'admin.supervisor.summary', ['startTime' => $startTime, 'endTime' => $endTime, 'type' => $type, 'brand' => $brand]);
            } else {
                return $this->msg('Send Faild!');
            }
        } else {
            $startTime = $request->input('start_time');
            $endTime = $request->input('end_time');
            $type = $request->input('type');
            $brand = $request->input('brand');

            $startTime = strtotime($startTime);
            $endTime = strtotime($endTime.' 23:59:59');

            return view('admin.hkaastaff_send_summary', ['startTime' => $startTime, 'endTime' => $endTime, 'type' => $type, 'brand' => $brand]);
        }
    }

    public function sign(Request $request, SystemLog $systemLogModel){
//        if ($request->isMethod('post')) {
//            $systemLogId = $request->input('system_log_id');
//            if (empty($systemLogId)) {
//                return $this->msg('operation failed!');
//            }
//
//            $sign = $request->input('sign');
//            if (empty($sign)) {
//                return $this->msg('sign, please!');
//            }
//
//            list($type, $sign) = explode(';', $sign);
//            list(, $sign)      = explode(',', $sign);
//            $data = base64_decode($sign);
//
//            $signName = $systemLogId.'_'.time().'image.png';
//            $datePath = date('Y/m/d', time());
//            $filePath = storage_path("app/image/".$datePath);
//            \Illuminate\Support\Facades\File::isDirectory($filePath) or \Illuminate\Support\Facades\File::makeDirectory($filePath, 0777, true, true);
//
//            file_put_contents($filePath.'/'.$signName, $data);
//
//            $systemLogModel->updateOne($systemLogId, ['hkaa_sign' => '/storage/'.$datePath.'/'.$signName, 'status' => 3]);
//
//            return redirect()->route("admin.hkaastaff");
//        } else {
//            $systemLogId = $request->input('system_log_id');
//            if (empty($systemLogId)) {
//                return $this->msg('operation failed!');
//            }
//
//            $systemLogInfo = $systemLogModel->getOne(['system_log_id' => $systemLogId]);
//
//            return view('admin.hkaastaff_sign', ['systemLogInfo' => $systemLogInfo]);
//        }
    }

    public function image(Request $request, SystemLogTaskImage $systemLogTaskImageModel, SystemLog $systemLogModel){
        if ($request->isMethod('post')) {
            $systemLogId = $request->input('system_log_id');
            $taskId = $request->input('task_id');
            $type = $request->input('type');
            $images = $request->input('images');

            if (empty($systemLogId) || empty($taskId) || empty($type)) {
                return $this->msg('operation failed!');
            }

            //删除原图
            $systemLogTaskImageModel->deleteAll([
                'system_log_id' => $systemLogId,
                'task_id' => $taskId,
                'type' => $type
            ]);

            //添加新图
            if (!empty($images)) {
                foreach($images as $image) {
                    $systemLogTaskImageModel->createOne([
                        'system_log_id' => $systemLogId,
                        'task_id' => $taskId,
                        'type' => $type,
                        'image' => $image
                    ]);
                }
            }

            $systemLogInfo = $systemLogModel->getOne(['system_log_id' => $systemLogId]);

            return redirect()->route('admin.supervisor.task', ['systemId' => $systemLogInfo['system_id'], 'from_id' => $systemLogInfo['from_id']]);
        } else {
            $systemLogId = $request->input('system_log_id');
            $taskId = $request->input('task_id');
            $type = $request->input('type');

            if (empty($systemLogId)) {
                return $this->msg('operation failed!');
            }

            $images = $systemLogTaskImageModel->getMore(['system_log_id' => $systemLogId, 'task_id' => $taskId, 'type' => $type]);

            return view('admin.hkaastaff_image', ['images' => $images, 'systemLogId' => $systemLogId, 'taskId' => $taskId, 'type' => $type]);
        }
    }

    public function totalNumber(Request $request, SystemCrontab $systemCrontabModel, Equipment $equipmentModel, Brand $brandModel){
        $startTime = $request->input('start_time');
        $endTime = $request->input('end_time');
        $type = $request->input('type');
        $brand = $request->input('brand');
//        $sortKey = $request->input('sort_key', 'system_log_id');
//        $sort = $request->input('sort', 'desc');

        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime.' 23:59:59');

        $data = $systemCrontabModel->getTotalData($startTime, $endTime, $type, $brand);

        $typeInfo = [];
        $brandInfo = [];
        if (!empty($type)) {
            $typeInfo = $equipmentModel->getOne(['equipment_id' => $type]);
        }
        if (!empty($brand)) {
            $brandInfo = $brandModel->getOne(['brand_id' => $brand]);
        }

//        if ($sort == 'asc') {
//            array_multisort(array_column($data['systemLogInfo'],$sortKey),SORT_ASC,$data['systemLogInfo']);
//        } else {
//            array_multisort(array_column($data['systemLogInfo'],$sortKey),SORT_DESC,$data['systemLogInfo']);
//        }
        return view('admin.hkaastaff_totalNumber', ['data' => $data, 'startTime' => $startTime, 'endTime' => $endTime, 'type' => $type, 'brand' => $brand, 'typeInfo' => $typeInfo, 'brandInfo' => $brandInfo]);

//        return view('admin.spervisor_totalNumber', ['data' => $data, 'startTime' => $startTime, 'endTime' => $endTime, 'type' => $type, 'brand' => $brand, 'sort_key' => $sortKey,  'sort' => $sort]);
    }

    public function normalConditions(Request $request, SystemCrontab $systemCrontabModel, Equipment $equipmentModel, Brand $brandModel){
        $startTime = $request->input('start_time');
        $endTime = $request->input('end_time');
        $type = $request->input('type');
        $brand = $request->input('brand');
//        $sortKey = $request->input('sort_key', 'system_log_id');
//        $sort = $request->input('sort', 'desc');

        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime.' 23:59:59');

        $data = $systemCrontabModel->getNormalConditionsData($startTime, $endTime, $type, $brand);

        $typeInfo = [];
        $brandInfo = [];
        if (!empty($type)) {
            $typeInfo = $equipmentModel->getOne(['equipment_id' => $type]);
        }
        if (!empty($brand)) {
            $brandInfo = $brandModel->getOne(['brand_id' => $brand]);
        }

//        if ($sort == 'asc') {
//            array_multisort(array_column($data,$sortKey),SORT_ASC,$data);
//        } else {
//            array_multisort(array_column($data,$sortKey),SORT_DESC,$data);
//        }

        return view('admin.hkaastaff_normalConditions', ['data' => $data, 'startTime' => $startTime, 'endTime' => $endTime, 'type' => $type, 'brand' => $brand, 'typeInfo' => $typeInfo, 'brandInfo' => $brandInfo]);

//        return view('admin.spervisor_normalConditions', ['data' => $data, 'startTime' => $startTime, 'endTime' => $endTime, 'type' => $type, 'brand' => $brand, 'sort_key' => $sortKey,  'sort' => $sort]);
    }

    public function followUpActions(Request $request, SystemCrontab $systemCrontabModel, Equipment $equipmentModel, Brand $brandModel){
        $startTime = $request->input('start_time');
        $endTime = $request->input('end_time');
        $type = $request->input('type');
        $brand = $request->input('brand');
//        $sortKey = $request->input('sort_key', 'system_log_id');
//        $sort = $request->input('sort', 'desc');

        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime.' 23:59:59');

        $data = $systemCrontabModel->getFollowUpActionsData($startTime, $endTime, $type, $brand);

        $typeInfo = [];
        $brandInfo = [];
        if (!empty($type)) {
            $typeInfo = $equipmentModel->getOne(['equipment_id' => $type]);
        }
        if (!empty($brand)) {
            $brandInfo = $brandModel->getOne(['brand_id' => $brand]);
        }

//        if ($sort == 'asc') {
//            array_multisort(array_column($data,$sortKey),SORT_ASC,$data);
//        } else {
//            array_multisort(array_column($data,$sortKey),SORT_DESC,$data);
//        }

        return view('admin.hkaastaff_followUpActions', ['data' => $data, 'startTime' => $startTime, 'endTime' => $endTime, 'type' => $type, 'brand' => $brand, 'typeInfo' => $typeInfo, 'brandInfo' => $brandInfo]);

//        return view('admin.spervisor_followUpActions', ['data' => $data, 'startTime' => $startTime, 'endTime' => $endTime, 'type' => $type, 'brand' => $brand, 'sort_key' => $sortKey,  'sort' => $sort]);
    }

    public function pendingSubmission(Request $request, SystemCrontab $systemCrontabModel, Equipment $equipmentModel, Brand $brandModel){
        $startTime = $request->input('start_time');
        $endTime = $request->input('end_time');
        $type = $request->input('type');
        $brand = $request->input('brand');
//        $sortKey = $request->input('sort_key', 'system_log_id');
//        $sort = $request->input('sort', 'desc');

        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime.' 23:59:59');

        $data = $systemCrontabModel->getPendingSubmissionData($startTime, $endTime, $type, $brand);

        $typeInfo = [];
        $brandInfo = [];
        if (!empty($type)) {
            $typeInfo = $equipmentModel->getOne(['equipment_id' => $type]);
        }
        if (!empty($brand)) {
            $brandInfo = $brandModel->getOne(['brand_id' => $brand]);
        }

//        if ($sort == 'asc') {
//            array_multisort(array_column($data,$sortKey),SORT_ASC,$data);
//        } else {
//            array_multisort(array_column($data,$sortKey),SORT_DESC,$data);
//        }
        return view('admin.hkaastaff_pendingSubmission', ['data' => $data, 'startTime' => $startTime, 'endTime' => $endTime, 'type' => $type, 'brand' => $brand, 'typeInfo' => $typeInfo, 'brandInfo' => $brandInfo]);

//        return view('admin.spervisor_pendingSubmission', ['data' => $data, 'startTime' => $startTime, 'endTime' => $endTime, 'type' => $type, 'brand' => $brand, 'sort_key' => $sortKey,  'sort' => $sort]);
    }

    public function menuDownload(Request $request, SystemLog $systemLogModel, Base $baseModel){
        $startTime = $request->input('start_time');
        $endTime = $request->input('end_time');
        $type = $request->input('type');
        $brand = $request->input('brand');
        $menu = $request->input('menu');

        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime.' 23:59:59');

        $html = $systemLogModel->menuListBlade($menu, $startTime, $endTime, $type, $brand);

        $fileName = '';
        $tim = time();
        if($menu == 1) {
            $fileName = 'Total-number-of-Maintenance-Checklist-'.date("Y", $tim).'-'.date("m", $tim).'-'.date("d", $tim);
        } else if ($menu == 2) {
            $fileName = 'Normal-Conditions-'.date("Y", $tim).'-'.date("m", $tim).'-'.date("d", $tim);
        } else if ($menu == 3) {
            $fileName = 'Checklist-with-Follow-Up-Actions-'.date("Y", $tim).'-'.date("m", $tim).'-'.date("d", $tim);
        } else if ($menu == 4) {
            $fileName = 'Checklist-Pending-Submission-'.date("Y", $tim).'-'.date("m", $tim).'-'.date("d", $tim);
        }

        $baseModel->downloadExcel($html, $fileName);
    }

    public function menuSend(Request $request, SystemLog $systemLogModel, Base $baseModel){
        if ($request->isMethod('post')) {
            $startTime = $request->input('start_time');
            $endTime = $request->input('end_time');
            $type = $request->input('type');
            $brand = $request->input('brand');
            $recipient = $request->input('recipient');
            $addCC = $request->input('addCC');
            $content = $request->input('content');
            $subject = $request->input('subject');
            $menu = $request->input('menu');

            $html = $systemLogModel->menuListBlade($menu, $startTime, $endTime, $type, $brand);
            $filePath = $baseModel->saveExcelFile($html);

            $res = $baseModel->sendEmail($recipient, $addCC, $filePath, $content, $subject);
            if ($res['status'] == 0) {
                return $this->msg('Send Success!', 'admin.hkaastaff.summary', ['startTime' => $startTime, 'endTime' => $endTime, 'type' => $type, 'brand' => $brand]);
            } else {
                return $this->msg('Send Faild!');
            }
        } else {
            $startTime = $request->input('start_time');
            $endTime = $request->input('end_time');
            $type = $request->input('type');
            $brand = $request->input('brand');
            $menu = $request->input('menu');

            $startTime = strtotime($startTime);
            $endTime = strtotime($endTime.' 23:59:59');

            return view('admin.hkaastaff_send_menu', ['startTime' => $startTime, 'endTime' => $endTime, 'type' => $type, 'brand' => $brand, 'menu' => $menu]);
        }
    }

    public function checkUsePermit(Request $request, SystemLog $systemLogModel){
        $systemLogId = $request->input('system_log_id');
        $type = $request->input('type');
        if (empty($systemLogId)) {
            return $this->msg('Check Faild!');
        }

        $systemLogInfo = $systemLogModel->getOne(['system_log_id' => $systemLogId]);

        if ($type == 'confirm') {
            return view('admin.hkaastaff_check_use_permit_confirm', ['systemLogInfo' => $systemLogInfo]);
        } else {
            return view('admin.hkaastaff_check_use_permit', ['systemLogInfo' => $systemLogInfo]);
        }
    }

    public function checkIncidentReport(Request $request, SystemLog $systemLogModel){
        $systemLogId = $request->input('system_log_id');
        $type = $request->input('type');
        if (empty($systemLogId)) {
            return $this->msg('Check Faild!');
        }

        $systemLogInfo = $systemLogModel->getOne(['system_log_id' => $systemLogId]);

        if ($type == 'confirm') {
            return view('admin.hkaastaff_check_incident_report_confirm', ['systemLogInfo' => $systemLogInfo]);
        } else {
            return view('admin.hkaastaff_check_incident_report', ['systemLogInfo' => $systemLogInfo]);
        }
    }

    public function statisticsSort(Request $request, SystemCrontab $systemCrontabModel){
        $startTime = $request->input('start_time');
        $endTime = $request->input('end_time');
        $type = $request->input('type');
        $brand = $request->input('brand');
        $sortKey = $request->input('sort_key');
        $sort = $request->input('sort');
        $menu = $request->input('menu');

        if ($sortKey == 'System ID') {
            $sortKey = 'system_id';
        }
        if ($sortKey == 'Form ID') {
            $sortKey = 'from_id';
        }

        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime.' 23:59:59');

        if ($menu == 1) {
            $data = $systemCrontabModel->getTotalData($startTime, $endTime, $type, $brand);

            if ($sort == 'asc') {
                array_multisort(array_column($data['systemInfo'],$sortKey),SORT_ASC, SORT_NATURAL, $data['systemInfo']);
            } else {
                array_multisort(array_column($data['systemInfo'],$sortKey),SORT_DESC, SORT_NATURAL, $data['systemInfo']);
            }

            $html = '';
            if(!empty($data['systemInfo'])){
                foreach($data['systemInfo'] as $value){
                    if(!empty($value['system_log'])){
                        $license_image = '';
                        if (!empty($value['system_log']['license_image'])) {
                            $license_image = '✓';
                        }
                        $license_expiry_time = '';
                        if (!empty($value['system_log']['license_expiry_time'])) {
                            $license_expiry_time = date('Y-m-d', $value['system_log']['license_expiry_time']);
                        }
                        $incident_image = '';
                        if (!empty($value['system_log']['incident_image'])) {
                            $incident_image = '✓';
                        }
                        $incident_report_time = '';
                        if (!empty($value['system_log']['incident_report_time'])) {
                            $incident_report_time = date("Y-m-d", $value['system_log']['incident_report_time']);
                        }
                        $sign_time = '';
                        if (!empty($value['system_log']['sign_time'])) {
                            $sign_time = date("Y-m-d", $value['system_log']['sign_time']);
                        }
                        $supervisor_remarks = '';
                        if (!empty($value['system_log']['task'])) {
                            foreach($value['system_log']['task'] as $v){
                                if (!empty($v['supervisor_remarks'])) {
                                    $supervisor_remarks = '✓';
                                    break;
                                }
                            }
                        }
                        $uri = route('admin.hkaastaff.task');
                        $red = '';
                        if($value['system_log']['license_expiry_time'] < strtotime('+1 month')) {
                            $red = 'style="color:red"';
                        }
                        $html .= '<tr>
                                <td style="text-align: center;"><a style="color:blue;" href="'.$uri.'?system_log_id='.$value['system_log']['system_log_id'].'">'.$value['system_log']['system_id'].'</a></td>
                                <td style="text-align: center;"><a style="color:blue;" href="'.$uri.'?system_log_id='.$value['system_log']['system_log_id'].'">'.$value['system_log']['from_id'].'</a></td>
                                <td style="text-align: center;">'.date("Y-m-d", $value['system_log']['work_time']).'</td>
                                <td style="text-align: center;">'.$sign_time.'</td>
                                <td style="text-align: center;">'.$supervisor_remarks.'</td>
                                <td style="text-align: center;">'.$license_image.'</td>
                                <td style="text-align: center;"><a '.$red.'>'.$license_expiry_time.'</a></td>
                                <td style="text-align: center;">'.$incident_image.'</td>
                                <td style="text-align: center;">'.$incident_report_time.'</td>
                            </tr>';
                    } else {
                        $html .= '<tr>
                                <td style="text-align: center;">'.$value['system_id'].'</td>
                                <td style="text-align: center;">'.$value['from_id'].'</td>
                                <td style="text-align: center;">'.date("Y-m-d", $value['work_time']).'</td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                            </tr>';
                    }
                }
            }
        } else if ($menu == 2) {
            $data = $systemCrontabModel->getNormalConditionsData($startTime, $endTime, $type, $brand);

            if ($sort == 'asc') {
                array_multisort(array_column($data,$sortKey),SORT_ASC, SORT_NATURAL, $data);
            } else {
                array_multisort(array_column($data,$sortKey),SORT_DESC, SORT_NATURAL, $data);
            }

            $html = '';
            if(!empty($data)){
                foreach($data as $value){
                    $license_image = '';
                    if (!empty($value['license_image'])) {
                        $license_image = '✓';
                    }
                    $license_expiry_time = '';
                    if (!empty($value['license_expiry_time'])) {
                        $license_expiry_time = date('Y-m-d', $value['license_expiry_time']);
                    }
                    $incident_image = '';
                    if (!empty($value['incident_image'])) {
                        $incident_image = '✓';
                    }
                    $incident_report_time = '';
                    if (!empty($value['incident_report_time'])) {
                        $incident_report_time = date("Y-m-d", $value['incident_report_time']);
                    }
                    $sign_time = '';
                    if (!empty($value['sign_time'])) {
                        $sign_time = date("Y-m-d", $value['sign_time']);
                    }
                    $supervisor_remarks = '';
                    if (!empty($value['task'])) {
                        foreach($value['task'] as $v){
                            if (!empty($v['supervisor_remarks'])) {
                                $supervisor_remarks = '✓';
                                break;
                            }
                        }
                    }
                    $uri = route('admin.hkaastaff.task');
                    $red = '';
                    if($value['license_expiry_time'] < strtotime('+1 month')) {
                        $red = 'style="color:red"';
                    }
                    $html .= '<tr>
                                <td style="text-align: center;"><a style="color:blue;" href="'.$uri.'?system_log_id='.$value['system_log_id'].'">'.$value['system_id'].'</a></td>
                                <td style="text-align: center;"><a style="color:blue;" href="'.$uri.'?system_log_id='.$value['system_log_id'].'">'.$value['from_id'].'</a></td>
                                <td style="text-align: center;">'.date("Y-m-d", $value['work_time']).'</td>
                                <td style="text-align: center;">'.$sign_time.'</td>
                                <td style="text-align: center;">'.$supervisor_remarks.'</td>
                                <td style="text-align: center;">'.$license_image.'</td>
                                <td style="text-align: center;"><a '.$red.'>'.$license_expiry_time.'</a></td>
                                <td style="text-align: center;">'.$incident_image.'</td>
                                <td style="text-align: center;">'.$incident_report_time.'</td>
                            </tr>';
                }
            }

        } else if ($menu == 3) {

            $data = $systemCrontabModel->getFollowUpActionsData($startTime, $endTime, $type, $brand);

            if ($sort == 'asc') {
                array_multisort(array_column($data,$sortKey),SORT_ASC, SORT_NATURAL, $data);
            } else {
                array_multisort(array_column($data,$sortKey),SORT_DESC, SORT_NATURAL, $data);
            }

            $html = '';
            if(!empty($data)){
                foreach($data as $value){
                    $license_image = '';
                    if (!empty($value['license_image'])) {
                        $license_image = '✓';
                    }
                    $license_expiry_time = '';
                    if (!empty($value['license_expiry_time'])) {
                        $license_expiry_time = date('Y-m-d', $value['license_expiry_time']);
                    }
                    $incident_image = '';
                    if (!empty($value['incident_image'])) {
                        $incident_image = '✓';
                    }
                    $incident_report_time = '';
                    if (!empty($value['incident_report_time'])) {
                        $incident_report_time = date("Y-m-d", $value['incident_report_time']);
                    }
                    $sign_time = '';
                    if (!empty($value['sign_time'])) {
                        $sign_time = date("Y-m-d", $value['sign_time']);
                    }
                    $supervisor_remarks = '';
                    if (!empty($value['task'])) {
                        foreach($value['task'] as $v){
                            if (!empty($v['supervisor_remarks'])) {
                                $supervisor_remarks = '✓';
                                break;
                            }
                        }
                    }
                    $uri = route('admin.hkaastaff.task');
                    $red = '';
                    if($value['license_expiry_time'] < strtotime('+1 month')) {
                        $red = 'style="color:red"';
                    }
                    $html .= '<tr>
                                <td style="text-align: center;"><a style="color:blue;" href="'.$uri.'?system_log_id='.$value['system_log_id'].'">'.$value['system_id'].'</a></td>
                                <td style="text-align: center;"><a style="color:blue;" href="'.$uri.'?system_log_id='.$value['system_log_id'].'">'.$value['from_id'].'</a></td>
                                <td style="text-align: center;">'.date("Y-m-d", $value['work_time']).'</td>
                                <td style="text-align: center;">'.$sign_time.'</td>
                                <td style="text-align: center;">'.$supervisor_remarks.'</td>
                                <td style="text-align: center;">'.$license_image.'</td>
                                <td style="text-align: center;"><a '.$red.'>'.$license_expiry_time.'</a></td>
                                <td style="text-align: center;">'.$incident_image.'</td>
                                <td style="text-align: center;">'.$incident_report_time.'</td>
                            </tr>';
                }
            }

        } else if ($menu = 4) {
            $data = $systemCrontabModel->getPendingSubmissionData($startTime, $endTime, $type, $brand);

            if ($sort == 'asc') {
                array_multisort(array_column($data,$sortKey),SORT_ASC, SORT_NATURAL, $data);
            } else {
                array_multisort(array_column($data,$sortKey),SORT_DESC, SORT_NATURAL, $data);
            }

            $html = '';
            if(!empty($data)){
                foreach($data as $value){
                    if (!empty($value['systemLogData'])){
                        $license_image = '';
                        if (!empty($value['systemLogData']['license_image'])) {
                            $license_image = '✓';
                        }
                        $license_expiry_time = '';
                        if (!empty($value['systemLogData']['license_expiry_time'])) {
                            $license_expiry_time = date('Y-m-d', $value['systemLogData']['license_expiry_time']);
                        }
                        $incident_image = '';
                        if (!empty($value['systemLogData']['incident_image'])) {
                            $incident_image = '✓';
                        }
                        $incident_report_time = '';
                        if (!empty($value['systemLogData']['incident_report_time'])) {
                            $incident_report_time = date("Y-m-d", $value['systemLogData']['incident_report_time']);
                        }
                        $sign_time = '';
                        if (!empty($value['systemLogData']['sign_time'])) {
                            $sign_time = date("Y-m-d", $value['systemLogData']['sign_time']);
                        }
                        $supervisor_remarks = '';
                        if (!empty($value['systemLogData']['task'])) {
                            foreach($value['systemLogData']['task'] as $v){
                                if (!empty($v['supervisor_remarks'])) {
                                    $supervisor_remarks = '✓';
                                    break;
                                }
                            }
                        }
                        $uri = route('admin.hkaastaff.task');
                        $red = '';
                        if($value['systemLogData']['license_expiry_time'] < strtotime('+1 month')) {
                            $red = 'style="color:red"';
                        }
                        $html .= '<tr>
                                <td style="text-align: center;"><a style="color:blue;" href="'.$uri.'?system_log_id='.$value['systemLogData']['system_log_id'].'">'.$value['systemLogData']['system_id'].'</a></td>
                                <td style="text-align: center;"><a style="color:blue;" href="'.$uri.'?system_log_id='.$value['systemLogData']['system_log_id'].'">'.$value['systemLogData']['from_id'].'</a></td>
                                <td style="text-align: center;">'.date("Y-m-d", $value['systemLogData']['work_time']).'</td>
                                <td style="text-align: center;">'.$sign_time.'</td>
                                <td style="text-align: center;">'.$supervisor_remarks.'</td>
                                <td style="text-align: center;">'.$license_image.'</td>
                                <td style="text-align: center;"><a '.$red.'>'.$license_expiry_time.'</a></td>
                                <td style="text-align: center;">'.$incident_image.'</td>
                                <td style="text-align: center;">'.$incident_report_time.'</td>
                            </tr>';
                    } else {
                        $html .= '<tr>
                                <td style="text-align: center;">'.$value['system_id'].'</td>
                                <td style="text-align: center;">'.$value['from_id'].'</td>
                                <td style="text-align: center;">'.date("Y-m-d", $value['work_time']).'</td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                                <td style="text-align: center;"></td>
                            </tr>';
                    }
                }
            }
        }

        $html .= '----------'.$sort;

        echo $html;exit;
    }

    public function downloadCrontab(){
        $pathName = '/storage/systemCron.xlsx';
        if (is_file(public_path($pathName))) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename=Scheduler-Form.xlsx');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize(public_path($pathName)));
            readfile(public_path($pathName));
            exit;
        }

        return $this->msg('History version not found！');
    }
}
