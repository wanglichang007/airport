<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SystemLog;

class BaseUserController extends BaseController
{
    private $store;
    //
    public function __construct(Request $request, SystemLog $systemLogModel)
    {
        $pendingReview = $systemLogModel->getMore(['status' => 1]);
        view()->share('reviewNumber', count($pendingReview));
        $this->middleware(function($request, $next) {
            $this->store = $request->session()->get("airportUser");

            if ($this->store) {
                $routeName = $request->route()->getName();
                $menu = $this->menu();

                if (empty($menu[$this->store['type']])) {
                    return $this->msg('User Type Error!');
                }

                $useMenu = $menu[$this->store['type']];
                if (!in_array($routeName, $useMenu)) {
                    return redirect()->route(current($useMenu));
                }
                return $next($request);
            } else {
                return redirect()->route("admin.login");
            }
        });
    }
}
