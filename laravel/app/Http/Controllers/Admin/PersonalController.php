<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Users;
use Illuminate\Http\Request;

class PersonalController extends BaseUserController
{
    //
    public function uppassword(Request $request, Users $usersModel){
        if ($request->isMethod('post')) {
            $password = $request->input('password');
            $repeat_password = $request->input('repeat_password');

            if (empty($password)) {
                return $this->msg('New Password Not allowed to be empty!');
            }

            if ($password != $repeat_password) {
                return $this->msg('The two passwords are inconsistent!');
            }

            $usersInfo = $request->session()->get("airportUser");
            $usersModel->updateOne($usersInfo['users_id'], ['password' => md5($password)]);


            return redirect()->route('admin.logout');
        } else {
            return view('admin.uppassword');
        }
    }
}
