<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogoutController extends BaseUserController
{
    //
    public function index(Request $request){
        $request->session()->forget('airportUser');
        return redirect()->route('admin.login');
    }
}
