<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    public function msg($message, $route = '', $params = []){
        return view('admin.message', ['message' => $message, 'route' => $route, 'params' => $params]);
    }

    /*
     * 用户可访问地址，如果无权限则访问存在权限的第一个路由
     */
    protected function menu(){
        return [
            //inspector用户可访问地址
            1 => [
                'admin.inspector',
                'admin.inspector.license',
                'admin.inspector.create',
                'admin.inspector.update',
                'admin.inspector.formid',
                'admin.inspector.report',
                'admin.inspector.task',
                'admin.inspector.sign',
                'admin.inspector.clear',
                'admin.inspector.image',
                'admin.inspector.updateLicense',
                'admin.inspector.updateReport',
                'admin.inspector.uploadTaskImage',
                'admin.inspector.delTaskImage',
                'admin.personal.uppassword',
                'admin.inspector.download',
                'admin.logout',
                'admin.upload',
            ],
            //supervisor用户可访问地址
            2 => [
                'admin.supervisor',
                'admin.supervisor.individual',
                'admin.supervisor.task',
                'admin.supervisor.summary',
                'admin.supervisor.confirm',
                'admin.supervisor.download',
                'admin.supervisor.downloadSummary',
                'admin.supervisor.sign',
                'admin.upload.image',
                'admin.supervisor.image',
                'admin.supervisor.totalNumber',
                'admin.supervisor.normalConditions',
                'admin.supervisor.followUpActions',
                'admin.supervisor.pendingSubmission',
                'admin.supervisor.send',
                'admin.supervisor.sendSummary',
                'admin.supervisor.menuDownload',
                'admin.supervisor.menuSend',
                'admin.supervisor.pendingReview',
                'admin.supervisor.checkUsePermit',
                'admin.supervisor.checkIncidentReport',
                'admin.supervisor.uploadTaskImage',
                'admin.supervisor.delTaskImage',
                'admin.supervisor.statisticsSort',
                'admin.supervisor.downloadPdf',
                'admin.supervisor.testImage',
                'admin.supervisor.downloadCrontab',
                'admin.inspector.formid',
                'admin.personal.uppassword',
                'admin.logout',
                'admin.upload',
            ],
            //hkaa_staff用户可访问地址
            3 => [
                'admin.hkaastaff',
                'admin.hkaastaff.individual',
                'admin.hkaastaff.task',
                'admin.hkaastaff.summary',
                'admin.hkaastaff.confirm',
                'admin.hkaastaff.download',
                'admin.hkaastaff.downloadSummary',
                'admin.hkaastaff.sign',
                'admin.upload.image',
                'admin.hkaastaff.image',
                'admin.hkaastaff.totalNumber',
                'admin.hkaastaff.normalConditions',
                'admin.hkaastaff.followUpActions',
                'admin.hkaastaff.pendingSubmission',
                'admin.hkaastaff.send',
                'admin.hkaastaff.sendSummary',
                'admin.hkaastaff.menuDownload',
                'admin.hkaastaff.menuSend',
                'admin.hkaastaff.pendingReview',
                'admin.hkaastaff.checkUsePermit',
                'admin.hkaastaff.checkIncidentReport',
                'admin.hkaastaff.statisticsSort',
                'admin.hkaastaff.downloadCrontab',
                'admin.inspector.formid',
                'admin.personal.uppassword',
                'admin.logout',
                'admin.upload',
            ]
        ];
    }
}
