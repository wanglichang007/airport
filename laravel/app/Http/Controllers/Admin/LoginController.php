<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users;

class LoginController extends BaseController
{
    //
    public function index(Request $request, Users $userModel){
        if ($request->isMethod("post")) {
            $account = $request->post('account');
            $password = $request->post('password');

            if (empty($account) || empty($password)) {
                return $this->msg("Account Or Password Not Null!");
            }

            $user = $userModel->getOne(['account' => $account, 'password' => md5($password)]);
            if (empty($user)) {
                return $this->msg("Account does not exist!");
            }

            $request->session()->put('airportUser', $user);

            $menu = $this->menu();
            $routes = current($menu[$user['type']]);
            return redirect()->route($routes);
        } else {
            return view('admin.login');
        }
    }
}
