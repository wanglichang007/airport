<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Upload;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UploadController extends BaseController
{
    //
    public function index(Request $request, Upload $upload){
        $image_file = $request->file('file');
        $image = $upload->uploadImage($image_file);

        echo json_encode($image, true);exit;
    }

    public function image(Request $request){
        $image = $request->input('image');
        $size = $request->input('size');

        $srcImage = public_path().$image;
        $w_h_size = explode('_', $size);

        $srcImageInfo = getimagesize($srcImage);
        $srcWidth = $srcImageInfo[0];
        $srcHeight = $srcImageInfo[1];

        $srcMime = $srcImageInfo['mime'];

        if($srcWidth >= $srcHeight){
            if (count($w_h_size) == 2) {
                $targeWidth = $w_h_size[0];
                $targeHeight = $w_h_size[1] * (round($srcHeight / $srcWidth, 2));
            } else {
                $targeWidth = $w_h_size[0];
                $targeHeight = $w_h_size[0] * (round($srcHeight / $srcWidth, 2));
            }
        } else {
            if (count($w_h_size) == 2) {
                $targeHeight = $w_h_size[1];
                $targeWidth = $w_h_size[0] * (round($srcWidth / $srcHeight, 2));
            } else {
                $targeHeight = $w_h_size[0];
                $targeWidth = $w_h_size[0] * (round($srcWidth / $srcHeight, 2));
            }
        }

        switch ($srcMime){
            case 'image/gif':
                $source_image = imagecreatefromgif($srcImage);
                break;

            case 'image/jpeg':
                $source_image = imagecreatefromjpeg($srcImage);
                break;

            case 'image/png':
                $source_image = imagecreatefrompng($srcImage);
                break;

            default:
                return false;
                break;
        }

        $target_image = imagecreatetruecolor(intval($targeWidth), intval($targeHeight));

        imagecopyresampled($target_image, $source_image, 0, 0, 0, 0, intval($targeWidth), intval($targeHeight), $srcWidth, $srcHeight);

        header('Content-type: image/jpeg');

        imagejpeg($target_image, null, 100);

        imagedestroy ($target_image);
    }
}
