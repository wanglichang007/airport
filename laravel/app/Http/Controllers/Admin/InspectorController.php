<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Base;
use App\Models\SystemCrontab;
use App\Models\SystemLogTaskImage;
use App\Models\Task;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\File;
use App\Helpers\Upload;
use App\Models\SystemLog;
use App\Models\System;
use ZipArchive;

class InspectorController extends BaseUserController
{
    //
    public function index(){

        return view("admin.inspector");
    }

    public function create(Request $request, SystemLog $systemLogModel, System $systemModel, SystemCrontab $systemCrontabModel){
        //创建systemId
        $systemId = $request->input("systemId");
        $fromId = $request->input("from_id");
        if (empty($systemId)) {
            return $this->msg("SystemId Not Null!");
        }

        if (empty($fromId)) {
            return $this->msg("FromId Not Null!");
        }

        $systemInfo = $systemModel->getOne(['system_id' => $systemId, 'from_id' => $fromId]);
        if (empty($systemInfo)) {
            return $this->msg("SystemId Not Find!");
        }

        $starDate = date('Y-m-01', time());
        $endDate = date('Y-m-d', strtotime("$starDate +1 month -1 day"));
        $starTime = strtotime($starDate.' 00:00:00');
        $endTime = strtotime($endDate.' 23:59:59');
        //第一步，验证当前的systemId+fromId是否需要本月检测
        $systemCrontabInfo = $systemCrontabModel->getOne(['system_id' => $systemId, 'from_id' => $fromId, ['work_time', '>=', $starTime], ['work_time', '<=', $endTime]]);
        if (empty($systemCrontabInfo)) {
            return $this->msg('SystemID No tasks this month！');
        }
        //第二步验证这个月是否开始做，或已经做完了
        $systemLogData = $systemLogModel->getOne(['system_id' => $systemId, 'from_id' => $fromId, ['create_time', '>=', $starTime], ['create_time', '<=', $endTime]]);
        if (!empty($systemLogData)) {
            return $this->msg("SystemID This months task has started or ended!");
        }

        $data = [
            'system_id' => $systemInfo['system_id'],
            'from_id' => $systemInfo['from_id'],
            's_id' => $systemInfo['id'],
            'license_image' => '',
            'license_expiry_time' => 0,
            'incident_image' => '',
            'incident_report_time' => 0,
            'work_time' => date('Y-m-d', $systemCrontabInfo['work_time']),
            'task' => '',
            'sign' => '',
            'sign_time' => 0,
            'status' => 0,
            'supervisor_sign' => '',
            'hkaa_remarks' => '',
            'hkaa_sign' => '',
            'last_modify_time' => time(),
            'create_time' => time()
        ];

        $systemLogId = $systemLogModel->createOne($data);

        if (empty($systemLogId)) {
            return $this->msg("New Faild!");
        }
        return redirect()->route('admin.inspector.task', ['system_log_id' => $systemLogId]);
        //$systemLogInfo = $systemLogModel->getOne(['system_log_id' => $systemLogId]);

        //return view('admin.inspector_license', ['systemId' => $systemId, 'dataModel' => $systemLogInfo]);
    }

    public function clear(Request $request, SystemLog $systemLogModel, SystemLogTaskImage $systemLogTaskImageModel){
        $systemLogId = $request->input('system_log_id');
        if (empty($systemLogId)) {
            return $this->msg('operation failed!');
        }

        $systemLogModel->updateOne($systemLogId, [
            'license_image' => '',
            'license_expiry_time' => 0,
            'incident_image' => '',
            'incident_report_time' => 0,
            'task' => '',
            'sign' => '',
            'status' => 0,
            'supervisor_sign' => '',
            'hkaa_remarks' => '',
            'hkaa_sign' => '',
            'sign_time' => 0,
            'last_modify_time' => time()
        ]);

        //删除systemLog 中task 中的图片
        $systemLogTaskImageModel->deleteAll([
            'system_log_id' => $systemLogId,
        ]);

        return redirect()->route("admin.inspector");
    }

    public function report(Request $request, SystemLog $systemLogModel, Upload $upload, System $systemModel){
        if ($request->isMethod('post')) {
            $view_image_url = $request->input('view_image_url');
            $system_log_id = $request->input('system_log_id');
            $incident_report_time = $request->input('incident_report_time');
            $image_file = $request->file('image_url');
            if (!empty($image_file)) {
                $image = $upload->uploadImage($image_file);
                if (!empty($image['code'])) {
                    return $this->msg('Upload failed!');
                }
                $image_url = $image['filePath'];
            } else {
                $image_url = !empty($view_image_url) ? $view_image_url : '';
            }

            $system_log_info = $systemLogModel->getOne(['system_log_id' => $system_log_id]);
            if (empty($system_log_info)) {
                return $this->msg('operation failed!');
            }

            $systemLogModel->updateOne($system_log_id, [
                'incident_image' => $image_url,
                'incident_report_time' => strtotime($incident_report_time)
            ]);

            $dataModel = $systemLogModel->getOne(['system_log_id' => $system_log_id]);

            $systemInfo = $systemModel->getOne(['id' => $dataModel['s_id']]);

            return view('admin.inspector_report', ['dataModel' => $dataModel, 'systemId' => $dataModel['system_id'], 'systemInfo' => $systemInfo]);
        } else {
            $systemLogId = $request->input('system_log_id');
            if (empty($systemLogId)) {
                return $this->msg('operation failed!');
            }

            $dataModel = $systemLogModel->getOne(['system_log_id' => $systemLogId]);

            $systemInfo = $systemModel->getOne(['id' => $dataModel['s_id']]);

            return view('admin.inspector_report', ['dataModel' => $dataModel, 'systemId' => $dataModel['system_id'], 'systemInfo' => $systemInfo]);
        }
    }

    public function license(Request $request, Upload $upload, SystemLog $systemLogModel, System $systemModel){
        if ($request->isMethod('post')) {
            $view_image_url = $request->input('view_image_url');
            $system_log_id = $request->input('system_log_id');
            $license_expiry_time = $request->input('license_expiry_time');
            $image_file = $request->file('image_url');
            if (!empty($image_file)) {
                $image = $upload->uploadImage($image_file);
                if (!empty($image['code'])) {
                    return $this->msg('Upload failed!');
                }
                $image_url = $image['filePath'];
            } else {
                $image_url = !empty($view_image_url) ? $view_image_url : '';
            }

            $system_log_info = $systemLogModel->getOne(['system_log_id' => $system_log_id]);
            if (empty($system_log_info)) {
                return $this->msg('operation failed!');
            }

            $systemLogModel->updateOne($system_log_id, [
                'license_image' => $image_url,
                'license_expiry_time' => strtotime($license_expiry_time)
            ]);

            $dataModel = $systemLogModel->getOne(['system_log_id' => $system_log_id]);

            return view('admin.inspector_license', ['dataModel' => $dataModel, 'systemId' => $dataModel['system_id']]);
        } else {
            $systemLogId = $request->input('system_log_id');
            if (empty($systemLogId)) {
                return $this->msg('operation failed!');
            }

            $dataModel = $systemLogModel->getOne(['system_log_id' => $systemLogId]);

            $systemInfo = $systemModel->getOne(['id' => $dataModel['s_id']]);

            return view('admin.inspector_license', ['dataModel' => $dataModel, 'systemId' => $systemInfo['system_id']]);
        }
    }

    public function task(Request $request, SystemLog $systemLogModel, Task $task, System $systemModel, SystemLogTaskImage $systemLogTaskImageModel){
        if ($request->isMethod('post')) {
            $data = $request->input('data');
            $systemLogId = $request->input('system_log_id');

            if (empty($systemLogId)) {
                return $this->msg('systemLogId Not allowed to be empty!');
            }

            $data = json_encode($data, true);

            $systemLogModel->updateOne($systemLogId, ['task' => $data]);

            return redirect()->route("admin.inspector.sign", ['system_log_id' => $systemLogId]);
        } else {
            $systemLogId = $request->input('system_log_id');
            if (empty($systemLogId)) {
                return $this->msg('operation failed!');
            }

            $systemLogInfo = $systemLogModel->getOne(['system_log_id' => $systemLogId]);

            //获取system id必填项
            $systemInfo = $systemModel->getOne(['system_id' => $systemLogInfo['system_id'], 'from_id' => $systemLogInfo['from_id']]);
            if (empty($systemInfo)) {
                return $this->msg('System Not Find!');
            }

            //组装task必填项
            $taskInfo = $systemModel->getTaskBySystemIdFromId($systemLogInfo['system_id'], $systemLogInfo['from_id']);
            //查询当前systemLogId下的当前task是否存储了图片
            foreach ($taskInfo as $key=> $value) {
                $taskImage = $systemLogTaskImageModel->getOne(['system_log_id' => $systemLogId, 'task_id' => $value['task_id'], 'type' => 1]);
                if (!empty($taskImage)) {
                    $taskInfo[$key]['inspector_upload_image'] = true;
                } else {
                    $taskInfo[$key]['inspector_upload_image'] = false;
                }
            }


            return view('admin.imspector_task', ['systemLogInfo' => $systemLogInfo, 'systemInfo' => $systemInfo, 'taskInfo' => $taskInfo]);
        }
    }

    public function sign(Request $request, SystemLog $systemLogModel){
        if ($request->isMethod('post')) {
            $systemLogId = $request->input('system_log_id');
            if (empty($systemLogId)) {
                return $this->msg('operation failed!');
            }

            $sign = $request->input('sign');
            if (empty($sign)) {
                return $this->msg('sign, please!');
            }

            list($type, $sign) = explode(';', $sign);
            list(, $sign)      = explode(',', $sign);
            $data = base64_decode($sign);

            $signName = $systemLogId.'_'.time().'image.png';
            $datePath = date('Y/m/d', time());
            $filePath = storage_path("app/image/".$datePath);
            \Illuminate\Support\Facades\File::isDirectory($filePath) or \Illuminate\Support\Facades\File::makeDirectory($filePath, 0777, true, true);

            file_put_contents($filePath.'/'.$signName, $data);

            $userInfo = $request->session()->get("airportUser");
            $systemLogModel->updateOne($systemLogId, ['sign' => '/storage/'.$datePath.'/'.$signName, 'status' => 1, 'sign_time' => time(), 'sign_name' => $userInfo['account']]);

            return $this->msg('Complete the signature and wait for review', 'admin.inspector');
            //return redirect()->route("admin.inspector");
        } else {
            $systemLogId = $request->input('system_log_id');
            if (empty($systemLogId)) {
                return $this->msg('operation failed!');
            }

            $systemLogInfo = $systemLogModel->getOne(['system_log_id' => $systemLogId]);

            return view('admin.imspector_sign', ['systemLogInfo' => $systemLogInfo]);
        }
    }

    public function image(Request $request, SystemLogTaskImage $systemLogTaskImageModel, SystemLog $systemLogModel){
        if ($request->isMethod('post')) {
            $systemLogId = $request->input('system_log_id');
            $taskId = $request->input('task_id');
            $type = $request->input('type');
            $images = $request->input('images');

            if (empty($systemLogId) || empty($taskId) || empty($type)) {
                return $this->msg('operation failed!');
            }

            //删除原图
            $systemLogTaskImageModel->deleteAll([
                'system_log_id' => $systemLogId,
                'task_id' => $taskId,
                'type' => $type
            ]);

            //添加新图
            if (!empty($images)) {
                foreach($images as $image) {
                    $systemLogTaskImageModel->createOne([
                        'system_log_id' => $systemLogId,
                        'task_id' => $taskId,
                        'type' => $type,
                        'image' => $image
                    ]);
                }
            }

            return redirect()->route('admin.inspector.image', ['system_log_id' => $systemLogId, 'task_id' => $taskId, 'type' => $type]);
        } else {
            $systemLogId = $request->input('system_log_id');
            $taskId = $request->input('task_id');
            $type = $request->input('type');

            if (empty($systemLogId)) {
                return $this->msg('operation failed!');
            }

            $images = $systemLogTaskImageModel->getMore(['system_log_id' => $systemLogId, 'task_id' => $taskId, 'type' => $type]);

            return view('admin.inspector_image', ['images' => $images, 'systemLogId' => $systemLogId, 'taskId' => $taskId, 'type' => $type]);
        }
    }

    public function formid(Request $request, System $systemModel, SystemLog $systemLogModel, SystemCrontab $systemCrontabModel){
        $systemId = $request->input('systemId');

        $returns = [
            'code' => 0,
            'message' => 'Success!'
        ];

        if (empty($systemId)) {
            $returns['code'] = 1;
            $returns['message'] = 'systemId not null!';
            echo json_encode($returns, true);exit;
        }

        $data = $systemModel->getMore(['system_id' => $systemId]);

        if (empty($data)) {
            $returns['code'] = 2;
            $returns['message'] = 'Not Find!';
            echo json_encode($returns, true);exit;
        }

        $returnData = [];

        foreach($data as $key => $value) {
            $systemLogInfo = $systemLogModel->getOne(['s_id' => $value['id']]);
            $systemLogStatus = !empty($systemLogInfo['status']) ? $systemLogInfo['status'] : 0;
            $systemLogId = !empty($systemLogInfo['system_log_id']) ? $systemLogInfo['system_log_id'] : 0;
            $isNewInput = false;
            //验证是否可以显示new input 按钮
            $starDate = date('Y-m-01', time());
            $endDate = date('Y-m-d', strtotime("$starDate +1 month -1 day"));
            $starTime = strtotime($starDate.' 00:00:00');
            $endTime = strtotime($endDate.' 23:59:59');
            //第一步，验证当前的systemId+fromId是否需要本月检测
            $systemCrontabInfo = $systemCrontabModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id'], ['work_time', '>=', $starTime], ['work_time', '<=', $endTime]]);
            if (!empty($systemCrontabInfo)) {
                //第二步验证这个月是否开始做，或已经做完了
                $systemLogData = $systemLogModel->getOne(['system_id' => $value['system_id'], 'from_id' => $value['from_id'], ['create_time', '>=', $starTime], ['create_time', '<=', $endTime]]);
                if (empty($systemLogData)) {
                    $isNewInput = true;
                }

//                $data[$key]['systemLogId'] = $systemLogId;
//                $data[$key]['systemLogStatus'] = $systemLogStatus;
//                $data[$key]['isNewInput'] = $isNewInput;
                $value['systemLogId'] = $systemLogId;
                $value['systemLogStatus'] = $systemLogStatus;
                $value['isNewInput'] = $isNewInput;

                $returnData[$systemCrontabInfo['work_time']] = $value;
            }
        }

        $returns['data'] = $returnData;

        echo json_encode($returns, true);exit;
    }

    public function updateLicense(Request $request, Upload $upload, SystemLog $systemLogModel, System $systemModel){
        if ($request->isMethod('post')) {
            $view_image_url = $request->input('view_image_url');
            $system_log_id = $request->input('system_log_id');
            $license_expiry_time = $request->input('license_expiry_time');
            $image_file = $request->file('image_url');
            if (!empty($image_file)) {
                $image = $upload->uploadImage($image_file);
                if (!empty($image['code'])) {
                    return $this->msg('Upload failed!');
                }
                $image_url = $image['filePath'];
            } else {
                $image_url = !empty($view_image_url) ? $view_image_url : '';
            }

            $system_log_info = $systemLogModel->getOne(['system_log_id' => $system_log_id]);
            if (empty($system_log_info)) {
                return $this->msg('operation failed!');
            }

            $systemLogModel->updateOne($system_log_id, [
                'license_image' => $image_url,
                'license_expiry_time' => strtotime($license_expiry_time)
            ]);

            return redirect()->route('admin.inspector');
        } else {
            $systemLogId = $request->input('system_log_id');
            if (empty($systemLogId)) {
                return $this->msg('operation failed!');
            }

            $dataModel = $systemLogModel->getOne(['system_log_id' => $systemLogId]);

            $systemInfo = $systemModel->getOne(['id' => $dataModel['s_id']]);

            return view('admin.inspector_uploadLicense', ['dataModel' => $dataModel, 'systemId' => $systemInfo['system_id']]);
        }
    }

    public function updateReport(Request $request, Upload $upload, SystemLog $systemLogModel, System $systemModel){
        if ($request->isMethod('post')) {
            $view_image_url = $request->input('view_image_url');
            $system_log_id = $request->input('system_log_id');
            $incident_report_time = $request->input('incident_report_time');
            $image_file = $request->file('image_url');
            if (!empty($image_file)) {
                $image = $upload->uploadImage($image_file);
                if (!empty($image['code'])) {
                    return $this->msg('Upload failed!');
                }
                $image_url = $image['filePath'];
            } else {
                $image_url = !empty($view_image_url) ? $view_image_url : '';
            }

            $system_log_info = $systemLogModel->getOne(['system_log_id' => $system_log_id]);
            if (empty($system_log_info)) {
                return $this->msg('operation failed!');
            }

            $systemLogModel->updateOne($system_log_id, [
                'incident_image' => $image_url,
                'incident_report_time' => strtotime($incident_report_time)
            ]);

            return redirect()->route('admin.inspector');
        } else {
            $systemLogId = $request->input('system_log_id');
            if (empty($systemLogId)) {
                return $this->msg('operation failed!');
            }

            $dataModel = $systemLogModel->getOne(['system_log_id' => $systemLogId]);

            $systemInfo = $systemModel->getOne(['id' => $dataModel['s_id']]);

            return view('admin.inspector_uploadReport', ['dataModel' => $dataModel, 'systemId' => $dataModel['system_id'], 'systemInfo' => $systemInfo]);
        }
    }

    public function uploadTaskImage(Request $request, Upload $upload, SystemLogTaskImage $systemLogTaskImageModel){
        $systemLogId = $request->input('system_log_id');
        $taskId = $request->input('task_id');
        $type = $request->input('type');
        $imageFile = $request->file('file');
        $image = $upload->uploadImage($imageFile);

        $image_id = $systemLogTaskImageModel->createOne([
            'system_log_id' => $systemLogId,
            'task_id' => $taskId,
            'type' => $type,
            'image' => $image['filePath']
        ]);

        if (!empty($image_id)) {
            echo json_encode(['code' => 0, 'viewPath' => $image['viewPath']], true);exit;
        } else {
            echo json_encode(['code' =>500], true);exit;
        }
    }

    public function delTaskImage(Request $request, SystemLogTaskImage $systemLogTaskImageModel){
        $system_log_task_image_id = $request->input('system_log_task_image_id');

        $res = $systemLogTaskImageModel->deleteAll(['system_log_task_image_id' => $system_log_task_image_id]);

        if (!empty($res)) {
            echo json_encode(['code' => 0], true);exit;
        } else {
            echo json_encode(['code' => 500], true);exit;
        }

    }

    public function download(Request $request, SystemLog $systemLogModel, Base $baseModel){
        $systemLogId = $request->input('system_log_id');
        $systemInfo = $systemLogModel->getOne(['system_log_id' => $systemLogId]);

        $data = $systemLogModel->insectorSupervisorBlade($systemLogId, 'inspector');

        $tim = time();
        $fileName = $systemInfo['system_id'].'-'.$systemInfo['from_id'].'-'.date("Y", $tim).'-'.date('m', $tim).'-'.date('d', $tim);
        $baseModel->downloadExcel($data, $fileName);
//        $excelFilePath = $baseModel->saveExcelFile($data, $fileName);

//        $pdfFilePath = $baseModel->downloadPdf($systemLogId);
//
//        $zipFilePath = storage_path('app/image/'.date('Y', $tim).'/'.date('m', $tim).'/'.date('d', $tim).'/');
//        if (!is_dir($zipFilePath)) {
//            mkdir($zipFilePath, 0755);
//        }
//        $zipFilename = $zipFilePath.$fileName.'.zip';
//        $zip = new ZipArchive;
//        if ($zip->open($zipFilename, ZIPARCHIVE::CREATE)!==TRUE) {
//            return $this->msg('zip faild!');
//        }
//
//        if (!empty($excelFilePath)) {
//            $zip->addFile($excelFilePath, $fileName.'.xlsx');
//        }
//        if (!empty($pdfFilePath)) {
//            $zip->addFile($pdfFilePath, $fileName.'.pdf');
//        }
//
//        $zip->close();
//
//        if (is_file($zipFilename)) {
//            return response()->download($zipFilename);
//        } else {
//            return $this->msg('Not Data!');
//        }
    }
}
