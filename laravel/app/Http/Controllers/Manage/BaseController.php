<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    //
    public function msg($message, $route = ''){
        return view('manage.message', ['message' => $message, 'route' => $route]);
    }
}
