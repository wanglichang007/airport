<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\Manage;
use Illuminate\Http\Request;

class LoginController extends BaseController
{
    //
    public function index(Request $request, Manage $manageModel){
        if ($request->isMethod("post")) {
            $account = $request->post('account');
            $password = $request->post('password');

            if (empty($account) || empty($password)) {
                return $this->msg("Account Or Password Not Null!");
            }

            $user = $manageModel->getOne(['account' => $account, 'password' => md5($password)]);
            if (empty($user)) {
                return $this->msg("Account does not exist!");
            }

            $request->session()->put('manageUser', $user);

            return redirect()->route("manage.users");
        } else {
            return view('manage.login');
        }
    }
}
