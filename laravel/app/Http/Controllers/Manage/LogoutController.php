<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LogoutController extends BaseUserController
{
    //
    public function index(Request $request){
        $request->session()->forget('manageUser');
        return redirect()->route('manage.login');
    }
}
