<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseUserController extends BaseController
{
    //
    public function __construct()
    {
        $this->middleware(function($request, $next) {
            $this->store = $request->session()->get("manageUser");

            if ($this->store) {
                return $next($request);
            } else {
                return redirect()->route("manage.login");
            }
        });
    }
}
