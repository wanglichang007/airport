<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\Manage;
use Illuminate\Http\Request;

class ManagesController extends BaseUserController
{
    //
    public function index(Manage $manageModel){
        $data = $manageModel->getList();

        return view('manage.manages', ['data' => $data]);
    }

    public function create(Request $request, Manage $manageModel) {
        if ($request->isMethod('post')) {
            $account = $request->input('account');
            $password = $request->input('password');
            $repeat_password = $request->input('repeat_password');
            $name = $request->input('nickname');
            if (empty($account) || empty($password)) {
                return $this->msg('Account Password Not allowed to be empty!');
            }

            if ($password != $repeat_password) {
                return $this->msg('The passwords entered two times are inconsistent!');
            }

            $manageInfo = $manageModel->getOne(['account' => $account]);
            if (!empty($manageInfo)) {
                return $this->msg( 'Manage User already exists!');
            }

            $manage_id = $manageModel->createOne([
                'account' => $account,
                'password' => md5($password),
                'name' => $name,
                'create_time' => time()
            ]);
            if (empty($manage_id)) {
                return $this->msg("operation failed");
            }

            return redirect()->route('manage.manages');
        } else {
            return view('manage.manages_create');
        }
    }
}
