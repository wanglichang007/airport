<?php

namespace App\Http\Controllers\Manage;

use App\Helpers\Upload;
use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Equipment;
use App\Models\System;
use App\Models\SystemCrontab;
use App\Models\Task;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;

class SystemController extends BaseUserController
{
    //
    public function index(Request $request, System $systemModel, Task $taskModel, Equipment $equipmentModel, Brand $brandModel, Upload $upload){
        if ($request->isMethod('post')) {
            $files = $request->file('system');
            $file = $upload->uploadImage($files);
            if (!empty($file['code'])) {
                return $this->msg('Upload failed!');
            }
            $file_url = public_path().$file['filePath'];

            $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_url);
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType); //实例化阅读器对象。
            $spreadsheet = $reader->load($file_url);  //将文件读取到到$spreadsheet对象中
            $worksheet = $spreadsheet->getActiveSheet();   //获取当前文件内容
            $sheetAllCount = $spreadsheet->getSheetCount(); // 工作表总数
            for ($index = 0; $index < $sheetAllCount; $index++) {   //工作表标题
                $title[] = $spreadsheet->getSheet($index)->getTitle();
            }

            $sheet = $spreadsheet->getSheet(0); // 读取第一個工作表

            $highest_row = $sheet->getHighestRow(); // 取得总行数

            $highest_column = $sheet->getHighestColumn(); ///取得列数  字母abc...
            $highestColumnIndex = Coordinate::columnIndexFromString($highest_column);

            for ($i = 1; $i <= $highestColumnIndex; $i++) {
                for ($j = 1; $j <= $highest_row; $j++) {
                    if ($j < 4) {
                        continue;
                    }
                    if ($i == 1) {
                        $conent = $sheet->getCellByColumnAndRow($i, $j)->getFormattedValue();
                    } else {
                        $conent = $sheet->getCellByColumnAndRow($i, $j)->getCalculatedValue();
                    }

                    $info[$j][$i] = $conent;
                }
            }

            //处理数据
            $header = [];
            foreach ($info as $key=>$value) {
                if ($key == 4) {
                    $header = $value;
                    //处理task
                    $this->checkTask($value);
                } else {
                    //处理类型、品牌、及system
                    $this->checkSystem($value, $header);
                }
            }

            return redirect()->route('manage.system');
        } else {
            $systemId = $request->input('ststem_id');

            $where = [];
            if (!empty($systemId)) {
                $where['system_id'] = $systemId;
            }

            $data = $systemModel->getList(['where' => $where]);
            foreach ($data->items() as $key=>$value) {
                if (!empty($value['system_type_id'])) {
                    $equipmentInfo = $equipmentModel->getOne(['equipment_id' => $value['system_type_id']]);
                    if (!empty($equipmentInfo)) {
                        $data->items()[$key]['equipment'] = $equipmentInfo['name'];
                    }
                }

                if (!empty($value['brand_type_id'])) {
                    $brandInfo = $brandModel->getOne(['brand_id' => $value['brand_type_id']]);
                    if (!empty($brandInfo)) {
                        $data->items()[$key]['brand'] = $brandInfo['name'];
                    }
                }
            }
            return view('manage.system', ['data' => $data, 'where' => $where]);
        }
    }

    public function task(Request $request, System $systemModel, Task $taskModel){
        $systemId = $request->input('system_id');
        if (empty($systemId)) {
            return $this->msg("operation failed");
        }

        $systemInfo = $systemModel->getOne(['id' => $systemId]);
        if (empty($systemInfo)) {
            return $this->msg("System Not Find!");
        }

        $data = [];
        if(!empty($systemInfo['task'])) {
            $task = json_decode($systemInfo['task'], true);
            foreach ($task as $key=>$task_id) {
                $taskInfo = $taskModel->getOne(['task_id' => $task_id]);
                if (!empty($taskInfo)) {
                    $data[$key]['taskInfo'] = $taskInfo;
                }
            }
        }

        return view('manage.system_task', ['task' => $data]);
    }

    /*
     * 表头进行task处理,大于5列的为task
     */
    private function checkTask($line){
        $taskModel = new Task();
        foreach ($line as $key=>$value) {
            if ($key > 10) {
                $arr = explode('#', $value);
                //难数据准确性
                if (count($arr) == 3) {
                    $taskInfo = $taskModel->getOne(['task_id' => $arr[0]]);
                    if (!empty($taskInfo)) {
                        $taskModel->updateOne($taskInfo['task_id'], [
                            'clause_no' => $arr[1],
                            'name' => $arr[2]
                        ]);
                    } else {
                        $taskModel->createOne([
                            'task_id' => $arr[0],
                            'clause_no' => $arr[1],
                            'name' => $arr[2]
                        ]);
                    }
                }
            }
        }
    }

    /*
     * 处理类型、品牌、及system
     */
    private function checkSystem($line, $header){
        //处理类型
        $equipment_id = 0;
        if (!empty($line[4])) {
            $equipmentModel = new Equipment();
            $equipmentInfo = $equipmentModel->getOne(['name' => $line[4]]);
            if (empty($equipmentInfo)) {
                $equipment_id = $equipmentModel->createOne(['name' => $line[4]]);
            } else {
                $equipment_id = $equipmentInfo['equipment_id'];
            }
        }

        //处理品牌
        $brand_id = 0;
        if (!empty($line[5])) {
            $brandModel = new Brand();
            $brandInfo = $brandModel->getOne(['name' => $line[5]]);
            if (empty($brandInfo)) {
                $brand_id = $brandModel->createOne(['name' => $line[5]]);
            } else {
                $brand_id = $brandInfo['brand_id'];
            }
        }

        //组合system的task
        $task = [];
        foreach ($line as $key=>$value) {
            if ($key > 10) {
                if ($value == 1) {
                    $header_task = $header[$key];
                    $arr = explode('#', $header_task);
                    $task[] = $arr[0];
                }
            }
        }
        if (!empty($task)) {
            $taskStr = json_encode($task, true);
        } else {
            $taskStr = '';
        }

        $systemModel = new System();
        if (!empty($line[3]) && !empty($line[10])) {
            $systemInfo = $systemModel->getOne(['system_id' => $line[3], 'from_id' => $line[10]]);
            if (empty($systemInfo)) {
                $systemModel->createOne([
                    'system_id' => $line[3],
                    'from_id' => $line[10],
                    'task' => $taskStr,
                    'system_type_id' => $equipment_id,
                    'brand_type_id' => $brand_id,
                    'last_modify_time' => time()
                ]);
            } else {
                $systemModel->updateOne($systemInfo['id'], [
                    'task' => $taskStr,
                    'system_type_id' => $equipment_id,
                    'brand_type_id' => $brand_id,
                    'last_modify_time' => time()
                ]);
            }
        }
    }

    public function crontab(Request $request, SystemCrontab $systemCrontabModel, Upload $upload, System $systemModel){
        if ($request->isMethod('post')) {
            $files = $request->file('system');
            $file = $upload->uploadCrontab($files);
            if (!empty($file['code'])) {
                return $this->msg('Upload failed!');
            }
            $file_url = public_path().$file['filePath'];

            $inputFileType = \PhpOffice\PhpSpreadsheet\IOFactory::identify($file_url);
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType); //实例化阅读器对象。
            $spreadsheet = $reader->load($file_url);  //将文件读取到到$spreadsheet对象中
            $worksheet = $spreadsheet->getActiveSheet();   //获取当前文件内容
            $sheetAllCount = $spreadsheet->getSheetCount(); // 工作表总数
            for ($index = 0; $index < $sheetAllCount; $index++) {   //工作表标题
                $title[] = $spreadsheet->getSheet($index)->getTitle();
            }

            $sheet = $spreadsheet->getSheet(0); // 读取第一個工作表

            $highest_row = $sheet->getHighestRow(); // 取得总行数

            $highest_column = $sheet->getHighestColumn(); ///取得列数  字母abc...
            $highestColumnIndex = Coordinate::columnIndexFromString($highest_column);

            for ($i = 1; $i <= $highestColumnIndex; $i++) {
                for ($j = 1; $j <= $highest_row; $j++) {
                    if ($i == 1) {
                        $conent = $sheet->getCellByColumnAndRow($i, $j)->getFormattedValue();
                    } else {
                        $conent = $sheet->getCellByColumnAndRow($i, $j)->getCalculatedValue();
                    }

                    $info[$j][$i] = $conent;
                }
            }

            $data = [];
            $start = false;
            foreach($info as $key=>$value) {
                //从Date开头的下一行开始导入
                if ($start == false) {
                    if (strpos($value[1], 'Date') !== false) {
                        $start = true;

                    }
                    continue;
                }
                //开始导入数据
                foreach ($value as $k=>$v) {
                    //第一列为工作日期
                    if ($k == 1) {
                        $work_time = strtotime($v);
                    }

                    //如果大于第2列，则认为是systemId
                    if ($k > 2) {
                        if (!empty($v)) {
                            //用.切分systemId及form id
                            $sinfo = explode('-', $v);
                            //第一个元素为systemId其余的为form id
                            $systemId = array_shift($sinfo);
                            foreach ($sinfo as $sv) {
                                //查询相应的form id，sv的格式为A|B这种形式，需要去数据库匹配查询1WA类似于这种形式的from id
                                $systemInfo = $systemModel->getOne(['system_id' => $systemId, ['from_id', 'like', '%'.$sv.'%']]);
                                if (!empty($systemInfo)) {
                                    $id = $systemCrontabModel->createOne([
                                        'system_id' =>$systemId,
                                        'from_id' => $systemInfo['from_id'],
                                        'work_time' => $work_time
                                    ]);
                                }
                            }
                        }
                    }
                }
            }

            return redirect()->route('manage.system.crontab');
        } else {
            $data = $systemCrontabModel->getList([]);
            return view('manage.system_crontab', ['data' => $data]);
        }
    }

    public function clearCrontab(SystemCrontab $systemCrontabModel){
        $systemCrontabModel->truncateCrontab();

        return redirect()->route('manage.system.crontab');
    }

    public function downloadCrontab(){
        $pathName = '/storage/systemCron.xlsx';
        if (is_file(public_path($pathName))) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename=systemCron.xlsx');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize(public_path($pathName)));
            readfile(public_path($pathName));
            exit;
        }

        return $this->msg('History version not found！');
    }
}
