<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\System;
use App\Models\SystemLog;
use App\Models\SystemLogTaskImage;
use Illuminate\Http\Request;

class SystemLogController extends BaseUserController
{
    //
    public function index(Request $request, SystemLog $systemLogModel){
        $status = $request->input('status');

        $where = [];
        if (!empty($status)) {
            $where[] = ['status', '<=', $status];
            $where[] = ['status', '>', 0];
        }

        $data = $systemLogModel->getList(['where' => $where]);

        return view('manage.system_log', ['data' => $data, 'where' => ['status' => $status]]);
    }

    public function showAll(Request $request, SystemLog $systemLogModel, System $systemModel){
        $systemLogId = $request->input('system_log_id');
        if (empty($systemLogId)) {
            return $this->msg("SystemLogId Not Null!");
        }

        $systemLogInfo = $systemLogModel->getOne(['system_log_id' => $systemLogId]);
        if (empty($systemLogInfo)) {
            return $this->msg("The system id does not exist Or under submit!");
        }

        //组装task必填项
        $taskInfo = $systemModel->getTaskBySystemIdFromId($systemLogInfo['system_id'], $systemLogInfo['from_id']);

        $systemInfo = $systemModel->getOne(['id' => $systemLogInfo['s_id']]);

        return view('manage.system_log_show', ['systemLogInfo' => $systemLogInfo, 'taskInfo' => $taskInfo, 'systemInfo' => $systemInfo]);
    }

    public function systemLogTask(Request $request, SystemLogTaskImage $systemLogTaskImageModel) {
        $systemLogId = $request->input('system_log_id');
        $taskId = $request->input('task_id');
        $type = $request->input('type');

        if (empty($systemLogId)) {
            return $this->msg('operation failed!');
        }

        $images = $systemLogTaskImageModel->getMore(['system_log_id' => $systemLogId, 'task_id' => $taskId, 'type' => $type]);

        return view('manage.system_task_image', ['images' => $images, 'systemLogId' => $systemLogId, 'taskId' => $taskId, 'type' => $type]);
    }
}
