<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\Users;
use Illuminate\Http\Request;

class UsersController extends BaseUserController
{
    //
    public function index(Request $request, Users $usersModel){
        $account = $request->input('account');
        $type = $request->input('type');

        $where = [];
        if (!empty($account)) {
            $where['account'] = $account;
        }
        if (!empty($type)) {
            $where['type'] = $type;
        }

        $data = $usersModel->getList(['where' => $where]);

        return view('manage.users', ['data' => $data, 'where' => $where]);
    }

    public function create(Request $request, Users $usersModel){
        if ($request->isMethod('post')) {
            $account = $request->input('account');
            $password = $request->input('password');
            $repeat_password = $request->input('repeat_password');
            $name = $request->input('nickname');
            $type = $request->input('type');
            if (empty($account) || empty($password)) {
                return $this->msg('Account Password Not allowed to be empty!');
            }

            if ($password != $repeat_password) {
                return $this->msg('The passwords entered two times are inconsistent!');
            }

            $usersInfo = $usersModel->getOne(['account' => $account]);
            if (!empty($usersInfo)) {
                return $this->msg( 'User already exists!');
            }

            $users_id = $usersModel->createOne([
                'account' => $account,
                'password' => md5($password),
                'name' => $name,
                'type' => $type,
                'create_time' => time()
            ]);
            if (empty($users_id)) {
                return $this->msg("operation failed");
            }

            return redirect()->route('manage.users');
        }  else {
            return view('manage.users_create');
        }
    }

    public function delete(Request $request, Users $usersModel){
        $userId = intval($request->input('user_id'));

        if (empty($userId)) {
            return $this->msg("operation failed");
        }

        $usersModel->deleteAll(['users_id' => $userId]);

        return redirect()->route('manage.users');
    }
}
