<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


Route::get('/', [\App\Http\Controllers\Admin\IndexController::class, "index"])->name("admin.index");
Route::match(['get', 'post'], '/login', [\App\Http\Controllers\Admin\LoginController::class, "index"])->name("admin.login");
Route::match(['get', 'post'], '/personal/uppassword', [\App\Http\Controllers\Admin\PersonalController::class, "uppassword"])->name("admin.personal.uppassword");
Route::get('/logout', [\App\Http\Controllers\Admin\LogoutController::class, "index"])->name("admin.logout");
Route::match(['get', 'post'], '/upload', [\App\Http\Controllers\Admin\UploadController::class, "index"])->name("admin.upload");

//查询systemId
Route::get('/inspector', [\App\Http\Controllers\Admin\InspectorController::class, "index"])->name("admin.inspector");
//清空system记录数据
Route::get('/inspector/clear', [\App\Http\Controllers\Admin\InspectorController::class, "clear"])->name("admin.inspector.clear");
//上传准用证图片
Route::match(['get', 'post'],'/inspector/license', [\App\Http\Controllers\Admin\InspectorController::class, "license"])->name("admin.inspector.license");
//上传报告表单
Route::match(['get', 'post'],'/inspector/report', [\App\Http\Controllers\Admin\InspectorController::class, "report"])->name("admin.inspector.report");
//更新上传准用证图片
Route::match(['get', 'post'],'/inspector/updateLicense', [\App\Http\Controllers\Admin\InspectorController::class, "updateLicense"])->name("admin.inspector.updateLicense");
//更新上传报告表单
Route::match(['get', 'post'],'/inspector/updateReport', [\App\Http\Controllers\Admin\InspectorController::class, "updateReport"])->name("admin.inspector.updateReport");
//填写systemID必填的task
Route::match(['get', 'post'],'/inspector/task', [\App\Http\Controllers\Admin\InspectorController::class, "task"])->name("admin.inspector.task");
//签名
Route::match(['get', 'post'],'/inspector/sign', [\App\Http\Controllers\Admin\InspectorController::class, "sign"])->name("admin.inspector.sign");
//创建systemId
Route::get('/inspector/create', [\App\Http\Controllers\Admin\InspectorController::class, "create"])->name("admin.inspector.create");
//根据systemId获取formId
Route::get('/inspector/formid', [\App\Http\Controllers\Admin\InspectorController::class, "formid"])->name("admin.inspector.formid");
//system log必填中的task上传的图片
Route::match(['get', 'post'],'/inspector/image', [\App\Http\Controllers\Admin\InspectorController::class, "image"])->name("admin.inspector.image");
//异步上传task image
Route::match(['get', 'post'],'/inspector/uploadTaskImage', [\App\Http\Controllers\Admin\InspectorController::class, "uploadTaskImage"])->name("admin.inspector.uploadTaskImage");
//异步删除task image
Route::match(['get', 'post'],'/inspector/delTaskImage', [\App\Http\Controllers\Admin\InspectorController::class, "delTaskImage"])->name("admin.inspector.delTaskImage");
//下载Inspector_Record Supervisor_Comment excel
Route::get('/inspector/download', [\App\Http\Controllers\Admin\InspectorController::class, "download"])->name("admin.inspector.download");



//supervisor角色首页
Route::get('/supervisor', [\App\Http\Controllers\Admin\SupervisorController::class, "index"])->name("admin.supervisor");
//查看待审核的任务
Route::get('/supervisor/pendingReview', [\App\Http\Controllers\Admin\SupervisorController::class, "pendingReview"])->name("admin.supervisor.pendingReview");
//查询system Id
Route::match(['get', 'post'],'/supervisor/individual', [\App\Http\Controllers\Admin\SupervisorController::class, "individual"])->name("admin.supervisor.individual");
//查询system工作报告task
Route::match(['get', 'post'],'/supervisor/task', [\App\Http\Controllers\Admin\SupervisorController::class, "task"])->name("admin.supervisor.task");
//task报告确认
Route::get('/supervisor/task_confirm', [\App\Http\Controllers\Admin\SupervisorController::class, "confirm"])->name("admin.supervisor.confirm");
//统计报告
Route::get('/supervisor/summary', [\App\Http\Controllers\Admin\SupervisorController::class, "summary"])->name("admin.supervisor.summary");
//签名
Route::match(['get', 'post'],'/supervisor/sign', [\App\Http\Controllers\Admin\SupervisorController::class, "sign"])->name("admin.supervisor.sign");
//下载Inspector_Record Supervisor_Comment excel
Route::get('/supervisor/download', [\App\Http\Controllers\Admin\SupervisorController::class, "download"])->name("admin.supervisor.download");
//下载Summary excel
Route::get('/supervisor/downloadSummary', [\App\Http\Controllers\Admin\SupervisorController::class, "downloadSummary"])->name("admin.supervisor.downloadSummary");
//发送Inspector_Record Supervisor_Comment邮件
Route::match(['get', 'post'],'/supervisor/send', [\App\Http\Controllers\Admin\SupervisorController::class, "send"])->name("admin.supervisor.send");
//发送spervisor_summary邮件
Route::match(['get', 'post'],'/supervisor/sendSummary', [\App\Http\Controllers\Admin\SupervisorController::class, "sendSummary"])->name("admin.supervisor.sendSummary");
//system log必填中的task上传的图片
Route::match(['get', 'post'],'/supervisor/image', [\App\Http\Controllers\Admin\SupervisorController::class, "image"])->name("admin.supervisor.image");
//supervisor查看Use Permit图片
Route::get('/supervisor/checkUsePermit', [\App\Http\Controllers\Admin\SupervisorController::class, "checkUsePermit"])->name("admin.supervisor.checkUsePermit");
//supervisor查看IncidentReport图片
Route::get('/supervisor/checkIncidentReport', [\App\Http\Controllers\Admin\SupervisorController::class, "checkIncidentReport"])->name("admin.supervisor.checkIncidentReport");
//异步上传task image
Route::match(['get', 'post'],'/supervisor/uploadTaskImage', [\App\Http\Controllers\Admin\SupervisorController::class, "uploadTaskImage"])->name("admin.supervisor.uploadTaskImage");
//异步删除task image
Route::match(['get', 'post'],'/supervisor/delTaskImage', [\App\Http\Controllers\Admin\SupervisorController::class, "delTaskImage"])->name("admin.supervisor.delTaskImage");

//显示指定尺寸图片
Route::get('/upload/image', [\App\Http\Controllers\Admin\UploadController::class, "image"])->name("admin.upload.image");

//Total Number列表页
Route::get('/supervisor/totalNumber', [\App\Http\Controllers\Admin\SupervisorController::class, "totalNumber"])->name("admin.supervisor.totalNumber");
//Normal Conditions列表页
Route::get('/supervisor/normalConditions', [\App\Http\Controllers\Admin\SupervisorController::class, "normalConditions"])->name("admin.supervisor.normalConditions");
//Checklist with Follow Up Actions列表页
Route::get('/supervisor/followUpActions', [\App\Http\Controllers\Admin\SupervisorController::class, "followUpActions"])->name("admin.supervisor.followUpActions");
//Checklist Pending Submission列表页
Route::get('/supervisor/pendingSubmission', [\App\Http\Controllers\Admin\SupervisorController::class, "pendingSubmission"])->name("admin.supervisor.pendingSubmission");
//1、2、3、4四种统计列表页下载
Route::get('/supervisor/menuDownload', [\App\Http\Controllers\Admin\SupervisorController::class, "menuDownload"])->name("admin.supervisor.menuDownload");
//1、2、3、4四种统计列表页发送邮件
Route::match(['get', 'post'], '/supervisor/menuSend', [\App\Http\Controllers\Admin\SupervisorController::class, "menuSend"])->name("admin.supervisor.menuSend");
//ajax获取统计列表页排序结果
Route::get('/supervisor/statisticsSort', [\App\Http\Controllers\Admin\SupervisorController::class, "statisticsSort"])->name("admin.supervisor.statisticsSort");
//下载task checklist图片
Route::get('/supervisor/downloadPdf', [\App\Http\Controllers\Admin\SupervisorController::class, "downloadPdf"])->name("admin.supervisor.downloadPdf");
//下载上次上传的systemCrontab excel表格
Route::get('/supervisor/downloadCrontab', [\App\Http\Controllers\Admin\SupervisorController::class, "downloadCrontab"])->name("admin.supervisor.downloadCrontab");
//test 截图上传
Route::match(['get', 'post'],'/supervisor/testImage', [\App\Http\Controllers\Admin\SupervisorController::class, "testImage"])->name("admin.supervisor.testImage");




Route::get('/hkaastaff', [\App\Http\Controllers\Admin\HkaastaffController::class, "index"])->name("admin.hkaastaff");
//查看待审核的任务
Route::get('/hkaastaff/pendingReview', [\App\Http\Controllers\Admin\HkaastaffController::class, "pendingReview"])->name("admin.hkaastaff.pendingReview");
//查询system Id
Route::match(['get', 'post'],'/hkaastaff/individual', [\App\Http\Controllers\Admin\HkaastaffController::class, "individual"])->name("admin.hkaastaff.individual");
//查询system工作报告task
Route::match(['get', 'post'],'/hkaastaff/task', [\App\Http\Controllers\Admin\HkaastaffController::class, "task"])->name("admin.hkaastaff.task");
//task报告确认
Route::get('/hkaastaff/task_confirm', [\App\Http\Controllers\Admin\HkaastaffController::class, "confirm"])->name("admin.hkaastaff.confirm");
//统计报告
Route::get('/hkaastaff/summary', [\App\Http\Controllers\Admin\HkaastaffController::class, "summary"])->name("admin.hkaastaff.summary");
//签名
Route::match(['get', 'post'],'/hkaastaff/sign', [\App\Http\Controllers\Admin\HkaastaffController::class, "sign"])->name("admin.hkaastaff.sign");
//下载Inspector_Record Supervisor_Comment excel
Route::get('/hkaastaff/download', [\App\Http\Controllers\Admin\HkaastaffController::class, "download"])->name("admin.hkaastaff.download");
//下载Summary excel
Route::get('/hkaastaff/downloadSummary', [\App\Http\Controllers\Admin\HkaastaffController::class, "downloadSummary"])->name("admin.hkaastaff.downloadSummary");
//发送Inspector_Record Supervisor_Comment邮件
Route::match(['get', 'post'],'/hkaastaff/send', [\App\Http\Controllers\Admin\HkaastaffController::class, "send"])->name("admin.hkaastaff.send");
//发送spervisor_summary邮件
Route::match(['get', 'post'],'/hkaastaff/sendSummary', [\App\Http\Controllers\Admin\HkaastaffController::class, "sendSummary"])->name("admin.hkaastaff.sendSummary");
//system log必填中的task上传的图片
Route::match(['get', 'post'],'/hkaastaff/image', [\App\Http\Controllers\Admin\HkaastaffController::class, "image"])->name("admin.hkaastaff.image");
//hakk查看Use Permit图片
Route::get('/hkaastaff/checkUsePermit', [\App\Http\Controllers\Admin\HkaastaffController::class, "checkUsePermit"])->name("admin.hkaastaff.checkUsePermit");
//hakk查看IncidentReport图片
Route::get('/hkaastaff/checkIncidentReport', [\App\Http\Controllers\Admin\HkaastaffController::class, "checkIncidentReport"])->name("admin.hkaastaff.checkIncidentReport");

//显示指定尺寸图片
Route::get('/upload/image', [\App\Http\Controllers\Admin\UploadController::class, "image"])->name("admin.upload.image");

//Total Number列表页
Route::get('/hkaastaff/totalNumber', [\App\Http\Controllers\Admin\HkaastaffController::class, "totalNumber"])->name("admin.hkaastaff.totalNumber");
//Normal Conditions列表页
Route::get('/hkaastaff/normalConditions', [\App\Http\Controllers\Admin\HkaastaffController::class, "normalConditions"])->name("admin.hkaastaff.normalConditions");
//Checklist with Follow Up Actions列表页
Route::get('/hkaastaff/followUpActions', [\App\Http\Controllers\Admin\HkaastaffController::class, "followUpActions"])->name("admin.hkaastaff.followUpActions");
//Checklist Pending Submission列表页
Route::get('/hkaastaff/pendingSubmission', [\App\Http\Controllers\Admin\HkaastaffController::class, "pendingSubmission"])->name("admin.hkaastaff.pendingSubmission");

//1、2、3、4四种统计列表页下载
Route::get('/hkaastaff/menuDownload', [\App\Http\Controllers\Admin\HkaastaffController::class, "menuDownload"])->name("admin.hkaastaff.menuDownload");
//1、2、3、4四种统计列表页发送邮件
Route::match(['get', 'post'], '/hkaastaff/menuSend', [\App\Http\Controllers\Admin\HkaastaffController::class, "menuSend"])->name("admin.hkaastaff.menuSend");

//ajax获取统计列表页排序结果
Route::get('/hkaastaff/statisticsSort', [\App\Http\Controllers\Admin\HkaastaffController::class, "statisticsSort"])->name("admin.hkaastaff.statisticsSort");
//下载上次上传的systemCrontab excel表格
Route::get('/hkaastaff/downloadCrontab', [\App\Http\Controllers\Admin\HkaastaffController::class, "downloadCrontab"])->name("admin.hkaastaff.downloadCrontab");

//管理员后台
Route::prefix('Manage')->group(function () {
    //首页
    //Route::get('/', [\App\Http\Controllers\Manage\IndexController::class, "index"])->name("manage.index");
    //0、inspector,1、supervisor,2、hkaa_staff用户列表
    Route::get('/', [\App\Http\Controllers\Manage\UsersController::class, "index"])->name("manage.users");
    Route::match(['get', 'post'],'/users/create', [\App\Http\Controllers\Manage\UsersController::class, "create"])->name("manage.users.create");
    Route::get('/delete', [\App\Http\Controllers\Manage\UsersController::class, "delete"])->name("manage.delete");

    //task列表
    Route::match(['get', 'post'],'/task', [\App\Http\Controllers\Manage\TaskController::class, "index"])->name("manage.task");

    //symstemID
    Route::match(['get', 'post'],'/system', [\App\Http\Controllers\Manage\SystemController::class, "index"])->name("manage.system");
    //查看systemId下的task
    Route::get('/system/check_task', [\App\Http\Controllers\Manage\SystemController::class, "task"])->name("manage.system.check_task");

    //定期需要检查的system crontab
    Route::match(['get', 'post'],'/system/crontab', [\App\Http\Controllers\Manage\SystemController::class, "crontab"])->name("manage.system.crontab");
    Route::get('/system/clearCrontab', [\App\Http\Controllers\Manage\SystemController::class, "clearCrontab"])->name("manage.system.clearCrontab");

    //systemId记录日志列表
    Route::get('/systemLog', [\App\Http\Controllers\Manage\SystemLogController::class, "index"])->name("manage.systemLog");
    Route::get('/systemLog/showAll', [\App\Http\Controllers\Manage\SystemLogController::class, "showAll"])->name("manage.systemLog.showAll");
    Route::get('/systemLog/systemLogTask', [\App\Http\Controllers\Manage\SystemLogController::class, "systemLogTask"])->name("manage.systemLogTask.image");

    //登陆页
    Route::match(['get', 'post'],'/login', [\App\Http\Controllers\Manage\LoginController::class, "index"])->name("manage.login");
    //退出页
    Route::get('/logout', [\App\Http\Controllers\Manage\LogoutController::class, "index"])->name("manage.logout");
    //修改密码页
    Route::match(['get', 'post'],'/personal/uppassword', [\App\Http\Controllers\Manage\PersonalController::class, "index"])->name("manage.personal.uppassword");

    //manage用户列表页
    Route::match(['get', 'post'],'/manages', [\App\Http\Controllers\Manage\ManagesController::class, "index"])->name("manage.manages");
    Route::match(['get', 'post'],'/manages/create', [\App\Http\Controllers\Manage\ManagesController::class, "create"])->name("manage.manages.create");

    //下载上次上传的systemCrontab excel表格
    Route::get('/downloadCrontab', [\App\Http\Controllers\Manage\SystemController::class, "downloadCrontab"])->name("manage.system.downloadCrontab");
});

